import Router from 'vue-router'
import accountRoutes from './account.routes'
import authRoutes from './auth.routes'
import cluebankRoutes from './cluebank.routes'
import dictionaryRoutes from './dictionary.routes'
import homeRoutes from './home.routes'
import documentsRoutes from './documents.routes'
import lessonsRoutes from './lessons.routes'
import testsRoutes from './tests.routes'
import examsRoutes from './exams.routes'
import readingsRoutes from './readings.routes'
import storeRoutes from './store.routes'
import digibookRoutes from './digibook.routes'
import singularRoutes from './singular.routes'
import wordyRoutes from './wordy.routes'
import wordbookRoutes from './wordbook.routes'

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  scrollBehavior(to, from, savedPosition) {
    return { x: 0, y: 0 }
  },
  routes: [
    ...accountRoutes,
    ...authRoutes,
    ...cluebankRoutes,
    ...dictionaryRoutes,
    ...digibookRoutes,
    ...documentsRoutes,
    ...examsRoutes,
    ...homeRoutes,
    ...lessonsRoutes,
    ...readingsRoutes,
    ...storeRoutes,
    ...wordyRoutes,
    ...testsRoutes,
    ...wordbookRoutes,
    ...singularRoutes,
  ],
})
