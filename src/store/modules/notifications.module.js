import * as http from '../../services/notifications.service'

const state = {
  notificationsList: [],
  unreadCount: 0,
  loading: false,
  loaded: false,
  more: false,
  loadingMore: false,
  page: 1,
  announcementVisible: false,
  activeNotification: {},
}

const mutations = {
  setLoading(state, status) {
    state.loading = status
  },
  setLoaded(state, status) {
    state.loaded = status
  },
  setLoadMore(state, status) {
    state.more = status
  },
  setLoadingMore(state, status) {
    state.loadingMore = status
  },
  loadNext(state, data) {
    state.notificationsList.push(...data)
  },
  setNotificationsList(state, data) {
    state.notificationsList = data
  },
  setNotificationsCount(state, data) {
    state.unreadCount = data
  },
  markNotificationsReadAll(state) {
    state.notificationsList.forEach(el => {
      el.read = true
    })
  },
  markNotificationRead(state, data) {
    let item = state.notificationsList.find(el => el.id === data)
    item.read = true
  },
  setActiveNotification(state, data) {
    state.activeNotification = data
  },
  setAnnouncementVisible(state, data) {
    if (!data) state.activeNotification = {}
    state.announcementVisible = data
  },
}

const actions = {
  getNotifications: ({ state, commit }) => {
    if (state.loaded) return
    commit('setLoading', true)
    http.getNotifications().then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setNotificationsCount', res.data.unread_notification_count)
      commit('setNotificationsList', res.data.data)
      commit('setLoadMore', res.data.next_page)
    })
  },

  loadNext: ({ commit }, page) => {
    commit('setLoadingMore', true)
    http.getNotificationsByPage(page).then(res => {
      commit('setNotificationsCount', res.data.unread_notification_count)
      commit('setLoadingMore', false)
      commit('loadNext', res.data.data)
      commit('setLoadMore', res.data.next_page)
    })
  },

  markReadAll: ({ commit }) => {
    http.checkAll().then(res => {
      commit('setNotificationsCount', res.data.unread_notification_count)
      commit('markNotificationsReadAll')
    })
  },

  markRead: ({ commit }, id) => {
    http.check(id).then(res => {
      commit('setNotificationsCount', res.data.unread_notification_count)
      commit('markNotificationRead', id)
    })
  },

  showNotification: ({ commit }, data) => {
    commit('setActiveNotification', data)
  },

  showAnnouncement: ({ commit }, data) => {
    commit('setAnnouncementVisible', data)
  },
}

const getters = {
  isLoaded: state => state.loaded,
  isLoading: state => state.loading,
  hasMore: state => state.more,
  isEmpty: state =>
    state.notificationsList.length === 0 && state.loading === false,
  notifications: state => state.notificationsList,
  loadingMore: state => state.loadingMore,
  unreads: state => state.unreadCount,
  announcementVisible: state => state.announcementVisible,
  activeNotification: state => state.activeNotification,
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
