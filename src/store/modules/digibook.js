import * as http from '../../services/digibook.service'
import {
  GET_DIGI_CURRENT_UNI,
  GET_DIGI_CURRENT_EXERCISE,
  GET_DIGI_CURRENT_CONTENT,
  GET_DIGIBOOK,
  GET_DIGIBOOK_EXERCISE,
} from '@/store/actions.type'

import {
  SET_DIGI_CURRENT_UNIT,
  SET_DIGI_CURRENT_EXERCISE,
  SET_DIGI_CURRENT_CONTENT,
  SET_DIGIBOOK,
  SET_DIGIBOOK_EXERCISE,
} from '@/store/mutations.type'
import { DIGIBOOK_TIMER_VISIBILITY } from '../actions.type'
import { SET_DIGIBOOK_TIMER_VISIBILITY } from '../mutations.type'

const state = {
  currentUnit: null,
  currentExercise: null,
  currentContent: null,
  digitimerVisible: false,
  digibook: {},
  exercises: [],
}

const mutations = {
  [SET_DIGI_CURRENT_UNIT](state, value) {
    state.currentUnit = value
  },
  [SET_DIGI_CURRENT_EXERCISE](state, value) {
    state.currentExercise = value
  },
  [SET_DIGI_CURRENT_CONTENT](state, value) {
    state.currentContent = value
  },
  [SET_DIGIBOOK](state, value) {
    state.digibook = value
  },
  [SET_DIGIBOOK_EXERCISE](state, value) {
    state.exercises = value
  },
  [SET_DIGIBOOK_TIMER_VISIBILITY](state, value) {
    if (state.digitimerVisible !== value) {
      state.digitimerVisible = value
    }
  }
}

const actions = {
  [GET_DIGI_CURRENT_UNI]({ commit }, value) {
    commit(SET_DIGI_CURRENT_UNIT, value)
  },
  [GET_DIGI_CURRENT_EXERCISE]({ commit }, value) {
    commit(SET_DIGI_CURRENT_EXERCISE, value)
  },
  [GET_DIGI_CURRENT_CONTENT]({ commit }, value) {
    commit(SET_DIGI_CURRENT_CONTENT, value)
  },
  [GET_DIGIBOOK]({ commit }, value) {
    // commit(SET_DIGI_UNITS, value)
    http.digibook(value).then(({ data }) => {
      commit(SET_DIGIBOOK, data)
    })
  },
  [GET_DIGIBOOK_EXERCISE]({ commit }, value) {
    // commit(SET_DIGI_UNITS, value)
    commit(SET_DIGIBOOK_EXERCISE, [])
    http.exercises(value).then(({ data }) => {
      commit(SET_DIGIBOOK_EXERCISE, data)
    })
  },
  [DIGIBOOK_TIMER_VISIBILITY]({ commit }, value) {
    commit(SET_DIGIBOOK_TIMER_VISIBILITY, value)
  }
}

const getters = {
  currentUnit: state => state.currentUnit,
  currentExercise: state => state.currentExercise,
  currentContent: state => state.currentContent,
  digibook: state => state.digibook,
  exercises: state => state.exercises,
  digitimerVisible: state => state.digitimerVisible
}

export default {
  state,
  mutations,
  actions,
  getters,
}
