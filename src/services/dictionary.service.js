import Vue from 'vue'

export const getDictionary = () => Vue.$axios('GET', `/dictionary`)

export const getDictionaryInfo = () => Vue.$axios('GET', `/dictionary/info`)

export const getResults = (word, lang) =>
  Vue.$axios('POST', `/dictionary?q=${word}?language=${lang}`)

export const getDictionaryHints = (word, lang) =>
  Vue.$axios('POST', `/dictionary?q=${word}&language=${lang}&type=autocomplete`)

export const getDictionaryResult = word => Vue.$axios('POST', `/dictionary?q=${word}`)
