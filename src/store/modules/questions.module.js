import * as http from '../../services/questions.service'

const state = {
  isLoading: true,
  isLoaded: false,
  hasMore: false,
  questionsList: [],
  buttonLoading: false,
  unreadAnswerCount: 0
}

const getters = {
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  more: state => state.hasMore,
  questions: state => state.questionsList,
  btnLoading: state => state.buttonLoading,
  hasUnreads: state => state.unreadAnswerCount > 0,
  unreadAnswers: state => state.unreadAnswerCount
}

const mutations = {
  setLoading: (state, data) => (state.isLoading = data),
  setBtnLoading: (state, data) => (state.buttonLoading = data),
  setLoaded: (state, data) => (state.isLoaded = data),
  setQuestionsList: (state, data) => (state.questionsList = data),
  setHasMore: (state, data) => (state.hasMore = data !== null),
  setMoreQuestions: (state, data) => state.questionsList.push(...data),
  setUnreadAnswerCount: (state, data) => (state.unreadAnswerCount = data),
  setQuestionRead: (state, data) => {
    state.questionsList.find(el => el.id === data).is_read = true
    state.unreadAnswerCount--
  }
}

const actions = {
  getQuestionsList: ({ commit }) => {
    commit('setLoading', true)
    http.getQuestions().then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setQuestionsList', res.data.data)
      commit('setHasMore', res.data.next_page_url)
    })
  },

  getMoreQuestions: ({ commit }, data) => {
    commit('setBtnLoading', true)
    http.filterQuestions(data).then(res => {
      commit('setBtnLoading', false)
      console.log('data incoming', res.data)
      commit('setMoreQuestions', res.data.data)
      commit('setHasMore', res.data.next_page_url)
    })
  },

  readAnswer: ({ commit }, data) => {
    http.readAnswer(data).then(res => {
      console.log('answer read: ', res)
      commit('setQuestionRead', data)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
