import Vue from 'vue'

export const fetchNotes = payload =>
  Vue.$axios('GET', `/notes?app=${payload.app}&page=${payload.page}`)

export const getNotes = () => Vue.$axios('GET', `/notes`)

export const removeNote = id => Vue.$axios('DELETE', `/notes/${id}`)

export const addNote = data =>
  Vue.$axios('POST', `/notes?text=${data.body}&app=${data.app}&title=${data.title}&url=${data.url}`)

export const editNote = data =>
  Vue.$axios('PUT', `/notes/${data.id}?text=${data.body}&app=${data.app}`)
