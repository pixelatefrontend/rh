import { getDigibookUnitContent } from '../../services/digibook.service'

const state = {
  loading: null,
  loaded: false,
  list: null,
  error: null,
  exerciseId: null,
}

const mutations = {
  setExercicesList: (state, data) => (state.list = data),
  setExercisesLoading: (state, data) => (state.loading = data),
  setExercisesLoaded: (state, data) => (state.loaded = data),
  setExerciseError: (state, data) => (state.loaded = data),
  setExerciseId: (state, data) => (state.loaded = data),
  clearList: state => (state.list = null),
}

const actions = {
  getList: async ({ rootState, commit }, id) => {
    commit('digibookIndex/setActiveList', 'exercise', { root: true })
    if (rootState.digibookIndex.activeUnitId === id) return false
    console.log('id: ', id)
    commit('setExerciseId', id)
    commit('clearList')
    commit('setExercisesLoading', true)
    commit('setExercisesLoaded', false)
    try {
      const response = await getDigibookUnitContent(id)
      commit('setExercicesList', response.data)
      commit('digibookIndex/setActiveUnitId', id, { root: true })
    } catch (error) {
      commit('setExerciseError', error.response)
    } finally {
      commit('setExercisesLoading', false)
      commit('setExercisesLoaded', true)
    }
  },
}

const getters = {
  list: state => state.list,
  loading: state => state.loading,
  loaded: state => state.loaded,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
}
