import shared from './digibook-shared.module'

const state = {
  tabList: [
    {
      icon: 'vocabulary-alt',
      title: 'VOCABULARY',
      visible: false,
    },
    {
      icon: 'collocation-alt',
      title: 'COLLOCATION',
      visible: false,
    },
    {
      icon: 'preposition-alt',
      title: 'PREPOSITION',
      visible: false,
    },
  ],
  activeTab: '',
}

const getters = {
  tabs: state => state.tabList.filter(el => el.visible === true),
  activeTab: state => state.activeTab,
}

const mutations = {
  setTabs: state => {
    const data = state.shared.content.data
    const tabs = state.tabList

    if (data.prepositions.length > 0) {
      tabs[2].visible = true
      state.activeTab = tabs[2].title
    }
    if (data.collocations.length > 0) {
      tabs[1].visible = true
      state.activeTab = tabs[1].title
    }
    if (data.words.length > 0) {
      tabs[0].visible = true
      state.activeTab = tabs[0].title
    }
  },

  setActiveTab: (state, data) => {
    state.activeTab = data.title
  },

  clearTabs: state => {
    state.tabList.forEach(tab => {
      tab.visible = false
    })
  },
}

const actions = {
  handleTabs: ({ commit }) => {
    commit('setTabs')
  },

  handleActiveTab: ({ commit }, data) => {
    commit('setActiveTab', data)
  },

  handleClearTabs: ({ commit }) => {
    commit('clearTabs')
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules: {
    shared,
  },
}
