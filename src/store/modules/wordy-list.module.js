import _ from 'lodash'
import * as http from '../../services/wordy.service'

const getters = {
  isLoading: state => state.isLoading,
  isLoaded: state => state.isLoaded,
  wordys: state => state.wordyList,
  loadingMore: state => state.loadingMore,
  hasMore: state => state.more,
  activeProduct: state => state.activeProduct,
  activeSet: state => state.activeProduct.sets[state.activeSetId]
}

const mutations = {
  setActiveProduct(state, data) {
    state.activeProduct = data
    localStorage.wordyActiveSet = 0
  },
  resetData: state => {
    state.activeProduct = []
  },
  setData: (state, data) => {
    state.filteredItems = data
    state.isLoading = false
    state.showItems = true
    if (data.length < 10) {
      state.isDataEnd = true
    }
  },
  setWordys(state, data) {
    state.wordyList = data
  },
  setLoading(state, value) {
    state.isLoading = value
  },
  setLoaded(state, value) {
    state.isLoaded = value
  },
  updateData(state, data) {
    state.filteredItems.push(...data)
    state.isLoading = false
    state.showItems = true
    if (data.length < 10) {
      state.isDataEnd = true
    }
  },
  increaseCurrentPage(state) {
    state.currentPage += 1
  },
  setCurrentPage(state, value) {
    state.currentPage = value
  },
  setGroupId(state, value) {
    state.groupId = value
  },
  sortBooks(state, selectedOption) {
    if (selectedOption.value === 'new') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.date).reverse()
    }

    if (selectedOption.value === 'old') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.date)
    }

    if (selectedOption.value === 'toLow') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.progress).reverse()
    }

    if (selectedOption.value === 'toHigh') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.progress)
    }
  },
  setActiveSetItem(state, id) {
    state.activeSetId = id
    localStorage.wordyActiveSet = id
  }
}

const actions = {
  getWordysByGroup: ({ commit }, groupId) => {
    console.log('store group id: ', groupId)
    commit('resetData')
    commit('setLoading', true)
    http.group(groupId).then((res) => {
      commit('setActiveProduct', res.data.data)
      commit('setActiveSetItem', 0)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  setActiveSet: ({ commit }, setId) => {
    console.log(setId)
    commit('setActiveSetItem', setId)
  },

  getWordysList: ({ commit }) => {
    commit('resetData')
    commit('setLoading', true)
    http.list().then((res) => {
      commit('setWordys', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  /* getDigibooksList: ({ commit }, filters) => {
    commit('resetData')
    commit('setLoading', true)
    http.list(filters).then((res) => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  }, */

  /* loadNext: ({ commit }, filters) => {
    commit('setLoadingMore', true)

    http.list(filters).then((res) => {
      commit('setLoadingMore', false)
      commit('loadNextItems', res.data.data)
      commit('setLoadMore', res.data.next_page)
    })
  }, */

  /* getDigibooks: ({ commit }) => {
    commit('resetData')
    commit('setLoading', true)
    http.digibooks(state.groupId, state.currentPage).then((res) => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  }, */
  /* loadMore: ({ state, commit }) => {
    commit('increaseCurrentPage')
    commit('setLoading', true)
    if (state.currentGroupName === 'all') {
      http.digibooks(state.currentPage, null).then(({ data }) => {
        commit('updateData', data.data)
      })
    } else {
      http.groups(state.currentGroupName, state.currentPage).then(({ data }) => {
        commit('updateData', data.data)
      })
    }
  }, */
}

const state = {
  activeSetId: 0,
  filteredItems: [],
  wordyList: [],
  showItems: true,
  isLoading: true,
  isLoaded: false,
  isDataEnd: false,
  currentPage: 1,
  setLevel: 0,
  setStatus: 2,
  level: 0,
  groupId: -1,
  currentGroupName: 'all',
  loadingMore: false,
  more: false,
  activeProduct: null
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
}
