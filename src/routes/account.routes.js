export default [

  {
    path: '/hesabim',
    component: () => import(/* webpackChunkName: "account" */ '../views/Account'),
    children: [
      {
        path: '',
        redirect: 'kisisel-bilgiler'
      },
      {
        path: 'kisisel-bilgiler',
        name: 'AccountDetails',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountDetails'),
        meta: {
          headerShow: true,
          title: 'Kişisel Bilgiler',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      },
      {
        path: 'sifre-degistir',
        name: 'AccountPasswords',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountPasswords'),
        meta: {
          headerShow: true,
          title: 'Şifre Değiştir',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      },
      {
        path: 'eposta-bildirimleri',
        name: 'AccountNotifications',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountNotifications'),
        meta: {
          headerShow: true,
          title: 'E-Posta Bildirimleri',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      },
      {
        path: 'kayitli-adresler',
        name: 'AccountAddresses',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountAddresses'),
        meta: {
          headerShow: true,
          title: 'Kayıtlı Adresler',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      },
      {
        path: 'urunlerim',
        name: 'AccountProducts',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountProducts'),
        meta: {
          headerShow: true,
          title: 'Ürünlerim',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      },
      {
        path: 'siparis-gecmisi',
        name: 'AccountOrders',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountOrders'),
        meta: {
          headerShow: true,
          title: 'Sipariş Geçmişi',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      },
      {
        path: 'promosyonlar',
        name: 'AccountPromotions',
        component: () => import(/* webpackChunkName: "account" */ '../views/Account/AccountPromotions'),
        meta: {
          headerShow: true,
          title: 'Promosyonlar',
          requiresAuth: true,
          pageClass: 'header--gray',
          toolbarVisible: true
        }
      }
    ],
    meta: {
      pageClass: 'header--gray',
      hasTour: false
    }
  }
]
