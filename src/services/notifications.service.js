import Vue from 'vue'

export const getNotifications = () => Vue.$axios('GET', '/notifications')

export const sortNotifications = sort => Vue.$axios('GET', `/notifications?sort=${sort}`)

export const searchInNotifications = param => Vue.$axios('GET', `/notifications?search=${param}`)

export const getNotificationsByPage = page => Vue.$axios('GET', `/notifications?page=${page}`)

export const checkAll = () => Vue.$axios('POST', '/notifications/check?all=1')

export const check = ids => Vue.$axios('POST', `/notifications/check?ids=${ids}`)
