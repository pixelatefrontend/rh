import Vue from 'vue'

const getters = {
  performance: state => state.performance,
  lastActivies: state => state.lastActivies,
  lastAwards: state => state.lastAwards,
  appReport: state => state.appReport,
  loading: state => state.loading,
  loaded: state => state.loaded,
  logsLoader: state => state.logsLoader,
  filters: state => {
    let filters = JSON.parse(JSON.stringify(state.performance.filter))
    filters.groups = filters.groups.map(
      ({ value, label, media, selected = value === state.performance.filter['group-value'] }) => ({
        value,
        label,
        selected,
        icon: media
      })
    )
    filters.dates = filters.dates.map(
      ({ value, label, media, selected = value === state.performance.filter['date-value'] }) => ({
        value,
        label,
        selected,
        icon: media
      })
    )
    return filters
  },
  filter: state => {
    let filter = state.filter !== null ? state.filter : []
    if (state.filter === null) {
      let apps = JSON.parse(JSON.stringify(state.performance.data.apps))
      apps.forEach(appData => {
        filter.push({ app: appData.value, date: '', group: '' })
      })
    }
    return filter
  },
  loaders: state => {
    let loaders = state.loaders !== null ? state.loaders : []
    if (state.loaders === null) {
      let apps = JSON.parse(JSON.stringify(state.performance.data.apps))
      apps.forEach(appData => {
        loaders.push({ app: appData.value, loading: false })
      })
    }
    return loaders
  }
}

const actions = {
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
  getPerformanceData: ({ commit }) => {
    commit('SET_LOADING', true)
    Vue.axios
      .get(`my-performance`)
      .then(response => {
        commit('SET_PREFORMANCE', response.data)
        commit('SET_LASTACTIVITES', response.data.data.logs)
        commit('SET_lASTAWARDS', response.data.data.badges)
        commit('SET_APPREPORT', response.data.data.apps)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
      })
      .catch(e => {
        console.log('catch', e)
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  getAppPreformaceData: ({ commit }, appfilter) => {
    let iseExam = (appfilter.app === 'exam')
    Vue.axios
      .get(`my-performance/${appfilter.app}?product_id=${appfilter.group}&date_id=${appfilter.date}&is_exam=${iseExam}`)
      .then(response => {
        let param = { 'data': response.data, 'appID': appfilter.app }
        commit('SET_ONEAPPREPORT', param)
        let loaders = appfilter.loaders
        loaders.flatMap(e => {
          if (e.app === appfilter.app) {
            e.loading = false
          }
        })
        commit('SET_LOADERS', loaders)
      })
      .catch(e => {
        console.log('catch', e)
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
        let loaders = appfilter.loaders
        loaders.flatMap(e => {
          if (e.app === appfilter.app) {
            e.loading = false
          }
        })
        commit('SET_LOADERS', loaders)
      })
  },
  async getLastActivites({ commit }, lastActivies) {
    await Vue.axios
      .get(`${lastActivies.paginate.next_page_url}`)
      .then(response => {
        let newActivies = response.data.data.logs
        newActivies.items.unshift(...lastActivies.items)
        commit('SET_LASTACTIVITES', newActivies)
        commit('SET_LOGS_LOADER', false)
      })
      .catch(e => {
        commit('SET_LOGS_LOADER', false)
        console.log('catch', e)
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  setFilter: ({ commit }, filter) => {
    commit('SET_FILTER', filter)
  },
  setAppLoader({ commit }, loaders) {
    commit('SET_LOADERS', loaders)
  },
  setLogsLoader({ commit }, loader) {
    commit('SET_LOGS_LOADER', loader)
  }
}

const mutations = {
  SET_PREFORMANCE(state, performance) {
    const $state = state
    $state.performance = performance
  },
  SET_LASTACTIVITES(state, lastActivies) {
    const $state = state
    $state.lastActivies = lastActivies
  },
  SET_NEW_ACTIVITES(state, newActivies) {

  },
  SET_lASTAWARDS(state, lastAwards) {
    const $state = state
    $state.lastAwards = lastAwards
  },
  SET_APPREPORT(state, appReport) {
    const $state = state
    $state.appReport = appReport
  },
  SET_ONEAPPREPORT(state, appReport) {
    const $state = state
    $state.appReport.flatMap(app => {
      if (app.value === appReport.appID) {
        app.items = appReport.data.data.items
        app.dates = appReport.data.data.dates
        app.total_point = appReport.data.data.total_point
        app.badges = appReport.data.data.badges
      }
    })
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_FILTER(state, filter) {
    const $state = state
    $state.filter = filter
  },
  SET_LOADERS(state, loaders) {
    const $state = state
    $state.loaders = loaders
  },
  SET_LOGS_LOADER(state, loader) {
    const $state = state
    $state.logsLoader = loader
  },
  RESET_STATE(state) {
    const $state = state
    $state.performance = null
    $state.lastActivies = null
    $state.lastAwards = null
    $state.appReport = null
    $state.loading = false
    $state.loaded = false
    $state.filter = null
    $state.loaders = null
    $state.logsLoader = false
  }
}

const state = {
  performance: null,
  lastActivies: null,
  lastAwards: null,
  appReport: null,
  loading: false,
  loaded: false,
  logsLoader: false,
  filter: null,
  loaders: null,

}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
