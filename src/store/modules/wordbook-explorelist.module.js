import Vue from 'vue'

const getters = {
  discoveries: state => state.discoveries,
  listDetails: state => state.listDetails,
  loading: state => state.loading,
  loaded: state => state.loaded,
  explorePage: state => state.explorePage,
  noSearchResult: state => state.noSearchResult,
}

const actions = {
  /**
   *
   */
  getExploreList: ({ state, commit }, searchWord) => {
    const sortType = state.sort
    commit('SET_LOADING', true)
    commit('SET_NO_SEARCH_RESULT', false)
    let urlPath = `/word-lists/explore?sort=${sortType}`
    if (searchWord) {
      urlPath = `/word-lists/explore?q=${searchWord}`
    }
    return Vue.axios
      .get(urlPath)
      .then(response => {
        commit('SET_DISCOVERIES', response.data.data)
        commit('SET_EXPLORE_PAGE', response.data.next_page)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        if (searchWord && !response.data.data) {
          commit('SET_NO_SEARCH_RESULT', true)
        } else {
          commit('SET_NO_SEARCH_RESULT', false)
        }
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  followList: ({ commit }, listId) => {
    commit('SET_LOADING', true)
    return Vue.axios
      .post(`/word-lists/follow?id=${listId}`)
      .then(res => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        return res
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
        return false
      })
  },
  /**
   *
   */
  loadMoreExplor: ({ commit, state }, pageNo) => {
    const sortType = state.sort
    commit('SET_LOADING', true)
    const $state = state
    return Vue.axios
      .get(`/word-lists/explore?sort=${sortType}&page=${pageNo}`)
      .then(response => {
        let newlist = response.data.data
        newlist.unshift(...$state.discoveries)
        commit('SET_DISCOVERIES', newlist)
        commit('SET_EXPLORE_PAGE', response.data.next_page)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },

  /**
   *
   */
  handleSortType: ({ commit }, type) => {
    commit('SET_SORT_TYPE', type)
  },
}

const mutations = {
  SET_DISCOVERIES(state, discoveries) {
    const $state = state
    $state.discoveries = discoveries
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_EXPLORE_PAGE(state, explorePage) {
    const $state = state
    if (explorePage) {
      $state.explorePage = $state.explorePage + 1
    } else {
      $state.explorePage = false
    }
  },
  SET_NO_SEARCH_RESULT(state, result) {
    const $state = state
    $state.noSearchResult = result
  },
  SET_SORT_TYPE(state, type) {
    const $state = state
    $state.sort = type
  },
}

const state = {
  discoveries: [],
  listDetails: [],
  loading: null,
  loaded: null,
  explorePage: 1,
  noSearchResult: false,
  sort: 'new',
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
