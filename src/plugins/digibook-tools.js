import Tools from './tools'

export default {
  install(Vue) {
    console.log('installed new plugin')
    Vue.component('MyTools', Tools)
  },
}
