import Vue from 'vue'

export const info = () => Vue.$axios('GET', `/wordy/info`)

export const list = () => Vue.$axios('GET', `/wordy/list`)

export const group = id => Vue.$axios('GET', `/wordy/${id}`)
