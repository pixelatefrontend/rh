import Vue from 'vue'

export const info = () => Vue.$axios('GET', `/documents/info`)

export const document = id => Vue.$axios('GET', `/documents/${id}`)

export const group = (id) => Vue.$axios('GET', `/documents?group_id=${id}`)

export const search = param => Vue.$axios('GET', `/documents/search/${param}`)

export const list = (filters) => Vue.$axios(
  'GET', `/documents?group_id=${filters.groupId}&category=${filters.categoryId}&page=${filters.page}&is_opened=${filters.isRead}&search=${filters.search}`
)
