import { getDigibook } from '../../services/digibook.service'

import exercises from './digibook-exercises-list.module'

const state = {
  digibook: null,
  digibookId: 0,
  loading: null,
  loaded: null,
  error: null,
  activeList: 'unit',
  activeUnitId: null,
  staticContents: null,
  activeStaticContent: null,
  exercises: null,
  contentListVisible: false,
  selection: [
    {
      type: 'unit',
      name: 'ünite',
      id: null,
      text: ''
    },
    {
      type: 'exercise',
      name: 'alıştırma',
      id: null,
      text: ''
    },
    {
      type: 'content',
      name: 'içerik',
      id: null,
      text: ''
    }
  ]
}

const mutations = {
  setLoading: (state, data) => (state.loading = data),
  setLoaded: (state, data) => (state.loaded = data),
  setError: (state, data) => (state.loaded = data),
  setDigibook: (state, data) => (state.digibook = data),
  setDigibookId: (state, data) => (state.digibookId = data),
  setActiveList: (state, data) => (state.activeList = data),
  setActiveUnitId: (state, data) => (state.activeUnitId = data),
  setStaticContents: (state, data) => {
    state.staticContents = data
    if (data) state.activeStaticContent = data[0]
  },
  setActiveStaticContent: (state, data) => {
    state.activeStaticContent = state.staticContents.find(el => el.link === data)
  },
  showContentList: (state, data) => (state.contentListVisible = data),
  setSelection: (state, data) => {
    console.log(data)
    state.digibook.selection[0].name = 'ünite'
    state.digibook.selection[1].name = 'alıştırma'
    state.digibook.selection[2].name = 'içerik'
  },
  setSelectionTitle: (state, data) => {
    if (data.type === null) {
      state.digibook.selection.map(el => (el.text = data.text))
    } else {
      let item = state.digibook.selection.find(el => el.type === data.type)
      item.text = data.text
    }
  },

  clearDigibook: state => {
    state.digibook = null
    state.digibookId = 0
    state.loading = null
    state.loaded = null
    state.error = null
  }
}

const actions = {
  fetchDigibook: async ({ state, commit }, id) => {
    if (state.digibookId === id) return true
    commit('setLoading', true)
    commit('setLoaded', false)
    commit('setDigibookId', parseInt(id))
    try {
      const response = await getDigibook(id)
      commit('setDigibook', response.data)
      commit('setSelection', response.data.selection)
      commit('setStaticContents', response.data.static_contents)
      commit('digibookIndex/exercises/setExercicesList', response.data.exercises, { root: true })
      commit('setLoaded', true)
      console.log(response.data)
    } catch (error) {
      commit('setError', error.response)
    } finally {
      commit('setLoading', false)
    }
  },

  handleSetActiveList: ({ commit }, data) => {
    commit('setActiveList', data)
  },

  handleSetActiveStaticContent: ({ commit }, data) => {
    commit('setActiveStaticContent', data)
  },

  handleContentListVisibility: ({ commit }, status) => {
    commit('showContentList', status)
  },

  handleSetSelectionTitle: ({ commit }, data) => {
    commit('setSelectionTitle', data)
  }
}

const getters = {
  loading: state => state.loading,
  loaded: state => state.loaded,
  digibook: state => state.digibook,
  bookName: state => state.digibook.title,
  bookDesc: state => state.digibook.text,
  isDownloadable: state => state.digibook.is_downloadable,
  downloadLink: state => state.digibook.download,
  buyLink: state => state.digibook.buy_link,
  physicalProductId: state => state.digibook.physical_product_id,
  prologue: state => state.digibook.prolog,
  guide: state => state.digibook.guideline,
  units: state => state.digibook.units,
  selection: state => state.digibook.selection,
  activeUnitId: state => state.activeUnitId,
  activeList: state => state.activeList,
  staticContents: state => state.staticContents,
  contentListVisible: state => state.contentListVisible,
  activeStaticContent: state => state.activeStaticContent
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
  modules: {
    exercises
  }
}
