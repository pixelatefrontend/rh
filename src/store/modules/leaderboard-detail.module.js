import Vue from 'vue'

const getters = {
  id: state => state.id,
  isActive: state => state.isActive,
  detail: state => state.detail,
  loading: state => state.loading,
  loaded: state => state.loaded,
  filter: state => state.filter
}

const actions = {
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
  getDetail: ({ commit, state }) => {
    commit('SET_LOADING', true)

    const params =
      state.filter.sort !== null
        ? `?sort=old&groups=${state.filter.sort}`
        : ''

    Vue.axios
      .get(`leadership/${state.id}${params}`)
      .then(response => {
        commit('SET_DETAIL', response.data.data)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
      })
      .catch((e) => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  setId: ({ commit }, id) => {
    commit('SET_ID', id)
  },
  setActive: ({ commit }, status) => {
    commit('SET_ACTIVE', status)
  },
  setFilter: ({ commit, dispatch }, filter) => {
    commit('SET_FILTER', filter)
    dispatch('getDetail')
  }
}

const mutations = {
  SET_ID(state, id) {
    const $state = state
    $state.id = id
  },
  SET_ACTIVE(state, status) {
    const $state = state
    $state.isActive = status
  },
  SET_DETAIL(state, detail) {
    const $state = state
    $state.detail = detail
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  RESET_STATE(state) {
    const $state = state
    $state.id = null
    $state.isActive = false
    $state.detail = null
    $state.loading = false
    $state.loaded = false
    $state.filter = {
      sort: null,
    }
  },
  SET_FILTER(state, filter) {
    const $state = state
    $state.filter = filter
  }
}

const state = {
  id: null,
  isActive: false,
  detail: null,
  loading: false,
  loaded: false,
  filter: {
    sort: null,
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
