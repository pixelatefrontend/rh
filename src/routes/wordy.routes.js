export default [
  {
    path: '/wordy',
    component: () => import(/* webpackChunkName: "wordy" */ '../views/Wordy/'),
    children: [
      {
        path: '',
        name: 'WordyLanding',
        component: () => import(/* webpackChunkName: "wordy" */ '../views/Wordy/WordyLanding'),
        meta: {
          title: 'Wordy',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: true,
          tourName: 'wordyTour',
          noteName: 'wordy'
        },
      },
      {
        path: ':id',
        name: 'WordyDetail',
        component: () => import(/* webpackChunkName: "wordy" */ '../views/Wordy/WordyDetail'),
        props: true,
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'wordy'
        }
      }
    ]
  }
]
