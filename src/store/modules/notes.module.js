import Vue from 'vue'
import * as http from '../../services/notes.service'

const state = {
  isLoading: true,
  isLoaded: false,
  notesList: null,
  noteCategories: [
    {
      key: 'all',
      name: 'Tüm İçerikler'
    }
  ],
  currentCategory: null,
  filteredNotesList: null,
  nextPage: false
}

const getters = {
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  notes: state => state.filteredNotesList,
  categories: state => state.noteCategories,
  more: state => state.nextPage
}

const mutations = {
  setLoading: (state, data) => {
    state.isLoading = data
  },

  setLoaded: (state, data) => {
    state.isLoaded = data
  },

  setNotesList: (state, data) => {
    state.notesList = data
    state.filteredNotesList = state.notesList
  },

  setNoteCategories: (state, data) => {
    if (state.noteCategories.length === 1) state.noteCategories.push(...data)
  },

  removeNoteFromList: (state, data) => {
    let removeIndex = state.notesList.map(item => item.id).indexOf(data)
    state.notesList.splice(removeIndex, 1)
  },

  addNoteToList: (state, data) => {
    state.notesList.push(data)
  },

  filterNotesList: (state, data) => {
    state.currentCategory = data
    if (data !== 'all') state.filteredNotesList = state.notesList.filter(el => el.app === data)
    else state.filteredNotesList = state.notesList
  },

  modifyNoteItem: (state, data) => {
    const noteIndex = state.notesList.findIndex(el => el.id === data.id)
    Vue.set(state.notesList[noteIndex], 'text', data.body)
  },

  setNextPage: (state, data) => {
    data !== null ? (state.nextPage = true) : (state.nextPage = false)
  },

  setMoreNotes: (state, data) => {
    state.notesList.push(...data)
  }
}

const actions = {
  fetchNotesList: ({ commit }, data) => {
    commit('setLoading', true)
    http.fetchNotes(data).then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setNotesList', res.data.notes.data)
      commit('setNoteCategories', res.data.categories)
      commit('setNextPage', res.data.notes.next_page_url)
    })
  },

  getNotesList: ({ commit }) => {
    commit('setLoading', true)
    http.getNotes().then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setNotesList', res.data.notes.data)
      commit('setNoteCategories', res.data.categories)
      commit('setNextPage', res.data.notes.next_page_url)
    })
  },

  async removeNoteItem({ commit }, id) {
    let result = await http.removeNote(id)
    console.log(result)
    commit('removeNoteFromList', id)
  },

  addNoteItem: ({ commit, dispatch }, data) => {
    http.addNote(data).then(res => {
      dispatch('getNotesList', { app: '', page: 1 })
    })
  },

  loadMoreNotes: ({ commit }, data) => {
    http.fetchNotes(data).then(res => {
      commit('setMoreNotes', res.data.notes.data)
      commit('setNextPage', res.data.notes.next_page_url)
    })
  },

  async editNoteItem({ commit }, data) {
    commit('setLoading', true)
    commit('setLoaded', false)
    let result = await http.editNote(data)
    console.log('result edit', result)
    if (result.status === 200) {
      http.getNotes({ app: 'all', page: 1 }).then(res => {
        commit('setLoading', false)
        commit('setLoaded', true)
        commit('setNotesList', res.data.notes.data)
        commit('setNextPage', res.data.notes.next_page_url)
      })
    }
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
