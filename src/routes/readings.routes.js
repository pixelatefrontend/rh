import ReadingsIndex from '../views/Readings/index'
import ReadingsList from '../views/Readings/ReadingsList'
import ReadingsDetail from '../views/Readings/ReadingsDetail'

export default [
  {
    path: '/okuma',
    component: ReadingsIndex,
    children: [
      {
        path: '',
        name: 'ReadingsList',
        component: ReadingsList,
        meta: {
          title: 'Okuma',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: true,
          tourName: 'readingsTour',
          noteName: 'reading',
        },
      },
      {
        path: ':id/liste/:list',
        name: 'ReadingDetail',
        component: ReadingsDetail,
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'reading',
        },
      },
    ],
  },
]
