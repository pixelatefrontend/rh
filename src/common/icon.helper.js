const icons = [
  {
    src: require('../assets/img/app-ders.svg'),
    key: 'lessons'
  },
  {
    src: require('../assets/img/app-ders.svg'),
    key: 'lesson'
  },
  {
    src: require('../assets/img/app-test.svg'),
    key: 'test'
  },
  {
    src: require('../assets/img/app-test.svg'),
    key: 'tests'
  },
  {
    src: require('../assets/img/app-wordy.svg'),
    key: 'wordies'
  },
  {
    src: require('../assets/img/app-wordy.svg'),
    key: 'wordy'
  },
  {
    src: require('../assets/img/app-word-book.svg'),
    key: 'word_book'
  },
  {
    src: require('../assets/img/app-okuma.svg'),
    key: 'reading'
  },
  {
    src: require('../assets/img/app-okuma.svg'),
    key: 'reading_lists'
  },
  {
    src: require('../assets/img/app-digibook.svg'),
    key: 'digibooks'
  },
  {
    src: require('../assets/img/app-digibook.svg'),
    key: 'digibook'
  },
  {
    src: require('../assets/img/app-deneme.svg'),
    key: 'exams'
  },
  {
    src: require('../assets/img/app-deneme.svg'),
    key: 'exam'
  },
  {
    src: require('../assets/img/app-dokuman.svg'),
    key: 'document'
  },
  {
    src: require('../assets/img/app-sozluk.svg'),
    key: 'dictionary'
  },
  {
    src: require('../assets/img/app-ipucu-havuzu.svg'),
    key: 'tip_pool'
  },
  {
    src: require('../assets/img/app-ipucu-havuzu.svg'),
    key: 'cluebank'
  },
  {
    src: require('../assets/img/app-global.svg'),
    key: 'global'
  },
  {
    src: require('../assets/img/app-notes.svg'),
    key: 'note'
  }
]

const getIcon = key =>
  icons.find(icon => icon.key === key) ? icons.find(icon => icon.key === key).src : ''

export default getIcon
