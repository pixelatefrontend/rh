import Vue from 'vue'
import Router from 'vue-router'
import store from './store'
import router from './routes'
import App from './App.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Logger from '@/plugins/logger'
import Axios from '@/plugins/axios'
import { loggedIn, getToken } from './common/auth'
import SvgIcon from './components/SvgIcon'
import DynamicIcon from './components/DynamicIcon'
import VueButton from './components/VueButton'
import VueLink from './components/VueLink'
import SlideUpDown from './components/SlideUpDown'
import Container from './components/Container'
import SelectBox from './components/SelectBox'
import Toasted from 'vue-toasted'
import VueClipboard from 'vue-clipboard2'
import Vuebar from 'vuebar'
import SmoothScrollbar from 'vue-smooth-scrollbar'
import Autocomplete from 'v-autocomplete'
import VTooltip from 'v-tooltip'
import VueConfetti from 'vue-confetti'
import VModal from 'vue-js-modal'
import Vuelidate from 'vuelidate'
import VueMask from 'v-mask'
import VueSweetalert2 from 'vue-sweetalert2'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VCalendar from 'v-calendar'
import VueGtag from 'vue-gtag'
import InputMask from 'vue-input-mask'
import ScrollLoader from 'vue-scroll-loader'
import VTour from 'vue-tour'
import VuePortal from '@linusborg/vue-simple-portal'
import TextareaAutosize from 'vue-textarea-autosize'
import { HANDLE_LOGIN_STATUS, SET_PREVIOUS_URL, SET_HEADER_CLASS } from './store/actions.type'

// require styles
import 'swiper/dist/css/swiper.css'
require('vue-tour/dist/vue-tour.css')

// Use Router
Vue.use(Router)
const originalPush = Router.prototype.push
Router.prototype.push = function push(location) {
  return originalPush.call(this, location).catch(err => err)
}

// Google Tag Manager initialization
Vue.use(VueGtag, { config: { id: 'UA-19742591-6' } }, router)

// UserSnap aka 'Sorun Bildir' widget integration
;(function() {
  var s = document.createElement('script')
  s.type = 'text/javascript'
  s.async = true
  s.src = '//api.usersnap.com/load/f29c1ac9-4fe3-4f11-a9ea-dcec969e3b82.js'
  var x = document.getElementsByTagName('script')[0]
  x.parentNode.insertBefore(s, x)
})()

var VueScrollTo = require('vue-scrollto')

Vue.use(VueAwesomeSwiper, {})
  .use(Logger)
  .use(Axios)
  .use(Toasted)
  .use(VueScrollTo)
  .use(VueClipboard)
  .use(Vuebar)
  .use(Autocomplete)
  .use(SmoothScrollbar, {
    alwaysShowTracks: true,
  })
  .use(VTooltip)
  .use(VueConfetti)
  .use(VModal, {
    componentName: 'VueModal',
  })
  .use(Vuelidate)
  .use(VueMask)
  .use(VueSweetalert2)
  .use(VCalendar)
  .use(ScrollLoader)
  .use(VTour)
  .use(VuePortal)
  .use(TextareaAutosize)

// This is global API URL that is used with Axios requests
const BASE_API_URL = 'https://www.remzihoca.com/platform_v2'

Vue.use(VueAxios, axios)
Vue.axios.defaults.baseURL = BASE_API_URL
Vue.axios.defaults.headers.common['Accept'] = 'application/json'
Vue.axios.defaults.headers.common['Content-Type'] = 'application/json'

// When user refresh the web page
if (getToken()) {
  Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + getToken()
}

// Registering common components makes them accessible from every component
Vue.component('svg-icon', SvgIcon)
Vue.component('dynamic-icon', DynamicIcon)
Vue.component('v-button', VueButton)
Vue.component('v-link', VueLink)
Vue.component('slide-up-down', SlideUpDown)
Vue.component('Container', Container)
Vue.component('select-box', SelectBox)
Vue.component('input-mask', InputMask)

Vue.config.productionTip = false
Vue.config.performance = process.env.NODE_ENV !== 'production'
Vue.config.devtools = process.env.NODE_ENV !== 'production'

// We are checking 'auth_token' availability in the localStorage.
// Providing access to user is handled in the Vuex store
store.dispatch(HANDLE_LOGIN_STATUS, loggedIn())

router.beforeEach((to, from, next) => {
  // This console statement is here to debug route status
  // console.log('status: ', store.getters.isAuthorized, to.name, to.matched.length, to.meta.requiresAuth)
  const ignoredPaths = ['/giris-yap', '/kaydol']
  const path = window.location.pathname
  const pageClass = to.meta.pageClass

  store.dispatch(SET_HEADER_CLASS, pageClass || '')

  if (ignoredPaths.indexOf(path) < 0) {
    store.dispatch(SET_PREVIOUS_URL, path)
  }
  if (to.name === 'Login') {
    if (store.getters.isAuthorized) next({ name: 'HomeSchedule' })
    else next()
  } else if (to.matched.length > -1) {
    if (store.getters.isAuthorized) {
      next()
    } else {
      if (to.meta.requiresAuth) next({ name: 'Login' })
      else next()
    }
  } else {
    if (to.meta.requiresAuth) {
      if (store.getters.isAuthorized) next()
      else next({ name: 'Login' })
    } else {
      if (store.getters.isAuthorized) next({ name: 'HomeSchedule' })
      else next()
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
