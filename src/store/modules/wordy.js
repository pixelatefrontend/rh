import {
  GET_WORDY_CURRENT_ITEM,
  GET_WORDY_DATA
} from '@/store/actions.type'

import {
  SET_WORDY_CURRENT_ITEM,
  SET_WORDY_DATA
} from '@/store/mutations.type'

const state = {
  currentItem: null,
  questions: {
    title: 'set 1',
    data: [
      {
        id: 0,
        type: 'classic',
        intro: null,
        title: 'Çoktan Seçmeli',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        questions: [
          {
            id: 0,
            word: 'present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            word: '2present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            word: '3present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            word: '4present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 4,
            word: '5present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 5,
            word: '6present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 6,
            word: '7present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 1,
        type: 'preposition',
        title: 'Preposition',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: [
          {
            id: 0,
            word: 'responsible',
            fillBoxLeft: true,
            words: [
              {
                extension: 'in',
                title: '1dan sorumlu olmak'
              },
              {
                extension: 'at',
                title: '2dan sorumlu olmak'
              },
              {
                extension: 'for',
                title: '3dan sorumlu olmak'
              }
            ]
          },
          {
            id: 1,
            word: 'responsible',
            fillBoxLeft: false,
            words: [
              {
                extension: 'in',
                title: '1dan sorumlu olmak'
              },
              {
                extension: 'at',
                title: '2dan sorumlu olmak'
              },
              {
                extension: 'for',
                title: '3dan sorumlu olmak'
              },
              {
                extension: 'by',
                title: '4dan sorumlu olmak'
              },
              {
                extension: 'from',
                title: '5dan sorumlu olmak'
              }
            ]
          },
          {
            id: 2,
            word: 'responsible',
            fillBoxLeft: true,
            words: [
              {
                extension: 'in',
                title: '1dan sorumlu olmak'
              },
              {
                extension: 'at',
                title: '2dan sorumlu olmak'
              }
            ]
          }
        ],
        questions: [
          {
            id: 0,
            isSentence: true,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '1seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            isSentence: false,
            fillBoxLeft: true,
            title: '2seçili kelimenin yerine hangi kelime yazılabilir',
            word: 'present',
            meaning: 'şu anda',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            isSentence: true,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '3seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            isSentence: false,
            fillBoxLeft: false,
            title: '4seçili kelimenin yerine hangi kelime yazılabilir',
            word: 'present',
            meaning: 'şu anda',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 2,
        type: 'dictate',
        title: 'Dikte',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            cover: require('@/assets/img/intro-img.jpg'),
            title: '1Birini sevindirmek, mutlu etmek için verilen şey',
            type: 'noun',
            word: [
              {
                letter: 'p',
                status: ''
              },
              {
                letter: 'r',
                status: ''
              },
              {
                letter: 'e',
                status: ''
              },
              {
                letter: 's',
                status: ''
              },
              {
                letter: 'e',
                status: ''
              },
              {
                letter: 'n',
                status: ''
              },
              {
                letter: 't',
                status: ''
              }
            ]
          },
          {
            id: 1,
            cover: require('@/assets/img/intro-img.jpg'),
            title: '2Birini sevindirmek, mutlu etmek için verilen şey',
            type: 'noun',
            word: [
              {
                letter: 'p',
                status: '',
                focus: false
              },
              {
                letter: 'r',
                status: '',
                focus: false
              },
              {
                letter: 'e',
                status: '',
                focus: false
              },
              {
                letter: 's',
                status: '',
                focus: false
              },
              {
                letter: 'e',
                status: '',
                focus: false
              },
              {
                letter: 'n',
                status: '',
                focus: false
              },
              {
                letter: 't',
                status: '',
                focus: false
              }
            ]
          }
        ]
      },
      {
        id: 3,
        type: 'classic-sound',
        title: 'Sesli Çoktan Seçmeli',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 4,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 5,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 6,
            word: 'present',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 4,
        type: 'minimal-pair',
        title: 'Telaffuz Farkı',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            word: 'present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: true,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              }
            ]
          },
          {
            id: 1,
            word: '2present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            word: '3present',
            wordType: 'verb',
            question: 'Kelimenin anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 5,
        type: 'synonym',
        title: 'Eş Anlamlılar',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            word: 'present',
            wordType: 'verb',
            question: 'Kelimenin eş anlamı nedir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            word: '2present',
            wordType: 'verb',
            question: 'Kelimenin eş anlamı nedir?',
            active: false,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            word: '3present',
            wordType: 'verb',
            question: 'Kelimenin eş anlamı nedir?',
            active: false,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 6,
        type: 'collocation',
        title: 'Collocation',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: [
          {
            id: 0,
            active: false,
            primaryWord: 'make decision',
            primaryMeaning: 'karar vermek',
            firstWord: {
              word: 'make',
              type: 'verb',
              meaning: 'yapmak'
            },
            secondWord: {
              word: 'decision',
              type: 'verb',
              meaning: 'karar'
            }
          },
          {
            id: 1,
            active: false,
            primaryWord: '2make decision',
            primaryMeaning: 'karar vermek',
            firstWord: {
              word: 'make',
              type: 'verb',
              meaning: 'yapmak'
            },
            secondWord: {
              word: 'decision',
              type: 'verb',
              meaning: 'karar'
            }
          },
          {
            id: 2,
            active: false,
            primaryWord: '3 make decision',
            primaryMeaning: 'karar vermek',
            firstWord: {
              word: 'make',
              type: 'verb',
              meaning: 'yapmak'
            },
            secondWord: {
              word: 'decision',
              type: 'verb',
              meaning: 'karar'
            }
          },
          {
            id: 3,
            active: false,
            primaryWord: '4 make decision',
            primaryMeaning: 'karar vermek',
            firstWord: {
              word: 'make',
              type: 'verb',
              meaning: 'yapmak'
            },
            secondWord: {
              word: 'decision',
              type: 'verb',
              meaning: 'karar'
            }
          }
        ],
        questions: [
          {
            id: 0,
            firstWord: '',
            secondWord: 'decision',
            meaning: 'karar vermek',
            title: 'anlamındaki kelime hangisidir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'make',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            firstWord: 'make',
            secondWord: '',
            meaning: 'karar vermek',
            title: 'anlamındaki kelime hangisidir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'make',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            firstWord: '',
            secondWord: 'decision',
            meaning: 'karar vermek',
            title: 'anlamındaki kelime hangisidir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'make',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            firstWord: '',
            secondWord: 'decision',
            meaning: 'karar vermek',
            title: 'anlamındaki kelime hangisidir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'make',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 4,
            firstWord: '',
            secondWord: 'decision',
            meaning: 'karar vermek',
            title: 'anlamındaki kelime hangisidir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'make',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 5,
            firstWord: '',
            secondWord: 'decision',
            meaning: 'karar vermek',
            title: 'anlamındaki kelime hangisidir?',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'make',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 7,
        type: 'definition',
        title: 'Kelime Tanımlama',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            wordType: 'verb',
            question: 'Birini sevindirmek, mutlu etmek için verilen şey',
            title: 'anlamındaki kelime hangisidir',
            active: true,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            wordType: 'noun',
            question: 'Birini sevindirmek, mutlu etmek için verilen şey',
            title: 'anlamındaki kelime hangisidir',
            active: false,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            wordType: 'verb',
            question: 'Birini sevindirmek, mutlu etmek için verilen şey',
            title: 'anlamındaki kelime hangisidir',
            active: false,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 8,
        type: 'phrase',
        title: 'İfade Tanımlama',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            wordType: 'verb',
            question: 'Birini sevindirmek, mutlu etmek için verilen şey',
            title: 'anlamındaki kelime hangisidir',
            active: true,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            wordType: 'noun',
            question: 'Birini sevindirmek, mutlu etmek için verilen şey',
            title: 'anlamındaki kelime hangisidir',
            active: false,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            wordType: 'verb',
            question: 'Birini sevindirmek, mutlu etmek için verilen şey',
            title: 'anlamındaki kelime hangisidir',
            active: false,
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 9,
        type: 'suffix',
        title: 'Kelime Türetme',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            title: 'Boş bırakılan alanı doğru seçenek ile doldurun!',
            type: 'verb',
            mainWord: 'present',
            mainWordMeaning: 'ikna etmek',
            extension: 'ing',
            extensionMeaning: 'sıfatları fiil yapar',
            word: '1convincing',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            title: 'Boş bırakılan alanı doğru seçenek ile doldurun!',
            type: 'verb',
            mainWord: 'present',
            mainWordMeaning: 'ikna etmek',
            extension: 'er',
            extensionMeaning: 'sıfatları fiil yapar',
            word: '2convincing',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            title: 'Boş bırakılan alanı doğru seçenek ile doldurun!',
            type: 'verb',
            mainWord: 'present',
            mainWordMeaning: 'ikna etmek',
            extension: 'ed',
            extensionMeaning: 'sıfatları fiil yapar',
            word: '3convincing',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            title: 'Boş bırakılan alanı doğru seçenek ile doldurun!',
            type: 'verb',
            mainWord: 'present',
            mainWordMeaning: 'ikna etmek',
            extension: 'ing',
            extensionMeaning: 'sıfatları fiil yapar',
            word: '4convincing',
            options: [
              {
                id: 0,
                key: 1,
                title: 'train',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'disability',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'enable',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 10,
        type: 'sentence-replace',
        title: 'Alternatif Kelime',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '1seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '2seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '3seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '4seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 11,
        type: 'recycle',
        title: 'Telafi Soruları',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '1seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 1,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '2seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 2,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '3seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          },
          {
            id: 3,
            question: ['Christophe Colomb discovered America', '', '1492.'],
            title: '4seçili kelimenin yerine hangi kelime yazılabilir',
            options: [
              {
                id: 0,
                key: 1,
                title: 'in',
                correctOption: false,
                selected: false
              },
              {
                id: 1,
                key: 2,
                title: 'on',
                correctOption: false,
                selected: false
              },
              {
                id: 2,
                key: 3,
                title: 'at',
                correctOption: true,
                selected: false
              }
            ]
          }
        ]
      },
      {
        id: 12,
        type: 'cloze',
        title: 'Boşluk Doldurma',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: null,
        questions: [
          {
            id: 0,
            title: 'fill 1'
          },
          {
            id: 1,
            title: 'fill 2'
          },
          {
            id: 2,
            title: 'fill 3'
          },
          {
            id: 3,
            title: 'fill 4'
          }
        ]
      },
      {
        id: 13,
        type: 'sound-pool',
        title: 'Ses Havuzu',
        text: 'Sesli alıştırmayı dinle. Duyduğun kelimeye karışılık gelen kelime kutusunu seç!',
        intro: [],
        question: 'Sesli alıştırmayı dinle. Sesini duyduğunuz kelimenin anlamı nedir?',
        questions: [
          {
            id: 0,
            answer: 'present'
          },
          {
            id: 1,
            answer: 'train'
          },
          {
            id: 2,
            answer: 'discover'
          },
          {
            id: 3,
            answer: 'row'
          }
        ],
        words: [
          {
            id: 0,
            title: 'convincing'
          },
          {
            id: 1,
            title: 'present'
          },
          {
            id: 2,
            title: 'train'
          },
          {
            id: 3,
            title: 'discover'
          },
          {
            id: 4,
            title: 'row'
          },
          {
            id: 5,
            title: 'pass away'
          },
          {
            id: 6,
            title: 'responsible'
          },
          {
            id: 7,
            title: 'disability'
          },
          {
            id: 8,
            title: 'discover'
          }
        ]
      }
    ]
  }
}

const mutations = {
  [SET_WORDY_CURRENT_ITEM] (state, value) {
    state.currentItem = value
  },
  [GET_WORDY_DATA] (state, value) {
    state.bars = value
  }
}

const actions = {
  [GET_WORDY_CURRENT_ITEM] ({ commit }, value) {
    commit(SET_WORDY_CURRENT_ITEM, value)
  },
  [GET_WORDY_DATA] ({ commit }, value) {
    commit(SET_WORDY_DATA, value)
  }
}

const getters = {
  getWordyCurrentItem (state) { return state.currentItem },
  getWordyData (state) { return state.questions }
}

export default {
  state,
  mutations,
  actions,
  getters
}
