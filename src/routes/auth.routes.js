export default [
  {
    path: '/giris-yap',
    name: 'Login',
    component: () => import(/* webpackChunkName: "auth" */ '../views/Auth/Login'),
    meta: {
      title: 'Giriş Yap',
      headerShow: false,
      toolbarVisible: false,
      requiresAuth: false
    }
  },
  {
    path: '/sifremi-sifirla',
    name: 'ForgotPassword',
    component: () => import(/* webpackChunkName: "auth" */ '../views/Auth/ForgotPassword'),
    meta: {
      title: 'Şifremi Sıfırla',
      headerShow: false,
      toolbarVisible: false,
      requiresAuth: false
    }
  },
  {
    path: '/sifre-yenile',
    name: 'ResetPassword',
    component: () => import(/* webpackChunkName: "auth" */ '../views/Auth/ResetPassword'),
    meta: {
      title: 'Şifremi Yenile',
      headerShow: false,
      toolbarVisible: false,
      requiresAuth: false
    }
  },
  {
    path: '/kaydol',
    name: 'Register',
    component: () => import(/* webpackChunkName: "auth" */ '../views/Auth/Register'),
    meta: {
      title: 'Kaydol',
      headerShow: false,
      toolbarVisible: false,
      requiresAuth: false
    }
  }
]
