import Vue from 'vue'

export const getHomeworks = () => Vue.$axios('GET', `/homeworks`)

export const filterHomeworks = data =>
  Vue.$axios('GET', `/homeworks?filter=${data.filter}&page=${data.page}`)
