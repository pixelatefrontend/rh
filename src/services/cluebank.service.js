import Vue from 'vue'

export const getAllClues = page => Vue.$axios('GET', `/cluebanks?_page=${page}`)

export const getClue = id => Vue.$axios('GET', `/cluebank/${id}`)

export const getCluebankInfo = () => Vue.$axios('GET', `/cluebank/info`)

export const getCluebankQuestions = () => Vue.$axios('GET', `/cluebank/question-types`)

export const getCluebankGrammars = () => Vue.$axios('GET', `/cluebank/grammars`)

export const getCluebankStrategies = () => Vue.$axios('GET', `/cluebank/strategies`)

export const getCluebankWords = () => Vue.$axios('GET', `/cluebank/words`)

export const favoriteClue = id => Vue.$axios('POST', `/cluebank/favorite/${id}`)

export const searchClues = str => Vue.$axios('GET', `/cluebank/search?q=${str}`)

export const getFavoriteClues = () => Vue.$axios('GET', `/cluebank/favorites`)
