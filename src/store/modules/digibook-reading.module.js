import Vue from 'vue'
import { notifier } from '../../common/helpers'
import {
  getDigibookItemToolData,
  saveReadingQuestionOption,
  showQuestionAnswer,
} from '../../services/digibook.service'
import { getContent } from '../helpers/digibook-actions.helper'

const readingItem = (id, type = 'question') => {
  let item = null
  if (type === 'paragraph') item = state.paragraph
  else item = state.questions.find(el => el.id === id)
  return item
}

const state = {
  data: null,
  questions: [],
  paragraph: null,
  contentId: null,
  loading: false,
  loaded: false,
}

const getters = {
  passage: state => state.paragraph,
  passageId: state => state.paragraph.paragraphId,
  hasImage: state => state.paragraph.image !== null,
  questions: state => state.questions,
  contentId: state => state.contentId,
  loading: state => state.loading,
  loaded: state => state.loaded,
}

const mutations = {
  setToolDataLoading: (state, data) => {
    let item = readingItem(data.itemId, data.itemType)
    item.toolLoading = data.status
  },

  setToolDataLoaded: (state, data) => {
    let item = readingItem(data.itemId, data.itemType)
    item.toolLoaded = data.status
  },

  setToolData: (state, data) => {
    let item = readingItem(data.itemId, data.itemType)
    console.log('item: ', item)
    item.toolData = data.result
  },

  setActiveTool: (state, data) => {
    let item = readingItem(data.itemId, data.itemType)
    let currentTool = item.activeTool
    item.activeTool = currentTool === data.tool ? '' : data.tool

    let tools = item.activeTools
    let isActive = tools.includes(data.tool)

    if (data.tool === 'question' || data.tool === '') return

    if (!isActive) tools.push(data.tool)
    else tools = tools.filter(el => el !== data.tool)

    Vue.set(item, 'activeTools', tools)
  },

  clearActiveTool: (state, data) => {
    let item = readingItem(data.itemId, data.itemType)
    item.activeTool = ''
  },

  setOption: (state, data) => {
    const question = readingItem(data.questionId)
    question.answerId = data.optionId
  },

  showQuestionAnswer: (state, data) => {
    const question = readingItem(data.itemId)
    const trueOption = question.options.find(el => el.correctOption === true)
    question.isLocked = true
    if (question.answerId === null) return
    question.answerStatus = question.answerId === trueOption.id
  },

  setData: (state, payload) => {
    state.paragraph = payload.paragraph
    Vue.set(state.paragraph, 'toolData', null)
    Vue.set(state.paragraph, 'toolLoading', null)
    Vue.set(state.paragraph, 'toolLoaded', null)
    Vue.set(state.paragraph, 'activeTool', '')
    Vue.set(state.paragraph, 'activeTools', [])

    let number = 1
    payload.data.forEach(q => {
      Vue.set(q, 'toolData', null)
      Vue.set(q, 'toolLoading', null)
      Vue.set(q, 'toolLoaded', null)
      Vue.set(q, 'activeTool', '')
      Vue.set(q, 'activeTools', [])
      Vue.set(q, 'number', number)
      number++
      state.questions.push(q)
    })
  },

  setContentId: (state, data) => (state.contentId = data),

  setLoading: (state, data) => (state.loading = data),

  setLoaded: (state, data) => (state.loaded = data),

  clearData: state => {
    state.questions.length = 0
    state.contentId = null
    state.paragraph = null
    state.loading = null
    state.loaded = null
  },
}

const actions = {
  fetchContent: getContent,

  handleParagraphTools: ({ commit }) => {
    commit('setParagraphTools')
  },

  handleActiveParagraphTool: ({ state, commit }, tool) => {
    const { activeTool } = state.shared.content.paragraph
    let item = activeTool === tool ? '' : tool
    commit('setActiveParagraphTool', item)
  },

  fetchParagraphToolData: async ({ state, commit, dispatch }, data) => {
    if (state.shared.content.paragraph.toolLoaded) {
      dispatch('handleActiveParagraphTool', data.toolName)
      return false
    }

    commit('setParagraphToolsLoaded', false)
    commit('setParagraphToolsLoading', true)

    try {
      const response = await getDigibookItemToolData(data)
      commit('setParagraphToolData', response.data)
      dispatch('handleActiveParagraphTool', data.toolName)
    } catch (error) {
      console.error(error)
      notifier.error('İçerik getirme başarısız!')
    } finally {
      commit('setParagraphToolsLoaded', true)
      commit('setParagraphToolsLoading', false)
    }
  },

  fetchToolData: async ({ commit, dispatch }, payload) => {
    let item = readingItem(payload.itemId, payload.itemType)

    if (payload.tool === 'answer') return dispatch('handleShowAnswer', payload)

    if (item.toolLoaded) return commit('setActiveTool', payload)

    commit('setToolDataLoading', { ...payload, status: true })

    const { status, data } = await getDigibookItemToolData(payload)

    if (status !== 200) return console.error(status, data)

    commit('setActiveTool', payload)
    commit('setToolDataLoading', { ...payload, status: false })
    commit('setToolDataLoaded', { ...payload, status: true })
    commit('setToolData', { ...payload, result: data })
  },

  handleShowAnswer: async ({ commit }, payload) => {
    console.log('payload: ', payload)
    try {
      const { data } = await showQuestionAnswer(payload)
      notifier.success(data.message)
      commit('showQuestionAnswer', payload)
    } catch (error) {
      console.error(error)
      notifier.error('Cevap görüntüleme başarısız!')
    }
  },

  handleOption: async ({ commit }, data) => {
    console.log(data)
    try {
      const response = await saveReadingQuestionOption(data)
      commit('setOption', data)
      notifier.success(response.data.message)
    } catch (error) {
      console.error(error)
      notifier.error('Cevap kaydetme başarısız!')
    }
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
