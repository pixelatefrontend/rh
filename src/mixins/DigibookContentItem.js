import Vue from 'vue'
import _ from 'lodash'

export default {
  props: {
    item: {
      type: Object,
      default: () => { },
    },
    tools: {
      type: Array,
      default: () => [],
    }
  },
  data() {
    return {
      isFetched: false,
      showAnswerClicked: false,
      showDescription: false,
      showTranslation: false,
      activeTool: {},
      toolWaiting: false,
      questionData: {
        words: [],
        notes: [],
        clues: [],
        optClues: [],
        myQuestions: null,
        otherQuestions: null,
        collocations: [],
        prepositions: [],
        description: '',
        translation: '',
        tags: [],
      },
      paragraphData: {
        title: '',
        subtitle: '',
        paragraphQuestions: [],
        notes: [],
        myQuestions: [],
        otherQuestions: [],
        words: [],
        collocations: [],
        prepositions: [],
      },
    }
  },
  computed: {
    toolAnswer() {
      return _.find(this.tools, ['type', 'answer'])
    },
  },
  methods: {
    closeTools(component) {
      if (component !== this) {
        this.showWords = false
        this.showTranslate = false
        this.activeTool = {}
      }
    },
    async getContentData() {
      // console.log('getContentData', this.itemType, 'this.toolWaiting', this.toolWaiting)
      const contentId = this.$route.params.contentId
      let itemId = 0

      if (this.itemType === 'paragraph') {
        itemId = this.paragraphId
      } else {
        itemId = this.item.question_content_id || this.item.id
      }

      const response = await Vue.$axios(
        'GET',
        `/digibook/digibook-item/${contentId}/${itemId}?type=${this.itemType}`,
      )
      const data = _.get(response, 'data')
      const question = _.head(_.get(data, 'questions', []))
      if (this.itemType === 'questionContent') {
        console.log(response.data)
        this.questionData.words = _.get(question, 'words', [])
        this.questionData.notes = _.get(data, 'notes', [])
        this.questionData.clues = _.get(data, 'clues', [])
        this.questionData.optClues = response.data.questions[0].options
        this.questionData.collocations = _.get(question, 'collocations', [])
        this.questionData.prepositions = _.get(question, 'prepositions', [])
        this.questionData.myQuestions = _.get(data, 'myQuestions', [])
        this.questionData.otherQuestions = _.get(data, 'otherQuestions', [])
        this.questionData.tags = _.get(question, 'question_tags', [])
        this.questionData.translation = _.get(data, 'translation', '')
        this.questionData.description = _.get(data, 'description', '')
      } else if (this.itemType === 'sentence') {
        this.questionData.words = _.get(data, 'words', [])
        this.questionData.collocations = _.get(data, 'collocations', [])
        this.questionData.prepositions = _.get(data, 'prepositions', [])
        this.questionData.translation = _.get(data, 'translate', [])
        this.questionData.notes = _.get(data, 'notes', [])
        this.questionData.myQuestions = _.get(data, 'myQuestions', [])
        this.questionData.otherQuestions = _.get(data, 'otherQuestions', [])
        this.questionData.clues = _.get(data, 'clues', [])
      } else if (this.itemType === 'paragraph') {
        console.log('digicontentitem:', response.data)
        this.paragraphData.notes = _.get(data, 'notes', [])
        this.paragraphData.myQuestions = _.get(data, 'myQuestions', [])
        this.paragraphData.otherQuestions = _.get(data, 'otherQuestions', [])
        this.paragraphData.words = _.get(data, 'words', [])
        this.paragraphData.prepositions = _.get(data, 'prepositions', [])
        this.paragraphData.collocations = _.get(data, 'collocations', [])
      }
      this.toolWaiting = false
    },
    mapCollocations(collocations) {
      const result = []
      _.forEach(Object.keys(collocations), key => {
        result.push({
          id: key,
          title: collocations[key].title,
          translation: collocations[key].translation,
          added: false,
        })
      })
      return result
    },
    mapPrepositions(prepositions) {
      const result = []
      _.forEach(Object.keys(prepositions), key => {
        result.push({
          id: key,
          title: prepositions[key].title,
          translation: prepositions[key].translation,
          added: false,
        })
      })
      return result
    },
    expandAndCollapse(toolType) {
      if (toolType === 'answer' && !this.showAnswer) {
        this.showAnswer = true
      }
      this.showInfo = toolType === 'description_and_tags' ? !this.showInfo : false
      this.showTranslate = toolType === 'root_and_translate' ? !this.showTranslate : false
      this.showNoteModal = toolType === 'notes' ? !this.showNoteModal : false
      this.showQuestionsModal =
        toolType === 'is_question_permission' ? !this.showQuestionsModal : false
      this.showWords = toolType === 'word_list' ? !this.showWords : false
    },
    async toolClick(tool) {
      console.log('toolClick', tool.type, 'this.toolWaiting', this.toolWaiting)

      this.expandAndCollapse(tool.type)
      if (_.isEmpty(this.parentObject) && !this.isFetched) {
        if (tool.type !== 'answer') {
          this.toolWaiting = true
        }
        await this.getContentData()
        this.isFetched = true
      }
      if (tool.type === 'root_and_translate') {
        this.activeTool = tool
        // this.activeTool = this.showTranslation ? tool : {}
        // if (this.showTranslate) {
        //   this.$emit('close-other-question-words', this)
        // }
      }

      if (tool.type === 'answer') {
        console.log('paragraph id: ', this.itemId, this.paragraphId, this.itemType)
        if (this.itemType === 'paragraph') {
          Vue.$axios(
            'POST',
            `/digibook/show-answer/${this.$route.params.contentId}?questionId=${this.questionItem.id}&paragraphId=${this.paragraphId}`
          )
          /* this.tests.forEach(question => {
            Vue.$axios(
              'POST',
              `/digibook/show-answer/${this.$route.params.contentId}?questionId=${question.id}&paragraphId=${this.paragraphId}`,
            )
          }) */
        } else {
          Vue.$axios(
            'POST',
            `/digibook/show-answer/${this.$route.params.contentId}?questionId=${this.question.id}`,
          )
          this.question.is_locked = true

          console.log(this.question)
          // this.showAnswer = !this.showAnswer
          if (this.currentKey !== null) {
            if (this.question.options[this.currentKey].correctOption) {
              this.playCorrect()
              this.status = 'success'
            } else {
              this.playWrong()
              this.status = 'error'
            }
          }
        }
        console.log('answer cliccked')
        this.showAnswerClicked = true
      }

      if (tool.type === 'description_and_tags') {
        // this.showDescription = !this.showDescription
        // this.showAnswer = false
        // this.showInfo = !this.showInfo
        // this.toolClick(_.find(this.tools, ['type', 'answer']))
      }

      // if (tool.type === 'notes') {
      //   this.showNoteModal = true
      // }

      // if (tool.type === 'is_question_permission') {
      //   this.showQuestionsModal = true
      // }

      if (tool.type === 'word_list' && !this.showResult) {
        // this.showWords = !this.showWords
        // this.activeTool = this.showWords ? tool : {}
        /* this.showTranslate = false
        if (this.showWords) {
          this.$emit('close-other-question-words', this)
        } */
        // setTimeout(() => {
        //   if (this.showWords) window.scrollTo(0, this.$refs.digiWords.offsetTop)
        // }, this.showWordsDuration)
      }
    },
    async playWrong() {
      await new Audio('/wrong.mp3').play()
    },
    async playCorrect() {
      await new Audio('/correct.mp3').play()
    },
    handleChangeMyQuestion(questionId) {
      let myQuestions = JSON.parse(JSON.stringify(this.questionData.myQuestions))
      myQuestions = myQuestions.filter(e => e.id !== questionId)
      this.questionData = { ...this.questionData, ...{ myQuestions } }
    },
  },
}
