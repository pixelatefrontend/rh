import ExamsIndex from '../views/Exams/index.vue'
import ExamsList from '../views/Exams/ExamsList.vue'
import ExamsDetail from '../views/Exams/ExamsDetail.vue'
import ExamsReport from '../views/Exams/ExamsReport.vue'

export default [{
  path: '/deneme',
  // component: () => import(/* webpackChunkName: "exams" */ '../views/Exams'),
  component: ExamsIndex,
  children: [
    {
      path: 'list',
      redirect: '/deneme'
    },
    {
      path: 'detay',
      redirect: '/deneme'
    },
    {
      path: 'rapor',
      redirect: '/deneme'
    },
    {
      path: ':other',
      redirect: '/deneme'
    },
    {
      path: '',
      name: 'ExamsList',
      // component: () => import(/* webpackChunkName: "exams" */ '../views/Exams/ExamsList'),
      component: ExamsList,
      meta: {
        title: 'Deneme',
        requiresAuth: true,
        headerShow: true,
        toolbarVisible: true,
        hasTour: true,
        tourName: 'examsTour',
        noteName: 'e_deneme'
      }
    },
    {
      path: 'detay/:detailId',
      name: 'ExamsDetail',
      // component: () => import(/* webpackChunkName: "exams" */ '../views/Exams/ExamsDetail'),
      component: ExamsDetail,
      meta: {
        requiresAuth: true,
        headerShow: false,
        toolbarVisible: true,
        noteName: 'e_deneme'
      }
    },
    {
      path: 'rapor/:detailId',
      name: 'ExamsReport',
      // component: () => import(/* webpackChunkName: "exams" */ '../views/Exams/ExamsReport'),
      component: ExamsReport,
      meta: {
        requiresAuth: true,
        headerShow: false,
        toolbarVisible: true,
        noteName: 'e_deneme'
      }
    }
  ]
}]
