import Vue from 'vue'

export const getAllOrders = () => Vue.$axios('GET', `/account/orders`)

export const provideAccess = code => Vue.$axios('POST', `/account/access-code?code=${code}`)

export const shareDiscountEmail = (email, coupon) => Vue.$axios('POST', `/account/share-discount-code?type=email&email=${email}&couponId=${coupon}`)

export const shareDiscountSms = (phone, coupon) => Vue.$axios('POST', `/account/share-discount-code?type=sms&phone_number=${phone}&couponId=${coupon}`)

export const giveProduct = payload => Vue.$axios('POST', `/account/give-product?email=${payload.email}&basketProductId=${payload.basket}`)

export const createDiscountCode = () => Vue.$axios('POST', `/account/discounts-generate-code`)
