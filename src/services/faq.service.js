import Vue from 'vue'

export const getFaqs = () => Vue.$axios('GET', `/faq`)

export const askFaq = payload =>
  Vue.$axios('POST', `/faq/ask?question=${payload.question}&categoryId=${payload.id}`)

export const searchFaq = term => Vue.$axios('GET', `/faq/search?q=${term}`)
