import Vue from 'vue'

const getters = {
  badges: state => state.badges,
  loading: state => state.loading,
  loaded: state => state.loaded,
  activeBadges: state =>
    state.badges ? state.badges.filter(b => !state.displayedItems.includes(b.id)) : []
}

const actions = {
  getBadges: ({ commit }) => {
    commit('SET_LOADING', true)
    Vue.axios
      .get(`/badges/new`)
      .then(response => {
        commit('SET_BADGES', response.data.new_badges)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  setDisplayed: ({ commit }, id) => {
    commit('SET_DISPLAYED', id)
  }
}

const mutations = {
  SET_BADGES(state, badges) {
    const $state = state
    $state.badges = $state.badges ? $state.badges.concat(badges) : badges
  },
  SET_DISPLAYED(state, id) {
    const $state = state
    const $displayedItems = JSON.parse(JSON.stringify($state.displayedItems))
    $displayedItems.push(id)
    $state.displayedItems = $displayedItems
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  }
}

const state = {
  badges: null,
  loading: null,
  loaded: null,
  displayedItems: []
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
