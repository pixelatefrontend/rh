let timeout = null

const delaySearch = (callback, delay = 1000) => {
  clearTimeout(timeout)
  timeout = setTimeout(() => {
    callback()
  }, delay)
}

export default delaySearch
