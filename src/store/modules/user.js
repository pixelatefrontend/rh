import Vue from 'vue'

import { LOGIN, LOGOUT, GET_USER } from '@/store/actions.type'
import {
  SET_TOKEN,
  UNSET_TOKEN,
  SET_USER_INFO,
  SET_NOTIFICATIONS,
} from '@/store/mutations.type'

import { getToken, setToken, removeToken } from '@/common/auth'
import {
  SET_USER_LOADED,
  SET_USER_DATA,
  LOGIN_ACTION_STARTED,
  SET_HOMEWORKS_STATUS,
  SET_NEW_ANSWER,
  SET_USER_APPS,
  SET_USER_FAVORITE_APPS,
  TOGGLE_FAVORITE_APP,
  SET_FIRST_TIME,
  SET_PHONE_CONFIRMED,
  SET_APPS_LOAD,
  SET_WORDBOOK_INTRO,
  SET_HOME_TOUR,
  SET_SOUNDS_ON,
} from '../mutations.type'
import {
  LOGIN_SOCIAL,
  TOGGLE_FAVORITE,
  TOGGE_FIRST_TIME,
  COMPLETE_LOGIN,
  HANDLE_WORDBOOK_INTRO,
  HANDLE_SOUNDS,
} from '../actions.type'
import { deleteCookie, notifier } from '../../common/helpers'
import { login, logout } from '../../services/auth.service'
import {
  profile,
  favoriteApp,
  unfavoriteApp,
  toggleFirstTime,
  toggleWordbookIntro,
  toggleSounds,
} from '../../services/user.service'

const state = {
  signedIn: false,
  token: '',
  orders: {},
  addresses: [],
  user: null,
  notifications: [],
  apps: [],
  userLoaded: false,
  loginAction: false,
  homeworksCompleted: true,
  newAnswer: false,
  firstTimer: null,
  wordbookIntro: null,
  favoriteApps: null,
  homeTour: null,
  usersApps: [],
  appMap: [
    {
      name: 'lesson',
      link: 'ders',
    },
    {
      name: 'digibook',
      link: 'digibook',
    },
    {
      name: 'wordy',
      link: 'wordy',
    },
    {
      name: 'reading',
      link: 'okuma',
    },
    {
      name: 'dictionary',
      link: 'sozluk',
    },
    {
      name: 'test',
      link: 'test',
    },
    {
      name: 'exam',
      link: 'deneme',
    },
    {
      name: 'cluebank',
      link: 'ipucu-havuzu',
    },
    {
      name: 'material',
      link: 'dokuman',
    },
  ],
  phoneConfirmed: false,
  appsListLoaded: false,
  soundsOn: true,
}

const mutations = {
  [SET_USER_APPS](state, data) {
    state.usersApps = data
  },
  [SET_TOKEN](state, token) {
    state.token = token
  },
  [UNSET_TOKEN](state) {
    state.token = ''
  },
  [SET_USER_INFO](state, data) {
    state.user = data
    state.apps = data.apps
  },

  [SET_USER_DATA](state, data) {
    state.user = data
  },

  [SET_NOTIFICATIONS](state, notifications) {
    state.notifications = notifications
  },
  [SET_USER_LOADED](state, data) {
    state.userLoaded = data
  },

  [LOGIN_ACTION_STARTED](state, data) {
    state.loginAction = data
  },

  [SET_HOMEWORKS_STATUS](state, data) {
    state.homeworksCompleted = data
  },

  [SET_NEW_ANSWER](state, data) {
    state.newAnswer = data
  },

  [SET_USER_FAVORITE_APPS](state) {
    state.favoriteApps = state.usersApps.filter(el => el.isFavorited === true)
  },

  [TOGGLE_FAVORITE_APP](state, app) {
    let item = state.usersApps.find(el => el === app)
    let itemFavorited = state.favoriteApps.indexOf(app) > -1
    let itemFavIndex = state.favoriteApps.indexOf(app)
    itemFavorited
      ? state.favoriteApps.splice(itemFavIndex, 1)
      : state.favoriteApps.push(app)
    item.isFavorited = !item.isFavorited
  },

  [SET_FIRST_TIME](state, data) {
    state.firstTimer = data
  },

  [SET_WORDBOOK_INTRO](state, data) {
    state.wordbookIntro = data
  },

  [SET_HOME_TOUR](state, data) {
    state.homeTour = data
  },

  [SET_PHONE_CONFIRMED](state, data) {
    state.phoneConfirmed = data
  },

  [SET_APPS_LOAD](state, status) {
    state.appsListLoaded = status
  },

  [SET_SOUNDS_ON](state, data) {
    state.soundsOn = data
  },
}

const actions = {
  [TOGGE_FIRST_TIME]({ commit }, data) {
    commit(SET_FIRST_TIME, data)
    toggleFirstTime(data)
  },

  [HANDLE_WORDBOOK_INTRO]({ commit }, data) {
    commit(SET_WORDBOOK_INTRO, data)
    toggleWordbookIntro(data)
  },

  [TOGGLE_FAVORITE]({ commit }, app) {
    commit(TOGGLE_FAVORITE_APP, app)
    app.isFavorited
      ? favoriteApp(app.name).then(r => console.log(r))
      : unfavoriteApp(app.name).then(r => console.log(r))
  },

  [LOGIN_SOCIAL]({ commit }, token) {
    Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    window.localStorage.setItem('temporaryToken', token)
  },

  async [LOGIN]({ commit, dispatch }, { email, password, remember }) {
    const result = await login(email, password, remember)
    if (result.data.token) {
      Vue.axios.defaults.headers.common['Authorization'] =
        'Bearer ' + result.data.token
      window.localStorage.setItem('temporaryToken', result.data.token)
      return {
        success: true,
        skip2fa: result.data.skip2fa,
        token: result.data.token,
      }
    } else {
      return { success: false, message: result.data.error }
    }
  },

  [COMPLETE_LOGIN]({ commit, dispatch }, token) {
    window.localStorage.removeItem('temporaryToken')
    Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
    commit(SET_TOKEN, token)
    setToken(token)
  },

  [LOGOUT]({ commit, dispatch }) {
    deleteCookie('platform-token')
    deleteCookie('skip2fa')
    deleteCookie('lastAuthenticatedUser')
    return logout().then(({ data }) => {
      delete Vue.axios.defaults.headers.common['Authorization']
      commit(UNSET_TOKEN)
      commit(SET_USER_LOADED, false)
      commit(SET_APPS_LOAD, false)
      commit(SET_USER_DATA, null)
      removeToken()
      dispatch('profile/clearUserProfile')
    })
  },
  [GET_USER]({ commit, dispatch }) {
    profile().then(result => {
      console.log('res: ', result)
      commit(SET_USER_INFO, result.data)
      commit(SET_FIRST_TIME, result.data.has_introduction)
      commit(SET_HOME_TOUR, result.data.has_tour)
      commit(SET_WORDBOOK_INTRO, result.data.has_word_book_tour)
      commit(SET_HOMEWORKS_STATUS, result.data.homeworksCompleted)
      commit('questions/setUnreadAnswerCount', result.data.newAnswer)
      commit(SET_USER_LOADED, true)
      commit(SET_USER_APPS, result.data.apps)
      commit(SET_USER_FAVORITE_APPS)
      commit(SET_APPS_LOAD, true)
      commit(SET_PHONE_CONFIRMED, result.data.isPhoneConfirmed)
      commit(SET_SOUNDS_ON, result.data.isSoundOn)
    })
  },

  [HANDLE_SOUNDS]({ commit }, data) {
    commit(SET_SOUNDS_ON, data)
    let message = data
      ? 'Ses efektleri açıldı!'
      : 'Ses efektleri kapatıldı!'
    notifier.success(message)
    toggleSounds(data)
  },
}

const getters = {
  hasPhone: state => state.user.hasPhone,
  appsListLoaded: state => state.appsListLoaded,
  usersApps: state => state.usersApps,
  favoriteApps: state => state.favoriteApps,
  hasMaxApps: state => state.favoriteApps.length === 3,
  fetchingData: state => state.loginAction,
  token(state) {
    return getToken()
  },
  isAuthenticated(state) {
    return state.token || getToken()
  },
  notifications(state) {
    return state.notifications
  },
  userFullName(state) {
    return state.user.fullname
  },
  userLoaded: state => state.userLoaded,
  userAppsLoaded: state => state.userLoaded,
  userAvatar: state => state.user.avatar,
  userData: state => state.user,
  userAppsList: state => state.apps,
  favoritedApps: state => {
    return state.apps.filter(item => item.isFavorited === true)
  },
  userBadge(state) {
    return state.user.badge
  },
  productCount: state => state.user.ownedProductCount,
  homeworksCompleted: state => state.homeworksCompleted,
  newAnswer: state => state.newAnswer,
  firstTimer: state => state.firstTimer,
  hasHomeTour: state => state.homeTour,
  hasWordbookIntro: state => state.wordbookIntro,
  phoneConfirmed: state => state.phoneConfirmed,
  soundsOn: state => state.soundsOn,
}

export default {
  state,
  mutations,
  actions,
  getters,
}
