import Vue from 'vue'
import { orderBy } from 'lodash'
import { generateStats, findCurrent } from '../../common/test-detail.helper'
import worker from '../../common/test-detail.worker'

const getters = {
  details: state => state.details,
  testName: state => state.details.title,
  loading: state => state.loading,
  loaded: state => state.loaded,
  questionList: state => state.details.questions,
  statistics: state => generateStats(state.details),
  currentQuestion: state => findCurrent(state.details),
  isDone: state => state.isDone,
  isPaused: state => state.isPaused,
}

const actions = {
  /**
   * get detail data and then start worker
   * @param {id:number} params
   */
  getDetails: ({ commit }, params) => {
    commit('RESET_STATE')
    commit('SET_LOADING', true)
    Vue.axios
      .get(`tests/${params.id}`)
      .then(response => {
        commit('SET_DETAILS', response.data)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        worker.runWorker()
        commit('UPDATE_PAUSED', false)
      })
      .catch(() => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },

  /**
   * @param {object} params
   */
  updateQuestion: ({ commit }, params) => {
    commit('UPDATE_QUESTION', params)
  },

  /**
   * @param {number} pointer
   */
  setPointer: ({ commit, getters, dispatch }, pointer) => {
    // $current is old current
    const $current = JSON.parse(JSON.stringify(getters.currentQuestion))
    dispatch('syncQuestion', $current)
    worker.stopWorker()
    commit('SET_POINTER', pointer)
    worker.runWorker()
  },

  /**
   * call mutation for increase current question duration
   * @noparam
   */
  setDurationDetail: ({ commit }) => {
    commit('SET_DURATION_DETAIL')
  },

  /**
   * call mutation for increase current question duration
   * @param {boolean} status
   */
  setPaused: ({ commit }, status) => {
    if (status) worker.stopWorker()
    else worker.runWorker()
    commit('UPDATE_PAUSED', status)
  },

  /**
   * call mutation for increase current question duration
   * @param {boolean} status
   */
  setDone: ({ commit }, status) => {
    if (status) worker.stopWorker()
    else worker.runWorker()
    commit('UPDATE_DONE', status)
  },

  /**
   * direction can be 'next' or 'prev'
   * @param {string} direction
   */
  navigate: ({ dispatch, state }, direction) => {
    const $index = state.details.questions.findIndex(item => item.id === state.details.pointer)
    let $pointer = null
    if (direction === 'next') {
      $pointer = state.details.questions[$index + 1].id
    } else if (direction === 'prev') {
      $pointer = state.details.questions[$index - 1].id
    } else {
      return false
    }
    dispatch('setPointer', $pointer)
  },

  /**
   * @param {object} question
   */
  async syncQuestion({ commit, state, dispatch }, question) {
    let $return = true
    const $queue = JSON.parse(JSON.stringify(state.queue))
    commit('SET_QUEUE', [])
    const $item = {
      type: 'waiting', // available types: waiting, success, fail
      detail: {
        question,
        id: state.details.id,
        pointer: state.details.pointer,
      },
    }
    $queue.push($item)
    await $queue.forEach(async item => {
      if (!(await dispatch('setQuestion', item))) $return = false
    })
    return $return
  },

  /**
   * @param {object} item
   */
  async setQuestion({ commit, state, dispatch }, item) {
    let $return = true
    const postdata = {
      testId: item.detail.id,
      questionId: item.detail.question.id,
      selectedOptionId: item.detail.question.selectedAnswer,
      duration: item.detail.question.duration,
      state: item.detail.question.state,
      pointer: item.detail.pointer,
      isPinned: item.detail.question.isPinned,
    }
    await Vue.axios
      .post(`tests/answer`, postdata)
      .then()
      .catch(() => {
        const $queue = JSON.parse(JSON.stringify(state.queue))
        $queue.push({ ...item, type: 'fail' })
        commit('SET_QUEUE', $queue)
        $return = false
      })
      .finally(() => {
        dispatch('queueChecker')
      })
    return $return
  },

  /**
   * @noparam
   */
  queueChecker({ commit, state }) {
    if (state.queue.filter(o => o.type === 'fail').length >= 3) {
      commit('SET_LOADED', false)
    }
  },

  /**
   * @noparam
   * @returns {boolean} isSuccess
   */
  async sendFinishReq({ state }) {
    let $return = true
    await Vue.axios
      .post(`tests/${state.details.id}`, { id: state.details.id })
      .then(() => {
        $return = true
      })
      .catch(() => {
        $return = false
      })
    return $return
  },

  /**
   * @noparam
   * @returns {boolean} isSuccess
   */
  async setFinish({ commit, dispatch, getters }) {
    let $return = true
    const $current = JSON.parse(JSON.stringify(getters.currentQuestion))
    if (!(await dispatch('syncQuestion', $current))) {
      $return = false
      commit('SET_LOADED', false)
    } else {
      if (!(await dispatch('sendFinishReq'))) {
        $return = false
        commit('SET_LOADED', false)
      }
    }
    return $return
  },
}

const mutations = {
  SET_DETAILS(state, details) {
    const $state = state
    details.questions = orderBy(details.questions, 'order')
    $state.details = details
  },
  SET_DURATION_DETAIL(state) {
    const index = state.details.questions.findIndex(item => item.id === state.details.pointer)
    state.details.questions[index].duration =
      parseInt(state.details.questions[index].duration, 10) + 1000
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_QUEUE(state, queue) {
    const $state = state
    $state.queue = queue
  },
  SET_POINTER(state, pointer) {
    const $state = state
    $state.details.pointer = pointer
  },
  UPDATE_QUESTION(state, params) {
    const $details = JSON.parse(JSON.stringify(state.details))
    const index = $details.questions.findIndex(item => item.id === $details.pointer)
    $details.questions[index] = { ...state.details.questions[index], ...params }
    state.details = $details
  },
  UPDATE_PAUSED(state, status) {
    const $state = state
    $state.isPaused = status
  },
  UPDATE_DONE(state, status) {
    const $state = state
    $state.isDone = status
  },
  RESET_STATE(state) {
    const $state = state
    $state.details = null
    $state.loading = false
    $state.loaded = false
    $state.isDone = false
    $state.isPaused = true
    $state.queue = []
  },
}

const state = {
  details: null,
  loading: false,
  loaded: false,
  isDone: false,
  isPaused: true,
  queue: [],
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
