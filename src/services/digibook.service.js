import Vue from 'vue'

export const digibooks = (group, page) =>
  Vue.$axios('GET', `/digibook?group_id=${group}&page=${page}`)

export const group = id => Vue.$axios('GET', `/digibook?group_id=${id}`)

export const digibookInfo = () => Vue.$axios('GET', `/digibook/info`)

export const info = () => Vue.$axios('GET', `/digibook/info`)

export const digibook = id => Vue.$axios('GET', `/digibook/${id}`)

export const getDigibook = id => Vue.$axios('GET', `/digibook/${id}`)

export const digibookDownload = id =>
  Vue.$axios('POST', `/digibook/${id}/download`)

export const getDigibookContent = id =>
  Vue.$axios('GET', `/digibook/content/${id}`)

export const getDigibookContentNew = id =>
  Vue.$axios('GET', `/digibook/content/${id}?new=1`)

export const getDigibookUnitContent = id =>
  Vue.$axios('GET', `/digibook/unit/${id}`)

export const getDigibookItemData = data =>
  Vue.$axios(
    'GET',
    `/digibook/digibook-item/${data.contentId}/${data.paragraphId}?type=${data.type}`
  )

export const saveAnswerParagraph = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-question/${data.contentId}?question_id=${data.questionId}&answer_id=${data.answerId}&paragraph_id=${data.paragraphId}`
  )

export const saveAnswerTestItem = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-question/${data.contentId}?question_id=${data.questionId}&question_translation=${data.body}`
  )

export const exercises = id => Vue.$axios('GET', `/digibook/unit/${id}`)

export const progress = progress =>
  Vue.$axios('GET', `/digibooks?_progress=${progress}`)

export const groups = (group, page) =>
  Vue.$axios('GET', `/digibooks?groups.name=${group}&_page=${page}`)

export const list = filters =>
  Vue.$axios(
    'GET',
    `/digibook?group_id=${filters.groupId}&page=${filters.page}&done=${filters.isFinished}&direction=${filters.orderType}&progress=${filters.finishType}`
  )

export const listByTime = filters =>
  Vue.$axios(
    'GET',
    `/digibook?group_id=${filters.groupId}&page=${filters.page}&done=${filters.isFinished}&direction=${filters.orderType}`
  )

export const listByCompletion = filters =>
  Vue.$axios(
    'GET',
    `/digibook?group_id=${filters.groupId}&page=${filters.page}&done=${filters.isFinished}&progress=${filters.finishType}`
  )

export const saveTableAnswer = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-table/${data.contentId}?key=${data.answerId}&value=${data.answerData}`
  )

export const saveTranslateAnswer = (contentId, data) =>
  Vue.$axios('POST', `/digibook/answer-translation/${contentId}`, { ...data })

export const saveTestQuestionOption = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-question/${data.contentId}?question_id=${data.questionId}&answer_id=${data.optionId}`
  )

export const saveReadingQuestionOption = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-question/${data.contentId}?question_id=${data.questionId}&answer_id=${data.optionId}&paragraph_id=${data.paragraphId}`
  )

export const saveMatchingPair = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-match/${data.contentId}?key=${data.value}&value=1`
  )
export const getDigibookItemToolData = data =>
  Vue.$axios(
    'GET',
    `/digibook/digibook-item/${data.contentId}/${data.itemId}?type=${data.itemType}`
  )
export const saveTestUserTranslation = data =>
  Vue.$axios(
    'POST',
    `/digibook/answer-question/${data.contentId}?question_id=${data.questionId}&question_translation=${data.body}`
  )

export const clearMatchedList = id =>
  Vue.$axios('POST', `/digibook/clear-match/${id}`)

export const showQuestionAnswer = data => {
  let endpoint = `/digibook/show-answer/${data.contentId}?questionId=${data.questionId}`

  if (data.paragraphId) endpoint += `&paragraphId=${data.paragraphId}`

  return Vue.$axios('POST', endpoint)
}
