import Vue from 'vue'
import { orderBy } from 'lodash'
import { manipulationListWithFilter, limit } from '../../common/helpers'

const getters = {
  list: state =>
    state.loaded
      ? limit(
        orderBy(
          manipulationListWithFilter(state.tests, state.filters).length > 0
            ? manipulationListWithFilter(state.tests, state.filters).concat(state.notifications)
            : [],
          'order',
        ),
        state.limit,
      )
      : [],
  types: state => state.types,
  questionTypes: state => state.questionTypes,
  notifications: state => state.notifications,
  loading: state => state.loading,
  loaded: state => state.loaded,
  limit: state => state.limit,
  filters: state => state.filters,
}

const actions = {
  getList: ({ commit }) => {
    /**
     * The following commit code has been added.
     * There was a problem like; when you went to another page after filtering
     * and returned to the listing screen, the filter was reset,
     * but the filtered content was displayed.
     * Calling this action every time the user enters the page.
     * So we reset the filter here.
     * So every time the user enters the page, they will see the unfiltered content.
    */
    commit('SET_FILTERS', {
      groupId: -1,
      typeId: -1,
      isDone: -1,
    })

    commit('SET_LOADING', true)
    //  FIXME:
    Vue.axios
      .get(`tests?per_page=99999`)
      .then(response => {
        commit('SET_TESTS', response.data.tests)
        commit('SET_TYPES', response.data.types)
        commit('SET_QUESTION_TYPES', response.data.questionTypes)
        commit('SET_NOTIFICATIONS', response.data.notifications ? response.data.notifications : [])
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
      })
      .catch(() => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  setLimit: ({ commit }, limit) => {
    commit('SET_LIMIT', limit)
  },
  setFilters: ({ commit }, filters) => {
    commit('SET_FILTERS', filters)
  },
}

const mutations = {
  SET_TESTS(state, tests) {
    const $state = state
    $state.tests = tests
  },
  SET_TYPES(state, types) {
    const $state = state
    $state.types = types
  },
  SET_QUESTION_TYPES(state, questionTypes) {
    const $state = state
    $state.questionTypes = questionTypes
  },
  SET_NOTIFICATIONS(state, notifications) {
    const $state = state
    $state.notifications = notifications
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_LIMIT(state, limit) {
    const $state = state
    $state.limit = limit
  },
  SET_FILTERS(state, filters) {
    const $state = state
    $state.filters = filters
  },
}

const state = {
  tests: null,
  types: null,
  questionTypes: null,
  notifications: null,
  loading: false,
  loaded: false,
  limit: 10,
  filters: {
    questionTypes: -1,
    groupId: -1,
    typeId: -1,
    isDone: -1,
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
