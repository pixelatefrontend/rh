import Vue from 'vue'

const state = {
  tools: null,
}

const getters = {
  tools: state => state.tools,
  availableTools: state => state.tools.filter(el => el.visible === true),
}

const mutations = {
  setTools: (state, data) => {
    state.tools = data
    state.tools.map(el => {
      Vue.set(el, 'visible', el.status)
    })
  },
}

const actions = {
  handleTools: ({ commit }, tools) => {
    console.log(tools)
    commit('setTools', tools)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
