import Vue from 'vue'

export const getPlatformApps = () => Vue.$axios('GET', `/apps`)

export const getWelcomeMessages = () => Vue.$axios('GET', `/welcome-messages`)

export const getCommunities = () => Vue.$axios('GET', `/communities`)

export const shareCommunity = data =>
  Vue.$axios(
    'POST',
    `/communities/send?linkId=${data.linkId}&method=${data.method}`
  )

export const sendFeedback = data =>
  Vue.$axios(
    'POST',
    `/feedback?message=${data.message}&is_public=${data.isPublic}`
  )

export const getGlobalSearchResults = param =>
  Vue.$axios('GET', `/global-search?q=${param}`)

export const getTranslatableStatus = (contentType, contentId) =>
  Vue.$axios(
    'GET',
    `/check-fast-translation-permission?contentType=${contentType}&contentId=${contentId}`
  )

export const checkTranslatableStatus = data =>
  Vue.$axios(
    'GET',
    `/check-fast-translation-permission?contentType=${data.contentType}&contentId=${data.contentId}`
  )

export const getCountries = () => Vue.$axios('GET', `/countries`)

export const checkConnection = () => Vue.$axios('GET', `/check-connection`)
