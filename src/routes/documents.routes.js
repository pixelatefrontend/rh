export default [
  {
    path: '/dokuman',
    component: () => import(/* webpackChunkName: "documents" */ '../views/Documents'),
    children: [
      {
        path: '',
        name: 'DocumentsList',
        component: () => import(/* webpackChunkName: "documents" */ '../views/Documents/DocumentsList'),
        meta: {
          title: 'Doküman',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: true,
          tourName: 'documentsTour',
          noteName: 'resource'
        }
      },
      {
        path: ':id',
        name: 'DocumentsDetail',
        component: () => import(/* webpackChunkName: "documents" */ '../views/Documents/DocumentsDetail'),
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'resource'
        }
      }
    ]
  }
]
