import store from '../store'
import worker from './deneme-detail.worker'
import { millisToMinutesAndSeconds } from './helpers'

/**
 * @param {object} details
 * @returns {number} countAnswered
 */
const countAnswered = (details) => details.questions.filter(question => question.selectedAnswer).length

/**
 * @param {object} details
 * @returns {number} countSkip
 */
// const countSkip = (details) => details.questions.filter(question => question.isPinned === true).length

/**
 * @param {object} details
 * @returns {number} countRemaining
 */
const countRemaining = (details) => details.questions.length - countAnswered(details)

/**
 * milisecond return
 * @param {object} details
 * @returns {number} total
 */
const calcPassing = (details) => {
  let total = 0
  details.questions.forEach(question => {
    total += parseInt(question.duration, 10)
  })
  return total
}

/**
 * milisecond return
 * @param {object} details
 * @returns {number} remaining
 */
const calcRemaining = (details) => {
  const passing = calcPassing(details)
  const duration = parseInt(details.examDuration, 10)
  const remaining = duration - passing
  if (remaining <= 0) {
    worker.stopWorker()
    store.dispatch('denemeDetail/sendFinishReq')
  }
  return remaining
}

/**
 * milisecond return
 * @param {object} details
 * @returns {number} spended
 */
const calcSpended = (details) => {
  const countQuestion = countAnswered(details)
  const spended = countQuestion ? calcPassing(details) / countQuestion : 0
  return spended
}

/**
 * @param {object} details
 * @returns {
  * answered: number,
  * remaining: number,
  * total: number,
  * passingTime: string,
  * remainingTime: string,
  * examDuration: string,
  * spendedPerQuestion: string } stats
 */
export const generateStats = (details) => {
  const stats = {
    answered: countAnswered(details),
    remaining: countRemaining(details),
    total: details.questions.length,
    passingTime: millisToMinutesAndSeconds(calcPassing(details)),
    remainingTime: millisToMinutesAndSeconds(calcRemaining(details)),
    examDuration: millisToMinutesAndSeconds(details.examDuration),
    spendedPerQuestion: millisToMinutesAndSeconds(calcSpended(details))
  }
  return stats
}

/**
 * @param {object} details
 * @param {number} id
 * @returns {object: detail} detail
 */
export const findCurrent = (details, id) => {
  const pointer = id || details.pointer
  const questions = details.questions
  return questions.find((question) => question.id === pointer)
}

export default generateStats
