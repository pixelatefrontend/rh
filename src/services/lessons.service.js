import Vue from 'vue'

export const info = () => Vue.$axios('GET', `/lessons/info`)

export const lesson = id => Vue.$axios('GET', `/lessons/${id}`)

export const watching = (id, time) => Vue.$axios('POST', `/lessons/update-watch-time?lessonId=${id}&watchTime=${time}`)

export const reset = (id) => Vue.$axios('POST', `/lessons/watched?lessonId=${id}`)

export const lessons = group => Vue.$axios('GET', `/lessons?group=${group}`)

export const list = filter => Vue.$axios('GET',
  `/lessons?group=${filter.groupId}&is_finished=${filter.isFinished}&page=${filter.page}&direction=${filter.orderType}`
)
