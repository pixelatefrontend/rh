import Vue from 'vue'

const getters = {
  list: state => state.list,
  currentTab: state => state.currentTab,
  loading: state => state.loading,
  loaded: state => state.loaded,
  loadingWords: state => state.loadingWords,
  loadingTab: state => state.loadingTab,
  nextPage: state => state.nextPage,
  filters: state => {
    let filters = JSON.parse(JSON.stringify(state.list.filters))
    filters.groups = filters.group.map(
      ({ value, label, media, selected = false }) => ({
        value,
        label,
        selected,
        icon: media,
      })
    )
    filters.language = filters.language.map(
      ({ id, value, key, selected = false }) => ({
        id: id,
        key: key,
        value: value,
        selected,
      })
    )
    filters.order = filters.order.map(
      ({ id, value, key, selected = id === state.list.order_value }) => ({
        selected: selected,
        label: value,
        value: id,
        id: id,
      })
    )
    filters.type = filters.type.map(
      ({ value, key, id, selected = id === state.list.type_value }) => ({
        selected: selected,
        label: value,
        type: key,
        id: id,
      })
    )
    return filters
  },
  filter: state => state.filter,
}

const actions = {
  /**
   *
   */
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
  /**
   *
   */
  getListData: ({ commit, state }, listData) => {
    if (listData.partial) {
      commit('SET_LOADING_TAB', true)
    } else {
      commit('SET_LOADING', true)
    }
    let groupData = state.currentTab !== 0 ? `&groupId=${state.currentTab}` : ''
    return Vue.axios
      .get(`/word-lists/${listData.id}?&page=${listData.pageNo}&${groupData}`)
      .then(response => {
        commit('SET_LIST', response.data)
        commit('SET_CURRENT_TAB', response.data.group_id_value)
        commit('SET_ITEMS', response.data.items)
        commit('SET_PAGENTION', response.data)
        commit('SET_LOADING', false)
        commit('SET_LOADING_TAB', false)
        commit('SET_LOADED', true)
        // console.log('the current Tab', state.currentTab)
        commit('SET_FILTER', {
          group: response.data.group_id_value,
          language: { en: false, tr: true },
          order: response.data.order_value,
          type: response.data.type_value,
          search: '',
        })
      })
      .catch(e => {
        // console.log('catch', e)
        commit('SET_LOADING_TAB', false)
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} currentTab
   */
  setCurrentTab({ commit }, currentTab) {
    commit('SET_CURRENT_TAB', currentTab)
  },
  /**
   *
   */
  setFilter: ({ commit }, filter) => {
    commit('SET_FILTER', filter)
  },
  /**
   *
   */
  resetFilter: ({ commit }) => {
    commit('RESET_FILTER')
  },
  /**
   *
   * @param {*} wordItems
   */
  updateWordCycle({ commit }, wordItem) {
    return Vue.axios
      .post(
        `/word-lists/cycle?languageId=${wordItem.languageId}&groupId=${wordItem.groupId}&itemId=${wordItem.id}`
      )
      .then(response => {
        if (response.data.success) {
          commit('SET_word_CYCLE', wordItem.id)
        }
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} wordItem
   */
  updateWordStatus({ commit }, wordItem) {
    return Vue.axios
      .post(
        `/word-lists/status?languageId=${wordItem.languageId}&groupId=${wordItem.groupId}&itemId=${wordItem.id}&value=${wordItem.value}&q=${wordItem.search}`
      )
      .then(response => {
        if (response.data.success) {
          commit('SET_WORD_STATES', wordItem)
        }
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} key
   */
  setLangFilter({ commit, state }, key) {
    let language = {}
    if (key === 'en') {
      language = {
        en: true,
        tr: false,
      }
    } else if (key === 'tr') {
      language = {
        en: false,
        tr: true,
      }
    }
    commit('SET_FILTER', {
      group: state.filter.group,
      language: language,
      order: state.filter.order,
      type: state.filter.type,
      search: state.filter.search,
    })
  },
  /**
   *
   * @param {*} param0
   * @param {*} groupId
   */
  setGroupFilter({ commit, state }, groupId) {
    commit('SET_FILTER', {
      group: groupId,
      language: state.filter.language,
      order: state.filter.order,
      type: state.filter.type,
      search: state.filter.search,
    })
  },
  /**
   *
   * @param {*} param0
   * @param {*} groupId
   */
  setTypeFilter({ commit, state }, typeID) {
    commit('SET_FILTER', {
      group: state.filter.group,
      language: state.filter.language,
      order: state.filter.order,
      type: typeID !== 'ALL' ? typeID : '',
      search: state.filter.search,
    })
    return true
  },
  /**
   *
   * @param {*} param0
   * @param {*} orderID
   */
  setListOrder({ commit, state }, orderID) {
    commit('SET_FILTER', {
      group: state.filter.group,
      language: state.filter.language,
      order: orderID.value,
      type: state.filter.type,
      search: state.filter.search,
    })
    return true
  },
  /**
   *
   * @param {*} param0
   * @param {*} term
   */
  setListSearchParam({ commit, state }, term) {
    commit('SET_FILTER', {
      group: state.filter.group,
      language: state.filter.language,
      order: state.filter.order,
      type: state.filter.type,
      search: term,
    })
    return true
  },
  /**
   *
   * @param {*} param0
   */
  applyFiterOnList({ commit, state }) {
    commit('SET_LOADING_WORDS', true)
    let filter = {
      groupId: state.filter.group ? state.filter.group : 1,
      orderId: state.filter.order ? state.filter.order : -1,
      typeId: state.filter.type ? state.filter.type : -1,
      languageId: state.filter.language.tr ? 1 : 2,
      search: state.filter.search,
    }
    Vue.axios
      .get(
        `/word-lists/${state.list.id}?page=1&groupId=${filter.groupId}&orderId=${filter.orderId}&typeId=${filter.typeId}&languageId=${filter.languageId}&q=${filter.search}`
      )
      .then(response => {
        commit('SET_ITEMS', response.data.items)
        commit('SET_PAGENTION', response.data)
        commit('SET_LOADING_WORDS', false)
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} listData
   */
  async MoreWords({ commit, state, dispatch }, listData) {
    commit('SET_LOADING_WORDS', true)
    let filter = {
      groupId: state.filter.group ? state.filter.group : 1,
      orderId: state.filter.order ? state.filter.order : -1,
      typeId: state.filter.type ? state.filter.type : -1,
      languageId: state.filter.language.tr ? 1 : 2,
      search: state.filter.search,
    }
    return Vue.axios
      .get(
        `/word-lists/${listData.id}?page=${listData.pageNO}&groupId=${filter.groupId}&orderId=${filter.orderId}&typeId=${filter.typeId}&languageId=${filter.languageId}&q=${filter.search}`
      )
      .then(response => {
        if (
          response.data.items.length === 0 &&
          state.filter.search.length > 0
        ) {
          console.log('has search, no result', listData)
          dispatch('setListSearchParam', '')
          dispatch('getListData', {
            id: listData.id,
            pageNo: 1,
            groupId: 1,
          })
          return commit('SET_LOADING_WORDS', false)
        }
        // let newwords = response.data
        // newwords.items.unshift(...state.list.items)
        console.log('otherwise')
        commit('SET_ITEMS', response.data.items)
        commit('SET_PAGENTION', response.data)
        commit('SET_LOADING_WORDS', false)
      })
  },
  updateshowBoxPhoto({ commit, state }, wordData) {
    let neWwords = state.list
    neWwords.items
      .find(word => word.id === wordData.id)
      .meanings.find(means => means.id === wordData.meaningIndex).showInBox =
      wordData.showInBox
    commit('SET_LIST', neWwords)
    commit('SET_ITEMS', neWwords.items)
    return true
  },
  /**
   *
   * @param {*} param0
   * @param {*} word
   */
  deleteWordFromListDetail({ commit }, word) {
    return Vue.axios
      .post(
        `/delete-from-user-word-list?type=${word.type}&data=${word.data}&word_list_id=${word.listID}`
      )
      .then(response => {
        return response
      })
  },
}

const mutations = {
  SET_LIST(state, list) {
    const $state = state
    $state.list = list
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_CURRENT_TAB(state, currentTab) {
    const $state = state
    $state.currentTab = currentTab
  },
  SET_LOADING_WORDS(state, loadingWords) {
    const $state = state
    $state.loadingWords = loadingWords
  },
  SET_LOADING_TAB(state, loadingTab) {
    const $state = state
    $state.loadingTab = loadingTab
  },
  SET_ITEMS(state, items) {
    // console.log('SET_ITEMS', items)
    const $state = state
    $state.list.items = items
  },
  RESET_STATE(state) {
    const $state = state
    $state.list = null
    $state.loading = false
    $state.loaded = false
    $state.loadingWords = false
    $state.loadingTab = false
    $state.filter = {
      group: null,
      language: { en: false, tr: true },
      order: null,
      type: null,
      search: '',
    }
  },
  SET_FILTERS(state, filters) {
    const $state = state
    $state.filter = filters
  },
  /**
   *
   * @param {*} state
   * @param {*} filter
   */
  SET_FILTER(state, filter) {
    const $state = state
    $state.filter = filter
  },
  /**
   *
   * @param {*} state
   */
  RESET_FILTER(state) {
    const $state = state
    $state.filter = {
      group: null,
      language: { en: false, tr: true },
      order: null,
      type: null,
      search: '',
    }
  },
  /**
   *
   * @param {*} state
   * @param {*} wordItem
   */
  SET_word_CYCLE(state, wordItem) {
    const $state = state
    $state.list.items.find(word => word.id === wordItem).cycle++
  },
  /**
   *
   * @param {*} state
   * @param {*} wordItem
   */
  SET_WORD_STATES(state, wordItem) {
    const $state = state
    $state.list.items.find(word => word.id === wordItem.id).status =
      wordItem.status
  },
  SET_PAGENTION(state, data) {
    const $state = state
    $state.list.totalPage = data.totalPage
    $state.list.currentPage = data.currentPage
    $state.nextPage = data.nextPage
  },
}

const state = {
  list: null,
  currentTab: 0,
  loading: false,
  loaded: false,
  loadingWords: false,
  loadingTab: false,
  nextPage: false,
  filter: {
    group: null,
    language: { en: false, tr: true },
    order: null,
    type: null,
    search: '',
  },
  filters: {},
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
