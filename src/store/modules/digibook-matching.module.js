import {
  clearMatchedList,
  saveMatchingPair,
} from '../../services/digibook.service'
import { notifier } from '../../common/helpers'
import shared from './digibook-shared.module'
import Vue from 'vue'

const state = {
  pairedItems: {
    items: [],
    answers: [],
  },
  unpairedItems: {
    items: [],
    answers: [],
  },
  matches: [],
  notMatches: {
    items: [],
    answers: [],
  },
}

const getters = {
  matches: state => state.matches,
  notMatches: state => state.notMatches,
  hasMatchedItems: state => state.matches.length > 0,
  hasFullyMatched: state => state.notMatches.items.length === 0,
}

const mutations = {
  setPairedList: state => {
    const { items, answers } = state.shared.content.data

    let matchedItems = items.filter(el => el.isMatched === true)
    matchedItems.forEach(match => {
      let answer = answers.find(answer => answer.itemID === match.itemID)
      state.matches.push({ item: match, answer: answer })
    })

    let notMatchedItems = items.filter(el => el.isMatched === false)
    Vue.set(state.notMatches, 'items', [...notMatchedItems])

    let matchedAnswers = []
    matchedItems.forEach(el => {
      let answerItem = answers.find(a => a.itemID === el.id)
      matchedAnswers.push(answerItem)
    })

    let notMatchedAnswers = answers.filter(
      el => matchedAnswers.indexOf(el) === -1
    )
    Vue.set(state.notMatches, 'answers', [...notMatchedAnswers])
  },

  setNewMatch: (state, data) => {
    state.matches.push({ item: data.item, answer: data.answer })
    let itemIndex = state.notMatches.items.indexOf(data.item)
    let answerIndex = state.notMatches.answers.indexOf(data.answer)

    state.notMatches.items.splice(itemIndex, 1)
    state.notMatches.answers.splice(answerIndex, 1)
  },

  clearMatchedList: (state, data) => {
    state.matches = []

    state.notMatches.items = data.items
    state.notMatches.answers = data.answers
  },

  cleanMatches: state => {
    state.matches = []
  },
}

const actions = {
  handleMatchedList: ({ commit }) => {
    commit('setPairedList')
  },

  handleNewMatch: async ({ state, commit }, data) => {
    let matched = state.matches.length
    let items = state.shared.content.data.items.length
    let completion = (100 / items) * (matched + 1)
    completion = Math.floor(completion)
    try {
      const pairing = {
        contentId: data.contentId,
        value: data.item.title,
      }
      const response = await saveMatchingPair(pairing)
      commit('digibook/index/setCompletion', completion, { root: true })
      notifier.success(response.data.message)
      commit('setNewMatch', data)
    } catch (error) {
      console.error(error)
      notifier.error('Eşleştirme başarısız!')
    }
  },

  handleClearMatchedList: async ({ commit }, id) => {
    commit('setLoading', true)
    try {
      const response = await clearMatchedList(id)
      commit('clearMatchedList', response.data.data)
      commit('digibook/index/setCompletion', 0, { root: true })
      notifier.success('Eşleştirme listesi temizlendi')
    } catch (error) {
      console.error('Pair clear error: \n', error)
      notifier.error('Eşleştirme listesi temizleme başarısız!')
    } finally {
      commit('setLoading', false)
    }
  },

  handleCleanMatches: ({ commit }) => {
    commit('cleanMatches')
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules: {
    shared,
  },
}
