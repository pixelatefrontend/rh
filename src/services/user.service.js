import Vue from 'vue'

export const profile = () => Vue.$axios('GET', '/user')

export const favoriteApp = app => Vue.$axios('POST', `/user/favourite-apps?app=${app}`)

export const unfavoriteApp = app => Vue.$axios('DELETE', `/user/favourite-apps?app=${app}`)

export const toggleTour = (app, value) => Vue.$axios('POST', `/user/tour?${app}=${value}`)

export const toggleSounds = value => Vue.$axios('POST', `/user/update-sound?value=${value}`)

export const toggleFirstTime = value =>
  Vue.$axios('POST', `/user/introduction?has_introduction=${value}`)

export const toggleWordbookIntro = value =>
  Vue.$axios('POST', `/user/introduction?word_book_intro=${value}`)
