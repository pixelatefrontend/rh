import Vue from 'vue'

export const lists = () => Vue.$axios('GET', '/word-lists')

export const usersList = () => Vue.$axios('GET', '/word-lists')

export const exploreList = () => Vue.$axios('GET', '/word-lists/explore')

// export const followingList = () => Vue.$axios('GET', '/word-lists/explore')

export const getList = id => Vue.$axios('GET', `/word-lists/${id}`)

export const explore = () => Vue.$axios('GET', '/word-lists/explore')

export const createList = (title, desc, status) =>
  Vue.$axios('POST', `/word-lists?title=${title}&description=${desc}&is_public=${status}`)

export const addList = data =>
  Vue.$axios(
    'POST',
    `/word-lists?title=${data.title}&description=${data.desc}&is_public=${data.status}`
  )

export const editList = data =>
  Vue.$axios(
    'PUT',
    `/word-lists/${data.id}?title=${data.title}&description=${data.desc}&is_public=${data.status}`
  )

// export const editList = id => Vue.$axios('PUT', `/word-lists/${id}`)

export const remove = id => Vue.$axios('DELETE', `/word-lists/${id}`)

export const removeList = id => Vue.$axios('DELETE', `/word-lists/${id}`)

export const addToList = (type, data, status, id) =>
  Vue.$axios(
    'POST',
    `/add-to-user-word-list?type=${type}&data=${data}&is_public=${status}&word_list_id=${id}`
  )

export const deleteFromList = (type, data, status, id) =>
  Vue.$axios(
    'DELETE',
    `/delete-from-user-word-list?type=${type}&data=${data}&is_public=${status}&word_list_id=${id}`
  )

export const searchList = term => Vue.$axios('GET', `/word-lists/search?q=${term}`)
