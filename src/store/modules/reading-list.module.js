import * as http from '../../services/readings.service'

const getters = {
  isLoading: state => state.isLoading,
  isLoaded: state => state.isLoaded,
  readings: state => state.readingsList,
  loadingMore: state => state.loadingMore,
  hasMore: state => state.more
}

const mutations = {
  resetData(state) {
    state.readingsList = []
  },
  setReadings(state, data) {
    state.readingsList = data
  },
  setLoading(state, value) {
    state.isLoading = value
  },
  setLoaded(state, value) {
    state.isLoaded = value
  },
  setLoadMore(state, value) {
    state.more = value
  },
  setLoadingMore(state, value) {
    state.loadingMore = value
  },
  loadNextItems(state, items) {
    state.readingsList.push(...items)
  }
}

const actions = {
  getReadingsByGroup: ({ commit }, groupId) => {
    commit('resetData')
    commit('setLoading', true)
    http.group(groupId).then((res) => {
      commit('setReadings', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  getReadingsList: ({ commit }, filters) => {
    commit('resetData')
    commit('setLoading', true)
    http.list(filters).then((res) => {
      commit('setReadings', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  loadNext: ({ commit }, filters) => {
    commit('setLoadingMore', true)

    http.list(filters).then((res) => {
      commit('setLoadingMore', false)
      commit('loadNextItems', res.data.data)
      commit('setLoadMore', res.data.next_page)
    })
  }
}

const state = {
  readingsList: [],
  isLoading: true,
  isLoaded: false,
  loadingMore: false,
  more: false
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
}
