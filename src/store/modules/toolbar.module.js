const state = {
  tools: [
    {
      id: 0,
      main: false,
      name: 'Kelime Defterim',
      slug: 'wordlists',
      icon: 'icon-lists',
      hasNotification: false,
      componentName: 'toolbar-word-list'
    },
    {
      id: 1,
      main: false,
      name: 'Notlarım',
      slug: 'notes',
      icon: 'icon-notes',
      hasNotification: false,
      mobileHidden: true,
      componentName: 'toolbar-notes'
    },
    {
      id: 2,
      main: false,
      name: 'Sorularım',
      slug: 'questions',
      icon: 'icon-question-answer',
      hasNotification: true,
      mobileHidden: true,
      componentName: 'toolbar-questions'
    },
    {
      id: 3,
      main: false,
      name: 'Ödevlerim',
      slug: 'homeworks',
      icon: 'icon-pens',
      hasNotification: true,
      mobileHidden: true,
      componentName: 'toolbar-homework'
    },
    {
      id: 4,
      main: false,
      slug: 'dash',
      mobileHidden: true,
      icon: 'icon-dots'
    },
    {
      id: 5,
      main: true,
      name: 'Sözlük',
      slug: 'dictionary',
      icon: require('@/assets/img/app-sozluk.svg'),
      hasNotification: false,
      componentName: 'toolbar-dictionary'
    },
    {
      id: 6,
      main: true,
      name: 'İpucu Havuzu',
      slug: 'cluebank',
      icon: require('@/assets/img/app-cluebank.svg'),
      hasNotification: true,
      componentName: 'toolbar-cluebank'
    }
  ],
  activeTool: {},
  recentTool: {}
}

const getters = {
  tools: state => state.tools,
  activeTool: state => state.activeTool,
  recentTool: state => state.recentTool
}

const mutations = {
  setActiveTool: (state, data) => {
    console.log('toolbar vuex: ', data)
    let tool = state.tools.find(el => el.id === data)
    state.activeTool = tool
  },
  setRecentTool: (state, data) => {
    let tool = state.tools.find(el => el.id === data)
    state.recentTool = tool
  },
  clearActiveTool: state => (state.activeTool = {})
}

const actions = {
  setActiveTool({
    commit
  }, toolId) {
    commit('setActiveTool', toolId)
  },

  setRecentTool({
    commit
  }, toolId) {
    commit('setRecentTool', toolId)
  },

  clearActiveTool({
    commit
  }) {
    commit('clearActiveTool')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
