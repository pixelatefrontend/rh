import Vue from 'vue'
import find from 'lodash/find'
import store from '../store/index'
import { SHOW_OVERLAY, SHOW_TOOLBAR } from '../store/actions.type'
import { SET_HIGHLIGHT_TEXT } from '../store/mutations.type'

const contentTypes = [
  {
    en: 'test',
    tr: 'test',
    icon: 'test',
  },
  {
    en: 'translation_list',
    tr: 'ceviri',
    icon: 'translation',
  },
  {
    en: 'table',
    tr: 'tablo',
    icon: 'table',
  },
  {
    en: 'match',
    tr: 'eslestirme',
    icon: 'pairing',
  },
  {
    en: 'reading_list',
    tr: 'okuma',
    icon: 'reading',
  },
  {
    en: 'word_list',
    tr: 'kelime-listesi',
    icon: 'wordlist',
  },
]

export const digiLinkBuilder = fields => {
  const contentType = contentTypes.find(el => el.en === fields.content_type).tr
  return `/digibook/${fields.digibook_id}/unite/${fields.unit_id}/alistirma/${fields.exercise_id}/${contentType}/${fields.content_id}`
}

export const digibookContentRouter = (fields, hash = '') => {
  const contentType = contentTypes.find(el => el.en === fields.contentType).tr
  return `/digibook/${fields.digibookId}/unite/${fields.unitId}/alistirma/${fields.exerciseId}/${contentType}/${fields.contentId}${hash}`
}

export const digiExerciseType = item => {
  const obj = contentTypes.find(el => el.en === item).icon
  return `digi${obj}`
}

export const digiTypeUrl = item => {
  return contentTypes.find(el => el.en === item).tr
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms))
}

export const speak = word => {
  const synth = window.speechSynthesis
  let utterance = new SpeechSynthesisUtterance(word)
  let voices = []

  while (voices.length === 0) {
    voices = synth.getVoices()
    if (voices.length === 0) {
      sleep(500)
    }
  }

  // apple
  let voice = find(
    voices,
    v => v.name === 'Samantha (Enhanced)' || v.name === 'Samantha'
  )
  if (!voice) {
    // chrome
    voice = find(voices, ['name', 'Google UK English Female'])
  }
  if (!voice) {
    // edge
    voice = find(
      voices,
      v =>
        v.name === 'Microsoft Zira - English (United States)' ||
        v.name === 'Microsoft Zira Desktop - English (United States)'
    )
  }
  if (!voice) {
    // android
    voice = find(voices, ['name', 'English United Kingdom'])
  }
  if (!voice) {
    // ios
    voice = find(
      voices,
      v =>
        (v.name === 'Alex' && v.lang === 'en-US') ||
        (v.name === 'Daniel' && v.lang === 'en-GB')
    )
  }
  if (!voice) {
    // others
    voice = find(voices, ['lang', 'en-US'])
  }

  utterance.voice = voice
  utterance.rate = 0.85
  speechSynthesis.speak(utterance)
}

const googleSpeak = async word => {
  var response = {}
  var xmlHttp = new XMLHttpRequest()
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === XMLHttpRequest.DONE) {
      response = JSON.parse(xmlHttp.responseText)
    }
  }
  xmlHttp.open(
    'POST',
    'https://texttospeech.googleapis.com/v1/text:synthesize?key=AIzaSyCozu0d0eh2zn62EZIuRh7MruVnCPIY3Cw',
    false
  ) // false for synchronous request
  xmlHttp.send(
    JSON.stringify({
      audioConfig: {
        audioEncoding: 'MP3',
      },
      input: {
        text: word,
      },
      voice: {
        languageCode: 'en-US',
        name: 'en-US-Wavenet-D',
      },
    })
  )
  var audio = new Audio('data:audio/mpeg;base64,' + response.audioContent)
  audio.play()
}

export const gspeak = async word => {
  var response = {}
  var xmlHttp = new XMLHttpRequest()
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === XMLHttpRequest.DONE) {
      response = JSON.parse(xmlHttp.responseText)
    }
  }
  xmlHttp.open(
    'POST',
    'https://texttospeech.googleapis.com/v1/text:synthesize?key=AIzaSyCozu0d0eh2zn62EZIuRh7MruVnCPIY3Cw',
    false
  ) // false for synchronous request
  xmlHttp.send(
    JSON.stringify({
      audioConfig: {
        audioEncoding: 'MP3',
      },
      input: {
        text: word,
      },
      voice: {
        languageCode: 'en-US',
        name: 'en-US-Wavenet-D',
      },
    })
  )
  var audio = new Audio('data:audio/mpeg;base64,' + response.audioContent)
  audio.play()
}

export const gspeakNew = (word, languageCode = 'en-US') => {
  var response = {}
  var xmlHttp = new XMLHttpRequest()
  xmlHttp.onreadystatechange = function() {
    if (xmlHttp.readyState === XMLHttpRequest.DONE) {
      response = JSON.parse(xmlHttp.responseText)
    }
  }
  xmlHttp.open(
    'POST',
    'https://texttospeech.googleapis.com/v1/text:synthesize?key=AIzaSyCozu0d0eh2zn62EZIuRh7MruVnCPIY3Cw',
    false
  ) // false for synchronous request
  xmlHttp.send(
    JSON.stringify({
      audioConfig: {
        audioEncoding: 'MP3',
      },
      input: {
        text: word,
      },
      voice: {
        languageCode: languageCode,
        // name: 'en-US-Wavenet-D'
      },
    })
  )
  return response.audioContent
}

const getWordType = key => {
  const keyword = key ? key.toUpperCase() : ''
  if (keyword === 'V') {
    return 'verb'
  } else if (keyword === 'N') {
    return 'noun'
  } else if (keyword === 'D' || keyword === 'DET') {
    return 'det'
  } else if (keyword === 'A' || keyword === 'ADJ') {
    return 'adj'
  }
  return 'noun'
}

export const manipulationListWithFilter = (exams, filters) => {
  let $exams = JSON.parse(JSON.stringify(exams))
  const $filters = filters
  for (const key in $filters) {
    if (key === 'typeId') {
      if ($filters[key] !== -1) {
        $exams = $exams.filter(item => item[key].includes($filters[key]))
      }
    } else if (key === 'questionTypeId') {
      if ($filters[key] !== -1) {
        $exams = $exams.filter(item =>
          item['questionTypeIds'].includes($filters[key])
        )
      }
    } else {
      if ($filters[key] !== -1) {
        $exams = $exams.filter(item => item[key] === $filters[key])
      }
    }
  }
  return $exams
}

/**
 * millis: milisecond typed number
 * mm:ss return
 * @param {number} millis
 * @returns {string} minuteSecond
 */
export const millisToMinutesAndSeconds = millis => {
  let minutes = 0
  let seconds = 0
  if (millis >= 0) {
    minutes = Math.floor(millis / 60000)
    seconds = ((millis % 60000) / 1000).toFixed(0)
  }
  return minutes + ':' + (seconds < 10 ? '0' : '') + seconds
}

export const limit = (exams, limit) =>
  limit !== -1 ? exams.splice(0, limit) : exams

export const emailValidator = email => {
  const regex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
  return regex.test(email)
}

export const getCookie = name => {
  const result = document.cookie.match('(^|;)\\s*' + name + '\\s*=\\s*([^;]+)')
  return result ? result.pop() : ''
}

export const replaceBetween = (origin, startIndex, endIndex, insertion) => {
  return (
    origin.substring(0, startIndex) + insertion + origin.substring(endIndex)
  )
}

const parseJwt = token => {
  try {
    return JSON.parse(atob(token.split('.')[1]))
  } catch (e) {
    return null
  }
}

export const jwtAlive = token => {
  if (getCookie(token) !== '' && parseJwt(getCookie(token)) !== null) {
    return parseJwt(getCookie(token)).exp * 1000 - new Date().getTime() > 0
  } else {
    return false
  }
}

export const notifier = {
  success: message => {
    Vue.swal({
      toast: true,
      timer: 3000,
      icon: 'success',
      position: 'top-end',
      showConfirmButton: false,
      title: message,
    })
  },

  error: message => {
    Vue.swal({
      toast: true,
      timer: 3000,
      icon: 'error',
      position: 'top-end',
      showConfirmButton: false,
      title: message,
    })
  },
}

export const errorBubble = message => {
  Vue.swal({
    toast: true,
    timer: 3000,
    icon: 'error',
    position: 'top-end',
    showConfirmButton: false,
    title: message,
  })
}

export const setPageTitle = (title, app, subpage) => {
  let text = ''
  subpage
    ? (text = title === undefined ? 'Yükleniyor... |' : title + ' |')
    : (text = '')
  document.title = `${text} ${app} | RH+ Platform`
}

export const setTemporaryToken = token => {
  Vue.axios.defaults.headers.common['Authorization'] = 'Bearer ' + token
  window.localStorage.setItem('temporaryToken', token)
}

export const deleteCookie = name => {
  document.cookie = `${name}= ;expires=Thu, 01 Jan 1970 00:00:00 GMT;path=/;domain=.remzihoca.com`
}

export const delayedSearch = (callback, delay = 1000) => {
  let timeout = null
  clearTimeout(timeout)
  timeout = setTimeout(() => {
    callback()
  }, delay)
}

export const downloadHelper = (url, name = 'Dosya') => {
  fetch(url)
    .then(res => res.blob())
    .then(blob => {
      notifier.success('Doküman indiriliyor...')
      const newUrl = window.URL.createObjectURL(blob)
      const a = document.createElement('a')
      a.href = newUrl
      a.download = name
      document.body.appendChild(a)
      a.click()
      a.remove()
    })
    .catch(err => {
      notifier.error(err.response.data.message)
    })
}

export const cloneObject = obj => JSON.parse(JSON.stringify(obj))

export const playResultSound = status => {
  let audio = new Audio('/silencer.mp3')
  audio.src = status ? '/correct.mp3' : '/wrong.mp3'
  audio.load()
  store.getters['soundsOn'] && audio.play()
}

export const fastTranslation = () => {
  let isTranslatable = store.getters['isTranslatable']
  let selection = window
    .getSelection()
    .getRangeAt(0)
    .cloneContents().textContent
  selection = selection.trim().toLowerCase()
  if (isTranslatable && selection.length > 0) {
    store.dispatch(SHOW_OVERLAY, true)
    store.dispatch(SHOW_TOOLBAR, true)
    store.dispatch('toolbar/setActiveTool', 5)
    store.commit(SET_HIGHLIGHT_TEXT, {
      text: selection,
      status: isTranslatable,
    })
  }
}

export const handleApplistScoll = (e, t) => {
  t.topShadow.style.opacity = t.el.scrollTop === 0 ? 0 : 1
  t.botShadow.style.opacity =
    t.el.scrollHeight - t.parent.scrollHeight === t.el.scrollTop ? 0 : 1
}

export default {
  googleSpeak,
  getWordType,
  emailValidator,
  handleApplistScoll,
}
