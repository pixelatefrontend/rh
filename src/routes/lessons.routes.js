export default [
  {
    path: '/ders',
    component: () => import(/* webpackChunkName: "lessons" */ '../views/Lessons'),
    children: [
      {
        path: '',
        name: 'LessonsList',
        component: () => import(/* webpackChunkName: "lessons" */ '../views/Lessons/LessonsList'),
        meta: {
          title: 'Ders',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: true,
          tourName: 'lessonsTour',
          noteName: 'lesson'
        }
      },
      {
        path: ':id',
        name: 'LessonsDetail',
        component: () => import(/* webpackChunkName: "lessons" */ '../views/Lessons/LessonsDetail'),
        meta: {
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          noteName: 'lesson',
          pageClass: 'header--gray'
        }
      },
      {
        path: ':id/video',
        name: 'LessonsWatch',
        component: () => import(/* webpackChunkName: "lessons" */ '../views/Lessons/LessonsWatch'),
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          noteName: 'lesson'
        }
      }
    ]
  }
]
