import Vue from 'vue'

const getters = {
  loading: state => state.loading,
  loaded: state => state.loaded,
  selectedWords: state => (state.selectedWords ? state.selectedWords : []),
  listMassegeStatus: state => state.statusType,
  listMessageText: state => state.listMessageText,
  addWordModal: state => state.addWordModal,
  wordType: state => state.wordType,
}

const actions = {
  /**
   *
   * @param {*} param0
   * @param {*} listDetails
   */
  async newQuickList({ commit }, listDetails) {
    commit('SET_STATUS_TYPE', false)
    return Vue.axios
      .post(
        `/word-lists?title=${listDetails.title}&description=${listDetails.description}&is_public=${listDetails.isPublic}`
      )
      .then(response => {
        commit('SET_STATUS_TYPE', response.data.success)
        commit('SET_MESSAGE_TEXT', response.data.message)
      })
  },
  /**
   *
   * @param {*} param0
   */
  clearFromMessage({ commit }) {
    commit('SET_STATUS_TYPE', false)
    commit('SET_MESSAGE_TEXT', '')
  },
  /**
   *
   * @param {*} param0
   * @param {*} newWord
   */
  addNewWordsToList({ commit, state, dispatch }, listId) {
    const words = state.selectedWords.map(el => el.data)
    // let wordIds = []
    // let wordstype = []
    // let wordsPublic = []
    // state.selectedWords.forEach(element => {
    //   wordIds.push('&data[]=' + element.data)
    // })
    const wordType = state.wordType
    return Vue.axios
      .post(`/add-to-user-word-list?is_public=false`, {
        type: wordType,
        data: words,
        word_list_id: listId,
      })
      .then(response => {
        console.log('Add new word responed', response.data)
        commit('SET_STATUS_TYPE', response.data.success)
        commit('SET_MESSAGE_TEXT', response.data.message)
        commit('SET_WORD_LIST', [])
        commit('SET_ADD_WORD_MODAL', false)
      })
  },

  /**
   *
   * @param {*} param0
   * @param {*} wordTxt
   */
  addWordToList({ commit, state }, wordTxt) {
    commit('Add_WORD_TO_LIST', wordTxt)
  },
  /**
   *
   * @param {*} param0
   * @param {*} wordTxt
   */
  deleteWordToList({ commit, state }, wordId) {
    let $selectedWords = state.selectedWords
    $selectedWords = $selectedWords.filter(item => item.data !== wordId)
    // console.log('check the oword id - ', wordId, $selectedWords)
    commit('SET_WORD_LIST', $selectedWords)
  },
  resetWordlist({ commit }) {
    const $selectedWords = []
    commit('SET_WORD_LIST', $selectedWords)
  },

  /**
   *
   * @param {*} param0
   * @param {*} addWordModal
   */
  setAddWordModal({ commit }, modelObject) {
    commit('SET_ADD_WORD_MODAL', modelObject.show)
    commit('SET_WORD_TYPE', modelObject.wordType)
  },
}
const mutations = {
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_STATUS_TYPE(state, listMassegeStatus) {
    const $state = state
    $state.listMassegeStatus = listMassegeStatus
  },
  SET_MESSAGE_TEXT(state, listMessageText) {
    const $state = state
    $state.listMessageText = listMessageText
  },
  SET_ADD_WORD_MODAL(state, addWordModal) {
    const $state = state
    $state.addWordModal = addWordModal
  },
  SET_WORD_LIST(state, selectedWords) {
    const $state = state
    $state.selectedWords = selectedWords
  },
  SET_WORD_TYPE(state, wordType) {
    const $state = state
    $state.wordType = wordType
  },
  Add_WORD_TO_LIST(state, wordType) {
    const $state = state
    const $selectedWords = state.selectedWords
    $selectedWords.push(wordType)
    $state.selectedWords = $selectedWords
  },
}

const state = {
  loading: false,
  loaded: false,
  selectedWords: [],
  listMassegeStatus: false,
  listMessageText: '',
  addWordModal: false,
  wordType: 'w',
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
