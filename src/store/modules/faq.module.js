import * as http from '../../services/faq.service'

const state = {
  faqList: [],
  popularFaqs: [],
  categoriesList: [],
  isLoading: true,
  isLoaded: false,
  activeCategory: {},
  search: '',
  filteredFaqsList: []
}

const getters = {
  faqs: state => {
    if (state.search.length === 0) return state.faqList
    return state.faqList.filter(el => {
      let question = el.question.toLowerCase()
      return question.indexOf(state.search) > -1
    })
  },
  popularFaqs: state => state.popularFaqs,
  categoriesLoaded: state => state.categoriesList.length > 0,
  filteredFaqs: state => state.filteredFaqsList,
  categories: state => state.categoriesList,
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  dataLoaded: state => !state.isLoading && state.isLoaded,
  activeCategory: state => state.activeCategory,
  searchText: state => state.search
}

const mutations = {
  setLoading: (state, data) => (state.isLoading = data),
  setLoaded: (state, data) => (state.isLoaded = data),
  setFaqsList: (state, data) => {
    state.faqList = data
    state.filteredFaqsList = data
  },
  setPopularFaqs: (state, data) => (state.popularFaqs = data),
  setCategoriesList: (state, data) => state.categoriesList.push(...data),
  setActiveCategory: (state, data) => {
    console.log('setActive: ', data)
    if (data.main === -1) state.filteredFaqsList = state.popularFaqs
    else {
      let parentCategory = state.categoriesList.find(el => el.id === data.main)
      state.activeCategory = parentCategory.children.find(el => el.id === data.sub)
      state.filteredFaqsList = state.activeCategory.questions
    }
  },
  setSearchText: (state, data) => {
    let searchTerm = data.trim().toLowerCase()

    state.filteredFaqsList = state.faqList.filter(el => {
      let question = el.question.toLowerCase()
      return question.indexOf(searchTerm) > -1
    })
  }
}

const actions = {
  getFaqs: ({ commit }) => {
    commit('setLoading', true)
    http.getFaqs().then(res => {
      console.log('resfaq: ', res)
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setFaqsList', res.data.popular_questions)
      commit('setPopularFaqs', res.data.popular_questions)
      commit('setCategoriesList', res.data.categories)
      commit('setActiveCategory', { sub: -1, main: -1 })
    })
  },

  activateCategory: ({ commit }, data) => {
    commit('setLoading', true)
    setTimeout(() => {
      commit('setLoading', false)
      commit('setActiveCategory', data)
    }, Math.random() * 1000)
  },

  filterFaqs: ({ commit }, data) => {
    // commit('setActiveCategory', -1)
    commit('setSearchText', data)
  },

  searchFaqs: ({ commit }, data) => {
    // commit('setActiveCategory', -1)
    commit('setLoading', true)
    http.searchFaq(data).then(res => {
      commit('setLoading', false)
      commit('setFaqsList', res.data)
    })
  },

  async askQuestion({ commit }, data) {
    return http.askFaq(data)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
