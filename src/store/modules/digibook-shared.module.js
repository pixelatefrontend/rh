import Vue from 'vue'
import { getDigibookContentNew } from '../../services/digibook.service'

const state = {
  content: null,
  contentId: 0,
  loading: true,
  loaded: false,
  error: null,
}

const getters = {
  content: state => state.content,
  contentId: state => state.contentId,
  loading: state => state.loading,
  loaded: state => state.loaded,
}

const mutations = {
  setContent: (state, data) => (state.content = data),
  setContentId: (state, data) => (state.contentId = data),
  setLoading: (state, data) => (state.loading = data),
  setLoaded: (state, data) => (state.loaded = data),
  setError: (state, data) => (state.error = data),
  setToolDataKeys: state => {
    console.log(state)
    state.content.data.map(el => {
      Vue.set(el, 'toolLoading', false)
      Vue.set(el, 'toolLoaded', false)
      Vue.set(el, 'toolData', null)
      Vue.set(el, 'activeTool', '')
    })
  },
  clearContent: state => {
    state.content = null
    state.loaded = false
    state.loading = true
    state.error = null
  },
}

const actions = {
  fetchContent: async ({ commit }, data) => {
    console.log('data: ', '\n', data)

    commit('setLoading', true)
    commit('setLoaded', false)
    commit('digibook/index/setCompletionLoading', true, { root: true })
    try {
      const response = await getDigibookContentNew(data.id)
      console.log(response)
      commit('setContent', response.data)
      commit('setContentId', data.id)
      commit('digibook/setContentTitle', response.data.title, { root: true })
      commit('digibook/index/setCompletion', response.data.progress, {
        root: true,
      })
      commit('digibook/index/setExercises', response.data.exercises, {
        root: true,
      })
      commit('digibook/index/setSelection', response.data.selection, {
        root: true,
      })
      if (data.withTools) {
        commit('digibook/tools/setTools', response.data.tools, {
          root: true,
        })
        commit('setToolDataKeys')
      }
    } catch (error) {
      console.error(error)
      commit('setError', error)
    } finally {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('digibook/index/setCompletionLoading', false, { root: true })
    }
  },

  handleClearContent: ({ commit }) => {
    commit('clearContent')
  },
}

export default {
  state,
  getters,
  mutations,
  actions,
}
