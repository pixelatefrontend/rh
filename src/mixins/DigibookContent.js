import Vue from 'vue'
import _ from 'lodash'

export default {
  data() {
    return {
      tools: [],
      questions: [],
      contentLoading: true,
      contentLoaded: false
    }
  },
  watch: {
    $route: 'getData'
  },

  computed: {
    contentHasLoaded() {
      return !this.contentLoading && this.contentLoaded
    }
  },

  methods: {
    closeTools(component) {
      _.forEach(this.$refs.testItem, item => {
        item.closeTools(component)
      })
    },
    async getData() {
      const contentId = this.$route.params.contentId
      // store.dispatch('SHOW_PRELOADER', true)
      this.contentLoading = true
      const response = await Vue.$axios('GET', `/digibook/content/${contentId}`)
      if (_.get(response, 'status') === 200) {
        this.contentLoading = false
        this.contentLoaded = true
        this.tools = this.mapTools(_.get(response, 'data.tools'))

        this.questionUserTranslatable = _.get(response, 'data.has_question_translation_space')
        if (this.contentType === 'paragraph') {
          console.log('digicontent', response.data)
          if (!this.texts) this.texts = _.get(response, 'data.data.texts')
          if (!this.tests) this.tests = _.get(response, 'data.data.tests')
          if (!this.paragraphId) this.paragraphId = _.get(response, 'data.data.paragraph_id')
        } else if (this.contentType === 'pairing') {
          this.items = _.get(response, 'data.data.items')
          this.answers = _.get(response, 'data.data.answers')
        } else {
          this.questions = _.get(response, 'data.data')
        }
        this.$emit('set-content-progress', _.get(response, 'data.progress'))
        // store.dispatch('SHOW_PRELOADER', false)
      }
    },
    async saveMatch(itemSentence, matchID) {
      const contentId = this.$route.params.contentId
      // store.dispatch('SHOW_PRELOADER', true)
      const response = await Vue.$axios(
        'POST',
        `/digibook/answer-match/${contentId}?key=${itemSentence}&value=${matchID}`
      )
      if (_.get(response, 'status') === 200) {
        console.log(_.get(response, 'status'))
      }
    },
    async clearMatch() {
      const contentId = this.$route.params.contentId
      // store.dispatch('SHOW_PRELOADER', true)
      const response = await Vue.$axios('POST', `/digibook/clear-match/${contentId}`)
      if (_.get(response, 'status') === 200) {
        console.log(_.get(response, 'status'))
      }
    },
    mapTools(tools) {
      const result = []
      _.forEach(Object.keys(tools), key => {
        let title = ''
        let icon = ''
        let status = tools[key].status
        let condition = tools[key].condition
        if (key === 'word_list') {
          title = 'Kelimeler'
          icon = 'icon-lists'
        } else if (key === 'root_and_translate') {
          title = 'Çevir'
          icon = 'icon-translate'
        } else if (key === 'answer') {
          title = 'Cevabı Gör'
          icon = 'icon-answer'
        } else if (key === 'description_and_tags') {
          title = 'Açıklaması'
          icon = 'icon-info'
        } else if (key === 'is_question_permission') {
          title = 'Soru Sor'
          icon = 'icon-question'
        }

        result.push({
          type: key,
          title,
          icon,
          status,
          condition,
          ...tools[key]
        })
      })

      // adding note for each question item feature is disabled by rh
      // notes can be taken via just toolbar right now
      // result.push({
      //   type: 'notes',
      //   title: 'Not Al',
      //   icon: 'icon-notes',
      //   status: true,
      //   condition: 'now',
      // })

      return result
    }
  }
}
