export default [
  {
    path: '/ipucu-havuzu',
    component: () => import('../views/Cluebank'),
    children: [
      {
        path: '',
        name: 'CluebankList',
        component: () => import('../views/Cluebank/CluebankList'),
        meta: {
          title: 'İpucu Havuzu',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'clue_bank'
        },
      },
      {
        path: '/ipucu-havuzu/arama/:term',
        name: 'CluebankSearchPage',
        component: () => import('../views/Cluebank/CluebankSearchPage'),
        meta: {
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'clue_bank'
        }
      },
      {
        path: '/ipucu-havuzu/:id',
        name: 'CluebankDetail',
        component: () => import('../views/Cluebank/CluebankDetail'),
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'clue_bank'
        }
      }
    ]
  },
]
