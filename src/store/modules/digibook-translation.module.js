import {
  getDigibookItemToolData,
  saveTranslateAnswer,
} from '../../services/digibook.service'
import { notifier } from '../../common/helpers'
import Vue from 'vue'
import {
  clearContent,
  getContent,
  setActiveTool,
} from '../helpers/digibook-actions.helper'

const state = {
  items: [],
  contentId: null,
  loading: false,
  loaded: false,
  error: null,
}

const getters = {
  questions: state => state.items,
  contentId: state => state.contentId,
  loading: state => state.loading,
  loaded: state => state.loaded,
}

const mutations = {
  setAnswer: (state, data) => {
    let item = state.items.find(el => el.id === data.answer.key)
    Vue.set(item, 'user_answer', data.answer.value)
  },

  setToolDataLoading: (state, data) => {
    let item = state.items.find(el => el.id === data.itemId)
    Vue.set(item, 'toolLoading', data.status)
  },

  setToolDataLoaded: (state, data) => {
    let item = state.items.find(el => el.id === data.itemId)
    Vue.set(item, 'toolLoaded', data.status)
  },

  setToolData: (state, data) => {
    let item = state.items.find(el => el.id === data.itemId)
    Vue.set(item, 'toolData', data.result)
  },

  setActiveTool: (state, data) => {
    let item = state.items.find(el => el.id === data.itemId)

    console.log('item: ', item)

    item.activeTool = data.tool

    let tools = item.activeTools
    let isActive = tools.includes(data.tool)

    if (data.tool === 'question' || data.tool === '') return

    if (!isActive) tools.push(data.tool)
    else tools = tools.filter(el => el !== data.tool)

    Vue.set(item, 'activeTools', tools)
  },

  setData: (state, payload) => {
    console.log('translation payload: ', payload)

    payload.data.forEach(el => {
      Vue.set(el, 'toolData', null)
      Vue.set(el, 'toolLoading', null)
      Vue.set(el, 'toolLoaded', null)
      Vue.set(el, 'activeTool', '')
      Vue.set(el, 'activeTools', [])
      state.items.push(el)
    })
  },

  setContentId: (state, data) => (state.contentId = data),

  setLoading: (state, data) => (state.loading = data),

  setLoaded: (state, data) => (state.loaded = data),

  clearData: state => {
    state.items.length = 0
    state.loaded = false
    state.loading = false
    state.error = null
  },
}

const actions = {
  fetchContent: getContent,

  handleClearContent: clearContent,

  handleActiveTool: setActiveTool,

  handleAnswer: async ({ state, commit }, data) => {
    const item = state.items.find(el => el.id === data.answer.key)
    if (item.user_answer === data.answer.value) return false

    try {
      const response = await saveTranslateAnswer(data.contentId, data.answer)
      commit('setAnswer', data)
      notifier.success(response.data.message)
    } catch (error) {
      notifier.error('Cevabınız kaydedilemedi!')
      console.error(error)
    }
  },

  fetchToolData: async ({ state, commit, dispatch }, payload) => {
    let item = state.items.find(el => el.id === payload.itemId)

    if (item.toolLoaded) return dispatch('handleActiveTool', payload)

    commit('setToolDataLoading', { ...payload, status: true })

    const { data, status } = await getDigibookItemToolData(payload)
    if (status !== 200) {
      commit('setToolDataLoading', { ...data, status: false })
      return console.error(data)
    }

    dispatch('handleActiveTool', payload)
    commit('setToolData', { ...payload, result: data })
    commit('setToolDataLoading', { ...payload, status: false })
    commit('setToolDataLoaded', { ...payload, status: true })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
