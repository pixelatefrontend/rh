import * as http from '../../services/user.service'

const state = {
  appsList: []
}

const getters = {
  allApps: state => state.appsList,
  favoritedApps: state => {
    return state.appsList.filter(item => item.isFavorited === true)
  }
}

const mutations = {
  SET_FAVORITED_APP(state, data) {
    let item = state.appsList.find(el => el.name === data)
    item.isFavorited = !item.isFavorited
  },

  SET_USER_APPS(state, data) {
    state.appsList = data
  }
}

const actions = {
  favoriteApp: ({ commit }, app) => {
    commit('SET_FAVORITED_APP', app)
    http.favoriteApp(app).then(res => {
      commit('SET_USER_APPS', res.data.apps)
    })
  },

  unfavoriteApp: ({ commit }, app) => {
    commit('SET_FAVORITED_APP', app)
    http.unfavoriteApp(app).then(res => {
      commit('SET_USER_APPS', res.data.apps)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
