import _ from 'lodash'
import * as http from '../../services/digibook.service'

const getters = {
  filteredItems: state => state.filteredItems,
  setStatus: state => state.setStatus,
  showItems: state => state.showItems,
  isLoading: state => state.isLoading,
  isLoaded: state => state.isLoaded,
  isDataEnd: state => state.isDataEnd,
  setLevel: state => state.setLevel,
  groupId: state => state.groupId,
  digibooks: state => state.digibooksList,
  loadingMore: state => state.loadingMore,
  hasMore: state => state.more,
}

const mutations = {
  resetData: state => {
    state.filteredItems = []
    state.showItems = false
    state.isLoading = true
    state.setStatus = 2
    state.setLevel = 0
    state.currentPage = 1
    state.status = -1
    state.level = 0
  },
  setData: (state, data) => {
    state.filteredItems = data
    state.isLoading = false
    state.showItems = true
    if (data.length < 10) {
      state.isDataEnd = true
    }
  },
  setDigibooks(state, data) {
    state.filteredItems = data
    state.digibooksList = data
  },
  setLoading(state, value) {
    state.isLoading = value
  },
  setLoaded(state, value) {
    state.isLoaded = value
  },
  updateData(state, data) {
    state.filteredItems.push(...data)
    state.isLoading = false
    state.showItems = true
    if (data.length < 10) {
      state.isDataEnd = true
    }
  },
  increaseCurrentPage(state) {
    state.currentPage += 1
  },
  setCurrentPage(state, value) {
    state.currentPage = value
  },
  setGroupId(state, value) {
    state.groupId = value
  },
  sortBooks(state, selectedOption) {
    if (selectedOption.value === 'new') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.date).reverse()
    }

    if (selectedOption.value === 'old') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.date)
    }

    if (selectedOption.value === 'toLow') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.progress).reverse()
    }

    if (selectedOption.value === 'toHigh') {
      state.filteredItems = _.sortBy(state.filteredItems, item => item.progress)
    }
  },
  setLoadMore(state, value) {
    state.more = value
  },
  setLoadingMore(state, value) {
    state.loadingMore = value
  },
  loadNextItems(state, items) {
    state.digibooksList.push(...items)
  },
}

const actions = {
  getDigibooksByGroup: ({ commit }, groupId) => {
    commit('resetData')
    commit('setLoading', true)
    http.group(groupId).then(res => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  getDigibooksList: ({ commit }, filters) => {
    commit('resetData')
    commit('setLoading', true)
    http.list(filters).then(res => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  getDigibooksListByTime: ({ commit }, filters) => {
    commit('resetData')
    commit('setLoading', true)
    http.listByTime(filters).then(res => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  getDigibooksListByCompletion: ({ commit }, filters) => {
    commit('resetData')
    commit('setLoading', true)
    http.listByCompletion(filters).then(res => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
    })
  },

  loadNext: ({ commit }, filters) => {
    commit('setLoadingMore', true)

    http.list(filters).then(res => {
      commit('setLoadingMore', false)
      commit('loadNextItems', res.data.data)
      commit('setLoadMore', res.data.next_page)
    })
  },

  getDigibooks: async ({ state, commit }, completed) => {
    commit('resetData')
    commit('setLoading', true)
    http.digibooks(state.groupId, state.currentPage).then(res => {
      commit('setDigibooks', res.data.data)
      commit('setLoadMore', res.data.next_page)
      commit('setLoading', false)
      commit('setLoaded', true)
      /* if (completed === true) {
              commit(
                'setData',
                _.filter(data.data, d => d.progress === 100),
              )
            } else if (completed === false) {
              commit(
                'setData',
                _.filter(data.data, d => d.progress < 100),
              )
            } else {
              commit('setData', data.data)
            } */
    })
  },
  loadMore: ({ state, commit }) => {
    commit('increaseCurrentPage')
    commit('setLoading', true)
    if (state.currentGroupName === 'all') {
      http.digibooks(state.currentPage, null).then(({ data }) => {
        commit('updateData', data.data)
      })
    } else {
      http.groups(state.currentGroupName, state.currentPage).then(({ data }) => {
        commit('updateData', data.data)
      })
    }
  },
  filterGroups: ({ state, commit }, group) => {
    commit('setCurrentPage', 1)
    commit('resetData')
    http.digibooks(group.id, state.currentPage).then(({ data }) => {
      commit('setData', data.data)
    })
  },
  setGroupId({ commit }, id) {
    commit('setGroupId', id)
  },
}

const state = {
  filteredItems: [],
  digibooksList: [],
  showItems: true,
  isLoading: true,
  isLoaded: false,
  isDataEnd: false,
  currentPage: 1,
  setLevel: 0,
  setStatus: 2,
  level: 0,
  groupId: -1,
  currentGroupName: 'all',
  loadingMore: false,
  more: false,
}

export default {
  namespaced: true,
  state,
  actions,
  getters,
  mutations,
}
