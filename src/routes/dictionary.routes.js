export default [
  {
    path: '/sozluk',
    component: () => import(/* webpackChunkName: "dictionary" */ '../views/Dictionary/'),
    children: [
      {
        path: '',
        name: 'DictionaryLanding',
        component: () => import(/* webpackChunkName: "dictionary" */ '../views/Dictionary/DictionaryLanding'),
        meta: {
          title: 'Sözlük',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'dictionary'
        },
      },
      {
        path: ':word',
        name: 'DictionaryResult',
        component: () => import(/* webpackChunkName: "dictionary" */ '../views/Dictionary/DictionaryResult'),
        meta: {
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: false,
          noteName: 'dictionary'
        }
      }
    ]
  }
]
