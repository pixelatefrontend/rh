export default [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "home" */ '../views/Home'),
    children: [
      {
        path: '',
        name: 'HomeSchedule',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home/Schedule'),
        meta: {
          toolbarVisible: true,
          requiresAuth: true,
          title: 'Çalışma Takvimi',
          hasTour: true,
          tourName: 'homeTour',
          headerShow: true,
          noteName: 'general',
          pageClass: ''
        }
      },
      {
        path: 'performansim',
        name: 'HomePerformance',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home/Performance'),
        meta: {
          toolbarVisible: true,
          requiresAuth: true,
          headerShow: true,
          hasTour: true,
          tourName: 'homeTour',
          title: 'Performans',
          noteName: 'general',
          pageClass: ''
        }
      },
      {
        path: 'liderlik-tablosu',
        name: 'HomeLeaderboard',
        component: () => import(/* webpackChunkName: "home" */ '../views/Home/Leaderboard'),
        meta: {
          toolbarVisible: true,
          headerShow: true,
          requiresAuth: true,
          hasTour: true,
          tourName: 'homeTour',
          title: 'Liderlik Tablosu',
          noteName: 'general',
          pageClass: ''
        }
      }
    ]
  },
]
