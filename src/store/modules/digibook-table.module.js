import { notifier } from '../../common/helpers'
import { saveTableAnswer } from '../../services/digibook.service'
import shared from './digibook-shared.module'

const state = {}

const getters = {
  answers: state => state.shared.content.data.answers,
  keys: state => state.shared.content.data.keys,
  tableContent: state => state.shared.content.data.items,
}

const mutations = {
  setAnswerData: (state, data) => {
    state.shared.content.data.answers[data.answerId] = data.answerData
  },
}

const actions = {
  handleAnswerData: async ({ commit }, payload) => {
    try {
      const response = await saveTableAnswer(payload)
      commit('setAnswerData', payload)
      notifier.success(response.data.message)
    } catch (error) {
      console.error(error)
      notifier.success('Cevap kaydetme başarısız!')
    }
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules: {
    shared,
  },
}
