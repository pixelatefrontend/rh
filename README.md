# rh

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Vue Tour Nasil Kullanilir?

Herhangi bir .vue uzantili _view_ icerisine girin.

#### View'e vue-tour'u import et
```
import Vue from 'vue'
import VueTour from 'vue-tour'
require('vue-tour/dist/vue-tour.css')
Vue.use(VueTour)
```
```
components: {
    VueTour,
},
```
Bu satiri ekledikten sonra <template> icerisine alttaki elementi ekleyin. 
```
<v-tour name="myTour" :steps="steps" :options="optionsOfTour"></v-tour>
```

#### Ardindan bu satiri data() icerisine ekleyin. Bu label namelerini olusturmaya yarayacak.
```
optionsOfTour: {
    labels: {
      buttonSkip: 'Sonlandır',
      buttonPrevious: 'Önceki',
      buttonNext: 'Devam Et',
      buttonStop: 'Bitir'
    },
},
```

#### Yine bu satirida data() icerisine ekleyin. Bu sekmelerin her biri tour'un bir adimini temsil eder ve siralama ayni buradaki gibi isler.
```
steps: [
    {
      target: '.element-class', 
      content: `<strong class="guide-title"> <svg data-v-4585df58="" viewBox="0 0 20 20" class="icon"><use xlink:href="/icon/icons.svg#icon-info"></use></svg> Baslik</strong> <p class="guide-desc">Aciklama</p>`,
    },
    {
      target: '.element-classes',
      content: `<strong class="guide-title"> <svg data-v-4585df58="" viewBox="0 0 20 20" class="icon"><use xlink:href="/icon/icons.svg#icon-info"></use></svg> Baslik</strong> <p class="guide-desc">Aciklama</p>`,
    }
],
```

Target icerisinde bulunan classname icin hangi elementi istiyorsaniz, onu chrome uzerinden inspect ederek unique bir secici olusturup ilerleyebilirsiniz.
