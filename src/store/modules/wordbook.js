import * as http from '@/api/wordbook'

const state = {
  exploreList: [],
  followingList: [],
  listLoaded: false,
  listLoading: false,
  more: false,
  moreLoaded: false,
  moreLoading: false,
  perPage: 10,
  usersList: [],
  listMassegeStatus: false,
  listMessageText: ''
}

const actions = {
  getUsersList: ({ commit }) => {
    commit('LIST_LOADING', true)
    commit('LIST_LOADED', false)
    commit('LIST_MORE', false)
    http.usersList().then((res) => {
      commit('LIST_LOADING', false)
      commit('LIST_LOADED', true)
      commit('SET_USERSLIST', res.data.data)
      console.log(res)
      commit('LIST_MORE', res.data.next_page)
    })
  },
  getExploreList: ({ commit }) => {
    commit('LIST_LOADING', true)
    commit('LIST_MORE', false)
    http.exploreList().then((res) => {
      commit('LIST_LOADING', false)
      commit('SET_EXPLORELIST', res.data.data)
      commit('LIST_MORE', res.data.next_page)
    })
  },
  removeList: ({ commit }, id) => {
    commit('LIST_MORE', false)
    http.removeList(id).then((res) => {
      commit('REMOVE_LIST', id)
      console.log(res)
      /* commit('SET_USERSLIST', res.data.data)
      commit('LIST_MORE', res.data.next_page) */
    })
  },
  addList: ({ commit }, data) => {
    http.addList(data).then((res) => {
      commit('LIST_CREATION_STATUS', res.data.message)
      commit('SET_LIST_MESSAGE_STATU', true)
      setTimeout(() => {
        commit('SET_LIST_MESSAGE_STATU', false)
      }, 3000)
    })
  },
  editList: ({ commit }, data) => {
    console.log('store edit id: ', data.id)
    http.editList(data).then((res) => {
      commit('LIST_CREATION_STATUS', res.data.message)
      commit('SET_LIST_MESSAGE_STATU', true)
      setTimeout(() => {
        commit('SET_LIST_MESSAGE_STATU', false)
      }, 3000)
    })
  }
}

const mutations = {
  SET_LIST_MESSAGE_STATU(state, status) {
    state.listMassegeStatus = status
  },
  LIST_CREATION_STATUS(state, message) {
    state.listMessageText = message
  },
  REMOVE_LIST(state, id) {
    state.usersList.splice(state.usersList.findIndex(el => el.id === id), 1)
  },
  LIST_LOADING(state, status) {
    state.listLoading = status
  },
  LIST_LOADED(state, status) {
    state.listLoaded = status
  },
  LIST_MORE(state, status) {
    state.more = status
  },
  LIST_MORE_LOADING(state, status) {
    state.moreLoading = status
  },
  LIST_MORE_LOADED(state, status) {
    state.moreLoaded = status
  },
  SET_USERSLIST(state, list) {
    state.usersList = list
  },
  SET_EXPLORELIST(state, list) {
    state.exploreList = list
  }
}

const getters = {
  listMassegeStatus: state => state.listMassegeStatus,
  listMessageText: state => state.listMessageText,
  exploreList: state => state.exploreList,
  listLoaded: state => state.listLoaded,
  listLoading: state => state.listLoading,
  more: state => state.more,
  moreLoaded: state => state.moreLoaded,
  moreLoading: state => state.moreLoading,
  usersList: state => state.usersList,
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
