import Vue from 'vue'

export const login = (email, password, remember) =>
  Vue.$axios('POST', '/login', { email, password, remember })

export const logout = () => Vue.$axios('POST', '/logout')

export const forgot = email => Vue.$axios('POST', `/forgot-password?email=${email}`)

export const handle2fa = () => Vue.$axios('GET', `/user/2fa/handle`)

export const verifyPhone = (countryId, phone) => Vue.$axios('POST', `/user/2fa/handle?country_id=${countryId}&phone=${phone}`)

export const verifyCode = code => Vue.$axios('POST', `/user/2fa/verify?code=${code}`)

export const register = data =>
  Vue.$axios(
    'POST',
    `/register?firstname=${data.firstName}&last_name=${data.lastName}&email=${data.email}&password=${data.password}`
  )

export const updatePassword = data =>
  Vue.$axios(
    'POST',
    `/update-password?token=${data.token}&password=${data.password}&password_confirmation=${data.passwordRepeat}`
  )
