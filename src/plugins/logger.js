/* eslint-disable no-console */
/* eslint-disable no-param-reassign */
import config from '@/config/index'

const { logLevel } = config

export default {
  install (Vue) {
    // eslint-disable-next-line
    console.log(`
       _          _       _       
 _ __ (_)_  _____| | __ _| |_ ___ 
| '_ \\| \\ \\/ / _ \\ |/ _\` | __/ _ \\
| |_) | |>  <  __/ | (_| | ||  __/
| .__/|_/_/\\_\\___|_|\\__,_|\\__\\___|
|_|                               
`)
    Vue.logger = {
      start () {},
      trace (...args) {
        if (['trace'].includes(logLevel)) {
          console.trace(...args)
        }
      },
      log (...args) {
        if (['trace', 'log'].includes(logLevel)) {
          console.log(...args)
        }
      },
      debug (...args) {
        if (['trace', 'log', 'debug'].includes(logLevel)) {
          console.debug(...args)
        }
      },
      info (...args) {
        if (['trace', 'log', 'debug', 'info'].includes(logLevel)) {
          console.info(...args)
        }
      },
      warn (...args) {
        if (['trace', 'log', 'debug', 'info', 'warn'].includes(logLevel)) {
          console.warn(...args)
        }
      },
      error (...args) {
        if (['trace', 'log', 'debug', 'info', 'warn', 'error'].includes(logLevel)) {
          console.error(...args)
        }
      }
    }
  }
}
