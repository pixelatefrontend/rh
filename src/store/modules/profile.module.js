import Vue from 'vue'
import {
  getUserProfile,
  getUserAddresses,
  removeAddress,
  getAddressItem,
  editAddress,
  newAddress,
  saveDefaultAvatar,
  saveProfile,
  updateEmailNotifications,
} from '../../services/account.service'

const state = {
  userKey: '',
  profile: {
    firstName: '',
    lastName: '',
    email: '',
    phone: '',
    jobDefinitionId: 0,
    educationDefinitionId: 0,
    profilePicture: '',
    birthday: '',
    gender: '',
    about: '',
  },
  addressList: [],
  currentAddress: {
    name: '',
    surname: '',
    addressName: '',
    address: '',
    tckn: '',
    phone: '',
    countryId: '',
    cityId: '',
    townId: '',
    districtId: '',
  },
  emailNotifications: [],
  addressLoaded: false,
  actionDone: false,
  actionStatus: '',
  isLoading: false,
  isProfileLoaded: false,
  emailPreferencesList: {},
  isAddressesLoaded: false,
}

const mutations = {
  clearProfile(state) {
    state.profile = {
      firstName: '',
      lastName: '',
      email: '',
      phone: '',
      jobDefinitionId: 0,
      educationDefinitionId: 0,
      profilePicture: '',
      birthday: '',
      gender: '',
      about: '',
    }
    state.addressList = []
    state.emailNotifications = []
    state.emailPreferencesList = {}
  },

  clearCurrentAddress(state) {
    state.currentAddress = {
      name: '',
      surname: '',
      addressName: '',
      address: '',
      tckn: '',
      phone: '',
      countryId: '',
      cityId: '',
      townId: '',
      districtId: '',
    }
  },

  setLoading(state, data) {
    state.isLoading = data
  },

  setLoaded(state, data) {
    state.addressLoaded = data
  },

  setProfile(state, data) {
    state.profile = data
  },

  setProfileLoaded(state, data) {
    state.isProfileLoaded = data
  },

  setEmailNotifications(state, data) {
    state.emailNotifications = data
  },

  setAddresses(state, data) {
    state.addressList = data
  },

  setProfileData(state, data) {
    console.log('mutate: ', data)
    Vue.set(state.profile, data.key, data.value)
  },

  setNotificationData(state, data) {
    let elem = state.emailPreferencesList.prefs.findIndex(
      el => el.key === data.key
    )
    Vue.set(state.emailPreferencesList.prefs[elem], 'status', data.value)
  },

  setAddressData(state, data) {
    Vue.set(state.currentAddress, data.key, data.value)
  },

  setAddressesLoaded(state, data) {
    state.isAddressesLoaded = data
  },

  addAddress(state, data) {
    state.addressList.push(data)
  },

  setCurrentAddress(state, data) {
    state.currentAddress = data
  },

  modifyAddressItem(state, data) {
    let target = state.addressList.find(el => el.id === data.id)
    target.name = data.value
  },

  clearAddressFromList(state, data) {
    let removal = state.addressList.findIndex(el => el.id === data)
    state.addressList.splice(removal, 1)
  },

  saveProfile(state, data) {
    state.actionDone = data.done
    state.actionStatus = data.status
  },

  setEmailPermissions(state, data) {
    state.emailPreferencesList.prefs = data
  },

  setProfilePicture(state, data) {
    state.profile.profilePicture = data
  },
}

const actions = {
  clearUserProfile({ commit }) {
    commit('clearProfile')
  },

  getProfile({ commit }) {
    if (!state.isProfileLoaded) {
      getUserProfile().then(res => {
        console.log('profile data', res.data)
        commit('setProfile', res.data)
        let prefs = res.data.notificationPermissions
        let permissions = []
        prefs.forEach(el => {
          permissions.push(...el.permissions)
        })
        commit('setEmailPermissions', permissions)
        commit('setEmailNotifications', res.data.notificationPermissions)
        commit('setProfileLoaded', true)
      })
    }
  },

  updateNotificationData({ commit }, data) {
    commit('setNotificationData', data)
  },

  setNotificationPreferences({ commit }, data) {
    commit('setLoading', true)
    updateEmailNotifications(data)
      .then(res => {
        commit('setLoading', false)
        commit('saveProfile', {
          done: true,
          status: 'OK',
        })
        console.log(res.data)
      })
      .catch(err => {
        commit('setLoading', false)
        commit('saveProfile', {
          done: true,
          status: 'OK',
        })
        console.log('err: ', err)
      })
  },

  updateProfileData({ commit }, data) {
    commit('setProfileData', data)
  },

  async saveProfileData({ commit }, data) {
    console.log('stored data: ', data)
    commit('setLoading', true)
    saveProfile(data)
      .then(res => {
        commit('setLoading', false)
        commit('saveProfile', {
          done: true,
          status: 'OK',
        })
        console.log(res)
      })
      .catch(err => {
        commit('setLoading', false)
        commit('saveProfile', {
          done: true,
          status: 'ERR',
        })
        console.log(err)
      })
  },

  saveUserAvatar({ commit }, data) {
    saveDefaultAvatar(data)
      .then(res => {
        console.log('avatar status: ', res)
      })
      .catch(err => {
        console.log('avatar status: ', err)
      })
  },

  handleProfilePicture({ commit }, data) {
    commit('setProfilePicture', data)
  },

  async getAddresses({ commit }) {
    getUserAddresses().then(res => {
      commit('setAddresses', res.data)
      commit('setAddressesLoaded', true)
    })
  },

  createAddress({ commit }, data) {
    commit('setLoaded', false)
    newAddress(data)
      .then(res => {
        console.log(res.data)
        let newItem = {
          id: res.data.id,
          name: res.data.addressName,
          address: res.data.fullAddress,
        }
        commit('setLoading', false)
        commit('saveProfile', {
          done: true,
          status: 'OK',
        })
        commit('addAddress', newItem)
        console.log(res)
      })
      .catch(err => {
        commit('setLoading', false)
        commit('saveProfile', {
          done: true,
          status: 'ERR',
        })
        console.log('err: ', err)
      })
  },

  clearAddress({ commit }, id) {
    removeAddress(id).then(res => {
      console.log(res.data)
      commit('clearAddressFromList', id)
    })
  },

  getSingleAddress({ commit }, id) {
    commit('setLoaded', false)
    getAddressItem(id).then(res => {
      commit('setLoaded', true)
      commit('setCurrentAddress', res.data)
    })
  },

  modifyAddress({ commit }, data) {
    commit('setLoaded', false)
    editAddress(data).then(res => {
      console.log(res.data)
      let modifyItem = {
        id: data.id,
        value: res.data.addressName,
      }
      commit('modifyAddressItem', modifyItem)
      commit('setLoaded', true)
      commit('setCurrentAddress', res.data)
    })
  },

  updateAddressData({ commit }, data) {
    commit('setAddressData', data)
  },
}

const getters = {
  emailPreferencesList: state => state.emailPreferencesList,
  profileLoaded: state => state.isProfileLoaded,
  userAvatar: state =>
    'https://www.remzihoca.com/' + state.profile.profilePicture,
  profilePicture: state => state.profile.profilePicture,
  userFullName: state => state.profile.firstName + ' ' + state.profile.lastName,
  formattedBirthday: state => new Date(state.profile.birthday * 1000),
  loading: state => state.isLoading,
  addressIsLoaded: state => state.addressLoaded,
  userProfile: state => state.profile,
  firstName: state => state.profile.firstName,
  lastName: state => state.profile.lastName,
  addresses: state => state.addressList,
  addressesLoaded: state => state.isAddressesLoaded,
  selectedAddress: state => state.currentAddress,
  emailPreferences: state => state.emailNotifications,
  action: state => {
    return {
      status: state.actionStatus,
      done: state.actionDone,
    }
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
