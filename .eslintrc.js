module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    '@vue/standard'
  ],
  rules: {
    'no-console': 'off',
    'no-debugger': 'error',
    'comma-dangle': 0,
    "vue/no-unused-vars": "warning",
    'space-before-function-paren': 'off',
    'standard/computed-property-even-spacing': 'off',
    "vue/no-use-v-if-with-v-for": "off"
  },
  parserOptions: {
    parser: 'babel-eslint'
  }
}
