import Vue from 'vue'

export const getUserProfile = () => Vue.$axios('GET', '/account/profile')

export const updateProfileData = (payload) => Vue.$axios('POST', `/account/profile?firstName=${payload.firstName}&lastName=${payload.lastName}&email=${payload.email}&phone=${payload.phone}`)

export const updatePassword = (payload) => Vue.$axios('POST', `/account/reset-password?old_password=${payload.oldPassword}&new_password=${payload.newPassword}&new_password_confirmation=${payload.newPasswordRepeat}`)

export const saveProfile = user => Vue.$axios('POST',
  `/account/profile?firstName=${user.firstName}&lastName=${user.lastName}&phone=${user.phone}&gender=${user.gender}&birthday=${user.birthday}&about=${user.about}&jobDefinitionId=${user.jobDefinitionId}&educationDefinitionId=${user.educationDefinitionId}&showOnProfile=${user.showOnProfile}`)

export const saveDefaultAvatar = user => Vue.$axios('POST',
  `/account/profile?firstName=${user.firstName}&lastName=${user.lastName}&defaultProfilePhotoNumber=${user.defaultProfilePhotoNumber}`)

export const updateEmailNotifications = payload => Vue.$axios('POST', `/account/profile?firstName=${payload.firstName}&lastName=${payload.lastName}&${payload.prefs}`)

export const getUserAddresses = () => Vue.$axios('GET', '/account/addresses')

export const getAddressItem = id => Vue.$axios('GET', `/account/addresses/${id}`)

export const removeAddress = id => Vue.$axios('DELETE', `/account/addresses/${id}`)

export const newAddress = payload => Vue.$axios('POST',
  `/account/addresses?addressName=${payload.addressName}&name=${payload.name}&surname=${payload.surname}&tckn=${payload.tckn}&phone=${payload.phone}&countryId=${payload.countryId}&cityId=${payload.cityId}&townId=${payload.townId}&districtId=${payload.districtId}&address=${payload.address}`)

export const editAddress = payload => Vue.$axios('PUT',
  `/account/addresses/${payload.id}?addressName=${payload.addressName}&name=${payload.name}&surname=${payload.surname}&tckn=${payload.tckn}&phone=${payload.phone}&countryId=${payload.countryId}&cityId=${payload.cityId}&townId=${payload.townId}&districtId=${payload.districtId}&address=${payload.address}`)
