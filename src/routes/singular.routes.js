export default [
  {
    path: '/bildirimler',
    name: 'Notifications',
    component: () =>
      import(
        /* webpackChunkName: "notifications" */ '../views/Singular/NotificationsPage'
      ),
    meta: {
      title: 'Bildirimler',
      requiresAuth: true,
      headerShow: true,
      toolbarVisible: true,
      pageClass: 'header--gray',
      hasTour: false,
    },
  },
  {
    path: '/yardim-merkezi',
    name: 'Faq',
    component: () =>
      import(/* webpackChunkName: "faq" */ '../views/Singular/FaqPage'),
    meta: {
      title: 'Yardım Merkezi',
      requiresAuth: true,
      headerShow: true,
      toolbarVisible: true,
      pageClass: 'header--gray',
    },
  },
  {
    path: '/icon-sprite',
    name: 'IconSprite',
    component: () =>
      import(
        /* webpackChunkName: "iconsprite" */ '../views/Singular/IconSprite'
      ),
    meta: {
      requiresAuth: true,
      headerShow: false,
      toolbarVisible: false,
    },
  },
  {
    path: '*',
    name: 'NotFound',
    component: () => import('../views/Singular/NotFoundPage.vue'),
    meta: {
      requiresAuth: false,
      headerShow: false,
      toolbarVisible: false,
    },
  },
]
