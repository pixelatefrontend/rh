import Vue from 'vue'

export const readings = page => Vue.$axios('GET', `/readings?_page=${page}`)

export const next = (id, page, open) => Vue.$axios('GET', `/readings?group_id=${id}&page=${page}&is_opened=${open}`)

export const opened = (id, page, open) => Vue.$axios('GET', `/readings?group_id=${id}&page=${page}&is_opened=${open}`)

export const groups = (group, page) => Vue.$axios('GET', `/readings?group=${group}&_page=${page}`)

export const getAll = id => Vue.$axios('GET', `/readings?group_id=${id}`)

export const info = () => Vue.$axios('GET', `/readings/info`)

export const reading = (id, list) => Vue.$axios('GET', `/readings/${id}?reading_list_id=${list}`)

export const read = (id, list) => Vue.$axios('GET', `/readings/is_read?id=${id}?reading_list_id=${list}`)

export const fetchAll = id => Vue.$axios('GET', `/readings?group_id=${id}`)

export const story = (id, page, open, level) => Vue.$axios('GET', `/readings?group_id=${id}&page=${page}&is_opened=${open}&level=${level}`)

export const solve = (list, pid, qid, option) => Vue.$axios('POST', `/readings/solve-question?reading_list_id=${list}&paragraph_id=${pid}&question_id=${qid}&selected_question_option_id=${option}`)

export const finish = (list, pid) => Vue.$axios('POST', `/readings/view-answers?reading_list_id=${list}&paragraph_id=${pid}`)

export const group = id => Vue.$axios('GET', `/readings?group_id=${id}`)

export const list = filters => Vue.$axios('GET',
  `/readings?group_id=${filters.groupId}&page=${filters.page}&is_opened=${filters.isRead}&level=${filters.levelId}`)
