import Vue from 'vue'
import { getAllOrders, giveProduct } from '../../services/orders.service'

const state = {
  ordersList: [],
  productsList: [],
  discountsList: {},
  giveProductLoading: false,
  giveStatus: {},
  productsLoaded: false
}

const getters = {
  results: state => state.ordersList,
  orders: state => state.ordersList.orders,
  products: state => state.productsList,
  giveProductLoading: state => state.giveProductLoading,
  productCount: state => state.productsList.length,
  discounts: state => state.ordersList.discounts,
  giveStatus: state => state.giveStatus,
  productsLoaded: state => state.productsLoaded
}

const mutations = {
  setOrdersList(state, data) {
    state.ordersList = data
    state.productsList = data.products
  },

  setProductUserData(state, data) {
    Vue.set(state.productsList[data.productIndex].users[data.index - 1], data.field, data.value)
  },

  setGiveProductLoading(state, data) {
    state.giveProductLoading = data
  },

  setGiveStatus(state, data) {
    state.giveStatus = data
  },

  addCoupon(state, data) {
    state.ordersList.discounts.coupons.push(data)
  },

  clearDiscountAmount(state) {
    state.ordersList.discounts.loyaltyDiscountAmount = 0
  },

  addNewUser(state, data) {
    let blankUser = {
      id: null,
      name: null,
      email: null,
      isOwner: false
    }
    state.productsList[data].users.push(blankUser)
  },

  updateProductUser(state, data) {
    Vue.set(state.productsList[data.productIndex].users, data.index, data.user)
  },

  setProductsLoaded(state, data) {
    state.productsLoaded = data
  }
}

const actions = {
  getOrdersList({ commit }) {
    getAllOrders().then(res => {
      commit('setOrdersList', res.data)
      commit('setProductsLoaded', true)
    })
  },

  addProductUser({ commit }, data) {
    commit('setGiveProductLoading', true)
    giveProduct(data)
      .then(res => {
        let update = {
          user: res,
          productIndex: data.product,
          index: data.index
        }
        commit('setGiveProductLoading', false)
        commit('setGiveStatus', res)
        if (res.id) {
          commit('updateProductUser', update)
        }
      })
      .catch(err => {
        console.log('error give: ', err)
        commit('setGiveProductLoading', false)
      })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
