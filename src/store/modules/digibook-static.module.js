const state = {
  contentsList: null,
  content: null,
  contentId: 0,
}

const getters = {
  contentsList: state => state.contentsList,
  content: state => state.content,
  title: state => state.content.title,
}

const mutations = {
  setContentsList: (state, data) => (state.contentsList = data),
  setContent: (state, slug) => {
    state.content = state.contentsList.find(el => el.link === slug)
  },
}

const actions = {
  handleContentsList: ({ commit }, data) => {
    commit('setContentsList', data)
  },

  handleContent: ({ commit }, slug) => {
    commit('setContent', slug)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
