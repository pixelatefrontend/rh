import Vue from 'vue'
import Vuex from 'vuex'
import common from '@/store/modules/common'
import user from '@/store/modules/user'
import cluebank from '@/store/modules/cluebank'
import dictionary from '@/store/modules/dictionary.module'
// import digibook from '@/store/modules/digibook'
import digibook from './modules/digibook.module'
// import digibookIndex from '@/store/modules/digibook-index.module'
import digibookContent from '@/store/modules/digibook-content.module'
import digibookPassage from '@/store/modules/digibook-passage.module'
import digibookList from '@/store/modules/digibook-list.module'
import readingList from '@/store/modules/reading-list.module'
import wordy from '@/store/modules/wordy'
import wordyList from '@/store/modules/wordy-list.module'
import denemeList from '@/store/modules/deneme-list.module'
import denemeDetail from '@/store/modules/deneme-detail.module'
import denemeReport from '@/store/modules/deneme-report.module'
import testList from '@/store/modules/test-list.module'
import testDetail from '@/store/modules/test-detail.module'
import testReport from '@/store/modules/test-report.module'
import wordbook from '@/store/modules/wordbook'
import notifications from '@/store/modules/notifications.module'
import favorites from '@/store/modules/favorites'
import profile from '@/store/modules/profile.module'
import calendar from '@/store/modules/calendar.module'
import orders from '@/store/modules/orders.module'
import leaderboard from '@/store/modules/leaderboard.module'
import performance from '@/store/modules/performance.module'
import leaderboardDetail from '@/store/modules/leaderboard-detail.module'
import rosette from '@/store/modules/rosette.module'
import wordyRosette from '@/store/modules/wordy-rosette.module'
import store from '@/store/modules/store.module'
import notes from '@/store/modules/notes.module'
import homeworks from '@/store/modules/homeworks.module'
import questions from '@/store/modules/questions.module'
import faq from '@/store/modules/faq.module'
import toolbar from '@/store/modules/toolbar.module'
import wordbookMyList from '@/store/modules/wordbook-mylist.module'
import wordbookExploreList from '@/store/modules/wordbook-explorelist.module'
import wordbookList from '@/store/modules/wordbook-list.module'
import wordbookPlaying from '@/store/modules/wordbook-playing.module.js'
import wordbookplugin from '@/store/modules/workbook-plugin.module'
import documents from '@/store/modules/documents.module'
import lessons from './modules/lessons.module'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    common,
    user,
    cluebank,
    dictionary,
    digibook,
    // digibookIndex,
    digibookContent,
    digibookPassage,
    digibookList,
    lessons,
    readingList,
    wordy,
    wordyList,
    denemeList,
    denemeDetail,
    denemeReport,
    testList,
    testDetail,
    testReport,
    wordbook,
    notifications,
    favorites,
    profile,
    calendar,
    orders,
    leaderboard,
    performance,
    leaderboardDetail,
    rosette,
    wordyRosette,
    store,
    notes,
    homeworks,
    questions,
    faq,
    toolbar,
    wordbookMyList,
    wordbookExploreList,
    wordbookList,
    wordbookPlaying,
    wordbookplugin,
    documents
  },
  strict: debug
})
