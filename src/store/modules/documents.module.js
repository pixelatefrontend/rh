import * as http from '../../services/documents.service'

const state = {
  groups: [],
  limit: 10,
  more: false,
  loadingMore: false,
  activeGroup: -1,
  categoryFilter: 0,
  categoryList: [],
  currentPage: 0,
  documents: [],
  documentsGroup: [],
  filteredList: [],
  groupLength: null,
  isLoading: true,
  isLoaded: false,
  loadMore: false,
  perPage: 10,
  readFilter: 2,
  stateLoad: true,
  searchResults: [],
}

const actions = {
  getDocumentsList: ({ commit }, filters) => {
    commit('CLEAR_LIST')
    commit('SET_LOADING', true)

    http.list(filters).then((res) => {
      commit('SET_DOCUMENTS', res.data.data)
      commit('SET_LOADMORE', res.data.next_page)
      commit('SET_LOADING', false)
      commit('SET_LOADED', true)
    })
  },
  getDocumentsByGroup: ({ commit }, groupId) => {
    commit('CLEAR_LIST')
    commit('SET_LOADING', true)

    http.group(groupId).then((res) => {
      commit('SET_DOCUMENTS', res.data.data)
      commit('SET_LOADMORE', res.data.next_page)
      commit('SET_LOADING', false)
      commit('SET_LOADED', true)
    })
  },
  loadNext: ({ commit }, filters) => {
    commit('SET_LOADING_MORE', true)

    http.list(filters).then((res) => {
      commit('SET_LOADING_MORE', false)
      commit('LOAD_NEXT', res.data.data)
      commit('SET_LOADMORE', res.data.next_page)
    })
  },
}

const mutations = {
  SET_LOADING_MORE(state, status) {
    state.loadingMore = status
  },
  SET_LOADING(state, status) {
    state.isLoading = status
  },
  SET_LOADED(state, status) {
    state.isLoaded = status
  },
  CLEAR_LIST(state) {
    state.filteredList = []
  },
  SET_DOCUMENTS(state, documents) {
    state.filteredList = documents
  },
  SET_LOADMORE(state, status) {
    state.more = status
  },
  LOAD_NEXT(state, docs) {
    state.filteredList.push(...docs)
  }
}

const getters = {
  listLoaded: state => state.isLoaded,
  hasMore: state => state.more,
  isEmpty: state => state.filteredList.length === 0 && state.isLoading === false,
  documentItems: state => state.filteredList,
  isLoading: state => state.isLoading,
  loadingMore: state => state.loadingMore
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
