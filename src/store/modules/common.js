import {
  SET_OVERLAY,
  SET_HEADER,
  SET_PRELOADER,
  SET_ACCOUNT_DROPDOWN,
  SET_WORDY_RESULTS,
  SET_KEYPRESS,
  SET_APPS_VISIBLE,
  SET_NOTIFICATIONS_VISIBLE,
  SET_TOOLBAR_VISIBLE,
  SET_QUESTIONS_TOOLBAR,
  SET_PAGE_CLASS,
  SET_DARKMODE,
} from '@/store/mutations.type'

import {
  SHOW_OVERLAY,
  SHOW_HEADER,
  SHOW_PRELOADER,
  SHOW_ACCOUNT_DROPDOWN,
  SHOW_WORDY_RESULTS,
  SHOW_KEYPRESS,
  SHOW_APPS,
  SHOW_NOTIFICATIONS,
  SHOW_TOOLBAR,
  SHOW_QUESTIONS_TOOLBAR,
  GET_PAGE_CLASS,
  SWITCH_DARKMODE,
} from '@/store/actions.type'
import {
  GET_APPS,
  DELETE_MODAL,
  SET_HEADER_CLASS,
  SHOW_GROUPS,
  GET_PROFILE,
  UPDATE_ACCOUNT_INFO,
  SHOW_FEEDBACK_MODAL,
  SHOW_COMMUNITY_MODAL,
  GET_COMMUNITIES,
  COMMUNITIES_SEND,
  HANDLE_LOGIN_STATUS,
  SHOW_OVERLAY_ALT,
  GET_COUNTRY_CODES,
  SET_PREVIOUS_URL,
  SYSTEM_AVAILABILITY,
  SHOW_WORDBOX_MODAL,
  HANDLE_OTHER_WORDS,
  HANDLE_CLUE_MODAL,
  CLUE_MODAL_VISIBLE,
  HANDLE_STATUS_CODE,
} from '../actions.type'

import {
  SET_APPINFO,
  SET_APP_GROUP,
  SET_APPINFO_LOAD,
  SET_APPS,
  SET_APPS_LOAD,
  SET_DELETE_MODAL,
  SET_HEADER_CLASS_NAME,
  TOGGLE_SHOW_GROUPS,
  SET_PROFILE,
  SET_ACCOUNT_INFO,
  SET_RESPONSE_MESSAGE,
  SET_PROFILE_LOADED,
  TOGGLE_FEEDBACK_MODAL,
  TOGGLE_COMMUNITY_MODAL,
  SET_COMMUNITIES,
  SET_COMMUNITIES_LOADED,
  SET_HIGHLIGHT_TEXT,
  SET_LOGIN_STATUS,
  SET_OVERLAY_ALT,
  SET_COUNTRY_CODES,
  HANDLE_SET_PREVIOUS_URL,
  SET_SYSTEM_AVAILABILITY,
  SET_WORDBOX_MODAL_VISIBILITY,
  SET_OTHER_WORDS,
  SET_CLUE_MODAL,
  SHOW_CLUE_MODAL,
  SET_DARKMODE_DETAIL,
  SET_STATUS_CODE,
} from '../mutations.type'
import {
  shareCommunity,
  getCommunities,
  getPlatformApps,
  getCountries,
  checkTranslatableStatus,
} from '../../services/common.service'
import {
  updateProfileData,
  getUserProfile,
} from '../../services/account.service'

const state = {
  isAuthorized: false,
  darkMode: false,
  showOverlay: false,
  showOverlayAlt: false,
  showPreloader: false,
  showHeader: false,
  showAccountDropdown: false,
  showWordyResult: false,
  showKeypress: false,
  showApps: false,
  showNotifications: false,
  showToolbar: false,
  pageClass: '',
  headerClass: '',
  groupsDropdownVisible: false,
  previousUrl: '/',
  showQuestionsToolbar: true,
  appInfo: {},
  appInfoDetail: [
    {
      name: 'documents',
      title: 'Doküman',
      icon: require('@/assets/img/app-dokuman-white.svg'),
      background: '#cc5d46',
    },
    {
      name: 'lessons',
      title: 'Ders',
      icon: require('@/assets/img/app-lesson-white.svg'),
      background: '#0ea6ce',
    },
    {
      name: 'readings',
      title: 'Okuma',
      icon: require('@/assets/img/app-okuma-white.svg'),
      background: '#813f8e',
    },
    {
      name: 'exams',
      title: 'Deneme',
      icon: require('@/assets/img/app-deneme-white.svg'),
      background: '#67419d',
    },
    {
      name: 'tests',
      title: 'Test',
      icon: require('@/assets/img/app-test-white.svg'),
      background: '#214f86',
    },
    {
      name: 'dictionary',
      title: 'Sözlük',
      icon: require('@/assets/img/app-sozluk-white.svg'),
      background: '#3759a1',
    },
    {
      name: 'wordy',
      title: 'Wordy',
      icon: require('@/assets/img/app-wordy-white.svg'),
      background: '#2E8273',
    },
    {
      name: 'cluebank',
      title: 'İpucu Havuzu',
      icon: require('@/assets/img/app-cluebank-white.svg'),
      background: null,
    },
    {
      name: 'digibook',
      title: 'Digibook',
      icon: require('@/assets/img/app-digibook-white.svg'),
      background: '#A6487D',
    },
  ],
  appInfoLoaded: false,
  lastActiveTab: -1,
  appsList: [],
  appsListLoaded: false,
  deleteModal: false,
  profile: null,
  profileLoaded: false,
  accountInfo: {
    email: null,
    phone: null,
    firstName: null,
    lastName: null,
  },
  responseMessage: null,
  feedbackModalVisible: false,
  communityModalVisible: false,
  communities: [],
  communitiesLoaded: false,
  highlightText: {
    text: '',
    status: true,
  },
  countryCodeList: [],
  offlineCount: 0,
  wordboxModalVisible: false,
  otherWords: [],
  clueModal: {
    title: null,
    body: null,
    isActive: false,
    id: null,
    favorited: null,
  },
  onMobile: false,
  isTranslatable: true,
  responseStatusCode: 200
}

const mutations = {
  setMobileView(state, data) {
    state.onMobile = data
  },

  setTranslatable(state, data) {
    state.isTranslatable = data
  },

  [SET_HIGHLIGHT_TEXT](state, data) {
    state.highlightText = data
  },
  [SET_ACCOUNT_INFO](state, data) {
    state.accountInfo.email = data.email
    state.accountInfo.phone = data.phone
    state.accountInfo.firstName = data.firstName
    state.accountInfo.lastName = data.lastName
  },
  [SET_PROFILE](state, data) {
    state.profile = data
  },
  [TOGGLE_SHOW_GROUPS](state, data) {
    state.groupsDropdownVisible = data
  },
  [SET_HEADER_CLASS_NAME](state, data) {
    state.headerClass = data
  },
  [SET_APPS](state, data) {
    state.appsList = data
  },
  [SET_APPS_LOAD](state, status) {
    state.appsListLoaded = status
  },
  [SET_APPINFO_LOAD](state, status) {
    state.appInfoLoaded = status
  },
  [SET_APP_GROUP](state, id) {
    state.lastActiveTab = id
  },
  [SET_APPINFO](state, info) {
    if (!state.appInfoLoaded) state.appInfo = info
  },
  [SET_DARKMODE](state, status) {
    state.darkMode = status
    localStorage.setItem('dark-mode', status)
    let activeTheme = status ? 'dark' : 'light'
    document.querySelector('html').setAttribute('data-theme', activeTheme)
  },

  [SET_DARKMODE_DETAIL](state, status) {
    state.darkMode = status
  },

  [SET_OVERLAY](state, status) {
    state.showOverlay = status
  },
  [SET_OVERLAY_ALT](state, status) {
    state.showOverlayAlt = status
  },
  [SET_HEADER](state, status) {
    state.showHeader = status
  },
  [SET_PRELOADER](state, status) {
    state.showPreloader = status
  },
  [SET_ACCOUNT_DROPDOWN](state, status) {
    state.showAccountDropdown = status
  },
  [SET_WORDY_RESULTS](state, status) {
    state.showWordyResult = status
  },
  [SET_KEYPRESS](state, status) {
    state.showKeypress = status
  },
  [SET_APPS_VISIBLE](state, status) {
    state.showApps = status
  },
  [SET_NOTIFICATIONS_VISIBLE](state, status) {
    state.showNotifications = status
  },
  [SET_TOOLBAR_VISIBLE](state, status) {
    state.showToolbar = status
  },
  [SET_QUESTIONS_TOOLBAR](state, status) {
    state.showQuestionsToolbar = status
  },
  [SET_PAGE_CLASS](state, status) {
    state.pageClass = status
  },
  [SET_DELETE_MODAL](state, status) {
    state.deleteModal = status
  },
  [SET_RESPONSE_MESSAGE](state, data) {
    state.responseMessage = data
  },
  [SET_PROFILE_LOADED](state, data) {
    state.profileLoaded = data
  },
  [TOGGLE_FEEDBACK_MODAL](state, data) {
    state.feedbackModalVisible = data
  },
  [TOGGLE_COMMUNITY_MODAL](state, data) {
    state.communityModalVisible = data
  },

  [SET_COMMUNITIES](state, data) {
    state.communities = data
  },

  [SET_COMMUNITIES_LOADED](state, data) {
    state.communitiesLoaded = data
  },
  [SET_LOGIN_STATUS](state, data) {
    state.isAuthorized = data
  },
  [SET_COUNTRY_CODES](state, data) {
    state.countryCodeList = data
  },

  [HANDLE_SET_PREVIOUS_URL](state, data) {
    state.previousUrl = data
  },

  [SET_SYSTEM_AVAILABILITY](state, data) {
    data ? (state.offlineCount = 0) : state.offlineCount++
  },

  [SET_WORDBOX_MODAL_VISIBILITY](state, data) {
    state.wordboxModalVisible = data
  },

  [SET_OTHER_WORDS](state, data) {
    state.otherWords = data
  },

  [SET_CLUE_MODAL](state, data) {
    state.clueModal = data
  },

  [SHOW_CLUE_MODAL](state, data) {
    state.clueModal.isActive = data
  },

  [SET_STATUS_CODE](state, data) {
    state.responseStatusCode = data
  },
}

const actions = {
  [HANDLE_STATUS_CODE]({ commit }, code) {
    commit(SET_STATUS_CODE, code)
  },

  async checkTranslatable({ commit }, payload) {
    console.log('pl: ', payload)
    console.log('check translatable status')
    const { data } = await checkTranslatableStatus(payload)
    commit('setTranslatable', data.data.allow)
    commit(SET_HIGHLIGHT_TEXT, {
      text: '',
      status: data.data.allow,
    })
  },

  [CLUE_MODAL_VISIBLE]({ commit }, data) {
    commit(SHOW_CLUE_MODAL, data)
  },

  [HANDLE_CLUE_MODAL]({ commit }, data) {
    commit(SET_CLUE_MODAL, data)
  },

  [HANDLE_OTHER_WORDS]({ commit }, data) {
    commit(SET_OTHER_WORDS, data)
  },

  [SHOW_WORDBOX_MODAL]({ commit }, data) {
    commit(SET_WORDBOX_MODAL_VISIBILITY, data)
  },

  [SYSTEM_AVAILABILITY]({ commit }, data) {
    commit(SET_SYSTEM_AVAILABILITY, data)
  },

  [SET_PREVIOUS_URL]({ commit }, data) {
    commit(HANDLE_SET_PREVIOUS_URL, data)
  },

  [HANDLE_LOGIN_STATUS]({ commit }, data) {
    commit(SET_LOGIN_STATUS, data)
  },

  async [COMMUNITIES_SEND]({ commit }, data) {
    return shareCommunity(data)
  },

  [GET_COMMUNITIES]({ commit }) {
    commit(SET_COMMUNITIES_LOADED, false)
    getCommunities().then(res => {
      commit(SET_COMMUNITIES_LOADED, true)
      console.log('comm res', res)
      commit(SET_COMMUNITIES, res.data.data)
    })
  },

  [SHOW_COMMUNITY_MODAL]({ commit }, data) {
    commit(TOGGLE_COMMUNITY_MODAL, data)
  },

  [SHOW_FEEDBACK_MODAL]({ commit }, data) {
    commit(TOGGLE_FEEDBACK_MODAL, data)
  },

  [UPDATE_ACCOUNT_INFO]({ commit }, payload) {
    updateProfileData(payload)
      .then(res => {
        commit(SET_RESPONSE_MESSAGE, res.data.message)
        commit(SET_ACCOUNT_INFO, payload)
      })
      .catch(e => {
        commit(SET_RESPONSE_MESSAGE, e)
      })
  },
  async [GET_PROFILE]({ commit }) {
    if (!state.profileLoaded) {
      const result = await getUserProfile()
      const memberData = {
        email: result.data.email,
        phone: result.data.phone,
        firstName: result.data.firstName,
        lastName: result.data.lastName,
      }
      commit(SET_PROFILE, result.data)
      commit(SET_PROFILE_LOADED, true)
      commit(SET_ACCOUNT_INFO, memberData)

      return memberData
    }
  },
  [SHOW_GROUPS]({ commit }, data) {
    commit(TOGGLE_SHOW_GROUPS, data)
  },
  [SET_HEADER_CLASS]({ commit }, data) {
    commit(SET_HEADER_CLASS_NAME, data)
  },
  [GET_APPS]({ commit }) {
    getPlatformApps().then(res => {
      commit(SET_APPS, res.data)
      commit(SET_APPS_LOAD, true)
    })
  },
  [SWITCH_DARKMODE]({ commit }, status) {
    commit(SET_DARKMODE, status)
  },
  [SHOW_OVERLAY]({ commit }, status) {
    commit(SET_OVERLAY, status)
  },
  [SHOW_OVERLAY_ALT]({ commit }, status) {
    commit(SET_OVERLAY_ALT, status)
  },
  [SHOW_HEADER]({ commit }, status) {
    commit(SET_HEADER, status)
  },
  [SHOW_PRELOADER]({ commit }, status) {
    commit(SET_PRELOADER, status)
  },
  [SHOW_ACCOUNT_DROPDOWN]({ commit }, status) {
    commit(SET_ACCOUNT_DROPDOWN, status)
  },
  [SHOW_WORDY_RESULTS]({ commit }, status) {
    commit(SET_WORDY_RESULTS, status)
  },
  [SHOW_KEYPRESS]({ commit }, status) {
    commit(SET_KEYPRESS, status)
  },
  [SHOW_APPS]({ commit }, status) {
    commit(SET_APPS_VISIBLE, status)
  },
  [SHOW_NOTIFICATIONS]({ commit }, status) {
    commit(SET_NOTIFICATIONS_VISIBLE, status)
  },
  [SHOW_TOOLBAR]({ commit }, status) {
    commit(SET_TOOLBAR_VISIBLE, status)
  },
  [SHOW_QUESTIONS_TOOLBAR]({ commit }, status) {
    commit(SET_QUESTIONS_TOOLBAR, status)
  },
  [GET_PAGE_CLASS]({ commit }, status) {
    commit(SET_PAGE_CLASS, status)
  },
  [DELETE_MODAL]({ commit }, status) {
    commit(SET_DELETE_MODAL, status)
  },
  [GET_COUNTRY_CODES]({ commit }) {
    if (state.countryCodeList.length === 0) {
      getCountries().then(res => {
        commit(SET_COUNTRY_CODES, res.data.data)
      })
    }
  },
}

const getters = {
  systemUnavailable: state => state.offlineCount > 2,
  highlightText: state => state.highlightText,
  communities: state => state.communities,
  communitiesLoaded: state => state.communitiesLoaded,
  communityModalVisible: state => state.communityModalVisible,
  feedbackModalVisible: state => state.feedbackModalVisible,
  profileData: state => state.profile,
  profileDataLoaded: state => state.profileLoaded,
  responseMessage: state => state.responseMessage,
  memberInfo: state => state.accountInfo,
  showDropdown: state => state.groupsDropdownVisible,
  headerClass: state => state.headerClass,
  modalOpen: state => state.deleteModal,
  lastTab: state => state.lastActiveTab,
  // userApps: state => state.appsList,
  appsLoaded: state => state.appsListLoaded,
  appLoaded: state => state.appInfoLoaded,
  appHeader: state => state.appInfo,
  appGroups: state => state.appInfo.groups,
  initialTab: state => state.appInfo.groups.find(el => el.id === -1),
  appHeaderInfo: state => app =>
    state.appInfoDetail.find(el => el.name === app),
  getDarkMode: state => state.darkMode,
  showOverlay: state => state.showOverlay,
  showOverlayAlt: state => state.showOverlayAlt,
  showPreloader: state => state.showPreloader,
  showHeader: state => state.showHeader,
  showAccountDropdown: state => state.showAccountDropdown,
  showWordyResult: state => state.showWordyResult,
  showKeypress: state => state.showKeypress,
  showApps: state => state.showApps,
  showNotifications: state => state.showNotifications,
  showToolbar: state => state.showToolbar,
  showQuestionsToolbar: state => state.showQuestionsToolbar,
  pageClass: state => state.pageClass,
  isAuthorized: state => state.isAuthorized,
  countryCodes: state => state.countryCodeList,
  previousUrl: state => state.previousUrl,
  wordboxModalVisible: state => state.wordboxModalVisible,
  otherWords: state => state.otherWords,
  clueModal: state => state.clueModal,
  onMobile: state => state.onMobile,
  isTranslatable: state => state.isTranslatable,
  responseStatusCode: state => state.responseStatusCode
}

export default {
  state,
  mutations,
  actions,
  getters,
}
