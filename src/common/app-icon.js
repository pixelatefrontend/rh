export const categoryIcon = {
  lesson: 'dersler',
  reading: 'okuma',
  resource: 'dokumanlar',
  e_deneme: 'deneme',
  dictionary: 'sozluk',
  dersler: 'dersler',
  clue_bank: 'cluebank',
  test: 'test',
  wordy: 'wordy',
  digibook: 'digibook'
}

export const categoryName = {
  lesson: 'Dersler',
  reading: 'Okumalar',
  resource: 'Dökümanlar',
  e_deneme: 'Denemeler',
  exam: 'Denemeler',
  sozluk: 'Sözlük',
  dictionary: 'Sözlük',
  dersler: 'Dersler',
  clue_bank: 'Clue Bank',
  test: 'Testler',
  wordy: 'Wordy',
  digibook: 'Digibook'
}
