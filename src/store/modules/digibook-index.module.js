import { getDigibookUnitContent } from '../../services/digibook.service'
import Vue from 'vue'

const state = {
  activeList: 'unit',
  activeUnitId: null,
  staticContents: null,
  activeStaticContent: null,
  units: null,
  exercises: null,
  loading: true,
  loaded: false,
  error: null,
  contentListVisible: false,
  completion: 0,
  completionLoading: false,
  selection: [
    {
      type: 'unit',
      id: 0,
      text: 'Seçiniz',
    },
    {
      type: 'exercise',
      id: 0,
      text: 'Seçiniz',
    },
    {
      type: 'content',
      id: 0,
      text: 'Seçiniz',
      content_type: '',
    },
  ],
}

const getters = {
  units: state => state.units,
  exercises: state => state.exercises,
  selection: state => state.selection,
  activeUnitId: state => state.activeUnitId,
  activeList: state => state.activeList,
  staticContents: state => state.staticContents,
  contentListVisible: state => state.contentListVisible,
  activeStaticContent: state => state.activeStaticContent,
  exercisesLoading: state => state.loading,
  exercisesLoaded: state => state.loaded,
  exercisesError: state => state.error,
  completion: state => state.completion,
  completionLoading: state => state.completionLoading,
}

const mutations = {
  showContentList: (state, data) => (state.contentListVisible = data),

  setUnits: (state, data) => (state.units = data),

  setCompletion: (state, data) => (state.completion = data),

  setCompletionLoading: (state, data) => (state.completionLoading = data),

  setActiveList: (state, data) => (state.activeList = data),

  setActiveUnitId: (state, data) => (state.activeUnitId = data),

  setExercisesLoading: (state, data) => (state.loading = data),

  setExercisesLoaded: (state, data) => (state.loaded = data),

  setExercisesError: (state, data) => (state.error = data),

  setExercises: (state, data) => {
    state.exercises = data
    if (state.exercises) {
      state.exercises.forEach(list => {
        list.contents.map(item => {
          Vue.set(item, 'activeContent', false)
        })
      })
    }
  },

  setActiveExercise: (state, data) => {
    let exerciseList = state.exercises.find(el => el.id === data.exerciseId)
    let item = exerciseList.contents.find(el => el.id === data.content.id)
    Vue.set(item, 'activeContent', true)
    console.log('exerciselist: ', exerciseList)
  },

  setStaticContents: (state, data) => (state.staticContents = data),

  setActiveStaticContent: (state, data) => {
    const content = state.staticContents.find(el => el.link === data)
    state.activeStaticContent = content
  },

  setSelection: (state, data) => {
    if (data.length > 0) state.selection = data
    state.selection[0].name = 'ünite'
    state.selection[1].name = 'alıştırma'
    state.selection[2].name = 'içerik'
  },

  setSelectionTitle: (state, data) => {
    if (data.type === null) {
      state.selection.map(el => (el.text = data.text))
    } else {
      let item = state.selection.find(el => el.type === data.type)
      item.text = data.text
    }
  },

  clearSelection: state => {
    state.selection = [
      {
        type: 'unit',
        id: 0,
        text: 'Seçiniz',
      },
      {
        type: 'exercise',
        id: 0,
        text: 'Seçiniz',
      },
      {
        type: 'content',
        id: 0,
        text: 'Seçiniz',
        content_type: '',
      },
    ]
  },

  clearExercises: state => (state.exercises = null),
}

const actions = {
  getExercisesList: async ({ state, commit }, id) => {
    // Handling content tab selection here
    commit('setActiveList', 'exercise')

    if (state.activeUnitId === id) return true
    // commit('clearExercises')
    commit('setExercisesLoading', true)
    commit('setExercisesLoaded', false)
    try {
      const response = await getDigibookUnitContent(id)
      commit('setExercises', response.data)
      commit('setActiveUnitId', id)
    } catch (error) {
      commit('setExercisesError', error.response)
    } finally {
      commit('setExercisesLoading', false)
      commit('setExercisesLoaded', true)
    }
  },

  handleActiveExercise: ({ commit }, data) => {
    commit('setActiveExercise', data)
  },

  handleActiveList: ({ commit }, data) => {
    commit('setActiveList', data)
  },

  handleActiveStaticContent: ({ commit }, data) => {
    commit('setActiveStaticContent', data)
  },

  handleContentListVisibility: ({ commit }, status) => {
    commit('showContentList', status)
  },

  handleSelectionTitle: ({ commit }, data) => {
    commit('setSelectionTitle', data)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
}
