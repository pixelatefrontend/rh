import store from '../store'

let worker = null

const runWorker = () => {
  stopWorker()
  worker = setInterval(() => {
    store.dispatch('denemeDetail/setDurationDetail')
  }, 1000)
}

const stopWorker = () => {
  clearInterval(worker)
}

export default { runWorker, stopWorker }
