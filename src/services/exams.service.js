import Vue from 'vue'

export const exams = page => Vue.$axios('GET', `/exams?page=${page}`)

export const filter = (page, group, status, type) =>
  Vue.$axios('GET', `/exams?_group=${group}&_status=${status}&_type=${type}&_page=${page}`)

export const getExamsInfo = () => Vue.$axios('GET', `/exams/info`)

export const groups = (group, page) => Vue.$axios('GET', `/exams?group=${group}&_page=${page}`)

export const status = (group, status) => Vue.$axios('GET', `/exams?_status=${status}&group=${group}`)

export const type = type => Vue.$axios('GET', `/exams?typeId=${type}`)

export const exam = id => Vue.$axios('GET', `/exams/${id}`)
