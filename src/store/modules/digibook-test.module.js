import { notifier } from '../../common/helpers'
import {
  getDigibookItemToolData,
  saveTestQuestionOption,
  saveTestUserTranslation,
  showQuestionAnswer,
} from '../../services/digibook.service'

import Vue from 'vue'
import {
  clearContent,
  getContent,
  setActiveTool,
} from '../helpers/digibook-actions.helper'

const questionItem = (state, blockId, questionId, contentType = 'question') => {
  const questionBlock = state.questionBlocks.find(qb => {
    return qb.id === blockId
  })

  const question = questionBlock.questions.find(q => {
    return q.id === questionId
  })

  if (contentType === 'questionContent') return questionBlock

  return question
}

const state = {
  contentId: null,
  questionBlocks: [],
  tools: null,
  userTranslatable: false,
  progress: 0,
  loading: false,
  loaded: false,
}

const getters = {
  contentId: state => state.contentId,
  loading: state => state.loading,
  loaded: state => state.loaded,
  questionBlocks: state => state.questionBlocks,
  hasLoaded: state => !state.loading && state.loaded,
  userTranslatable: state => state.userTranslatable,
}

const mutations = {
  setToolData: (state, data) => {
    let question = questionItem(
      state,
      data.questionBlockId,
      data.questionId,
      data.itemType
    )
    Vue.set(question, 'toolData', data.result)
  },

  clearActiveTool: (state, data) => {
    let question = questionItem(state, data.questionBlockId, data.questionId)
    Vue.set(question, 'activeTool', '')
  },

  setActiveTool: (state, data) => {
    let question = questionItem(
      state,
      data.questionBlockId,
      data.questionId,
      data.itemType
    )

    const currentTool = question.activeTool
    question.activeTool = currentTool === data.tool ? '' : data.tool

    let tools = question.activeTools
    let isActive = tools.includes(data.tool)

    if (data.tool === 'question' || data.tool === '') return

    if (!isActive) tools.push(data.tool)
    else tools = tools.filter(el => el !== data.tool)

    Vue.set(question, 'activeTools', tools)
  },

  setToolDataLoading: (state, data) => {
    let question = questionItem(
      state,
      data.questionBlockId,
      data.questionId,
      data.itemType
    )
    Vue.set(question, 'toolLoading', data.status)
  },

  setToolDataLoaded: (state, data) => {
    let question = questionItem(
      state,
      data.questionBlockId,
      data.questionId,
      data.itemType
    )
    Vue.set(question, 'toolLoaded', data.status)
  },

  setData: (state, payload) => {
    let number = 1
    state.tools = payload.tools
    state.progress = payload.progress
    state.userTranslatable = payload.userTranslatable
    payload.data.forEach(q => {
      q.questions.map(el => {
        Vue.set(el, 'toolData', null)
        Vue.set(el, 'toolLoading', null)
        Vue.set(el, 'toolLoaded', null)
        Vue.set(el, 'activeTool', '')
        Vue.set(el, 'activeTools', [])
        Vue.set(el, 'number', number)
        number++
      })
      Vue.set(q, 'toolData', null)
      Vue.set(q, 'toolLoading', null)
      Vue.set(q, 'toolLoaded', null)
      Vue.set(q, 'activeTool', '')
      Vue.set(q, 'activeTools', [])
      state.questionBlocks.push(q)
    })
  },

  clearData: state => {
    state.contentId = null
    state.progress = 0
    state.tools = null
    state.questionBlocks.length = 0
    state.userTranslatable = false
  },

  setContentId: (state, data) => (state.contentId = data),

  setLoading: (state, data) => (state.loading = data),

  setLoaded: (state, data) => (state.loaded = data),

  setUserTranslation: (state, data) => {
    let question = questionItem(state, data.questionBlockId, data.questionId)
    question.questionTranslation = data.body
  },

  setOption: (state, data) => {
    let question = questionItem(state, data.questionBlockId, data.questionId)
    question.answerId = data.optionId
  },

  showQuestionAnswer: (state, data) => {
    let question = questionItem(state, data.questionBlockId, data.questionId)
    const trueOption = question.options.find(el => el.correctOption === true)
    question.isLocked = true
    if (question.answerId === null) return
    question.answerStatus = question.answerId === trueOption.id
  },

  setNewQuestion: (state, data) => {
    let questionObj = questionItem(state, data.questionBlockId, data.questionId)
    let myQuestions = questionObj.toolData.myQuestions
    myQuestions.push(data.question)
    Vue.set(questionObj.toolData, 'myQuestions', myQuestions)
  },

  editQuestion: (state, data) => {
    let questionObj = questionItem(state, data.questionBlockId, data.questionId)
    let myQuestions = questionObj.toolData.myQuestions
    let editedQuestion = myQuestions.find(el => el.id === data.question.id)
    editedQuestion.question = data.question.question
    Vue.set(questionObj.toolData, 'myQuestions', myQuestions)
  },

  removeQuestion: (state, data) => {
    let questionObj = questionItem(state, data.questionBlockId, data.questionId)
    let myQuestions = questionObj.toolData.myQuestions
    myQuestions = myQuestions.filter(el => el.id !== data.askingId)
    Vue.set(questionObj.toolData, 'myQuestions', myQuestions)
  },
}

const actions = {
  fetchContent: getContent,

  handleClearData: clearContent,

  handleActiveTool: setActiveTool,

  fetchToolData: async ({ state, commit, dispatch }, data) => {
    let question = questionItem(
      state,
      data.questionBlockId,
      data.questionId,
      data.itemType
    )

    if (data.tool === 'answer') {
      return dispatch('handleShowQuestionAnswer', data)
    }

    if (question.toolLoaded) {
      return dispatch('handleActiveTool', data)
    }

    commit('setToolDataLoading', { ...data, status: true })
    const response = await getDigibookItemToolData(data)
    console.log('response: ', response)
    if (response.status !== 200) {
      commit('setToolDataLoading', { ...data, status: false })
      return console.error(response)
    }

    dispatch('handleActiveTool', data)
    commit('setToolData', { ...data, result: response.data })
    commit('setToolDataLoading', { ...data, status: false })
    commit('setToolDataLoaded', { ...data, status: true })
  },

  handleOption: async ({ commit }, data) => {
    console.log(data)
    try {
      const response = await saveTestQuestionOption(data)
      commit('setOption', data)
      notifier.success(response.data.message)
    } catch (error) {
      console.error(error)
      notifier.error('Cevap kaydetme başarısız!')
    }
  },

  handleUserTranslation: async ({ state, commit }, data) => {
    const question = questionItem(state, data.questionBlockId, data.questionId)
    if (question.questionTranslation === data.body) return false

    try {
      const response = await saveTestUserTranslation(data)
      notifier.success(response.data.message)
      commit('setUserTranslation', data)
    } catch (error) {
      console.error(error)
      notifier.error('Cevap kaydetme başarısız!')
    }
  },

  handleShowQuestionAnswer: async ({ commit }, data) => {
    console.log('data: ', data)
    try {
      const { data: result } = await showQuestionAnswer(data)
      notifier.success(result.message)
      commit('showQuestionAnswer', data)
    } catch (error) {
      console.error(error)
      notifier.error('Cevap görüntüleme başarısız!')
    }
  },

  handleNewQuestion: ({ commit }, data) => {
    commit('setNewQuestion', data)
  },

  handleEditQuestion: ({ commit }, data) => {
    commit('editQuestion', data)
  },

  handleRemoveQuestion: ({ commit }, data) => {
    commit('removeQuestion', data)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
}
