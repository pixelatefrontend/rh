import { getDigibookContentNew } from '../../services/digibook.service'

const getContent = async ({ state, commit }, id) => {
  if (id === state.contentId) return
  commit('clearData')
  commit('setLoading', true)
  commit('digibook/index/setCompletionLoading', true, { root: true })
  const { status, data } = await getDigibookContentNew(id)
  console.log('Digibook Content: \n', data)
  commit('setLoading', false)
  if (status !== 200) return
  if (data.tools) {
    commit('digibook/tools/setTools', data.tools, { root: true })
  }
  commit('digibook/index/setCompletion', data.progress, { root: true })
  commit('digibook/index/setCompletionLoading', false, { root: true })
  commit('digibook/index/setExercises', data.exercises, { root: true })
  commit('digibook/index/setSelection', data.selection, { root: true })
  commit('digibook/setContentTitle', data.title, { root: true })
  commit('setData', data)
  commit('setContentId', id)
  commit('setLoaded', true)
}

const clearContent = ({ commit }) => {
  commit('clearData')
}

const setActiveTool = ({ commit }, data) => {
  commit('setActiveTool', data)
}

export { getContent, clearContent, setActiveTool }
