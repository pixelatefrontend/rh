import { getDigibook } from '../../services/digibook.service'
import { digibookContentRouter } from '../../common/helpers'
import index from './digibook-index.module'
import statics from './digibook-static.module'
import translation from './digibook-translation.module'
import table from './digibook-table.module'
import test from './digibook-test.module'
import matching from './digibook-matching.module'
import reading from './digibook-reading.module'
import wordlist from './digibook-wordlist.module'
import tools from './digibook-tools.module'

const state = {
  digibook: null,
  loading: true,
  loaded: false,
  error: null,
  lastContentUrl: '',
  activeDigibookId: null,
  contentTitle: '',
}

const getters = {
  digibook: state => state.digibook,
  loading: state => state.loading,
  loaded: state => state.loaded,
  error: state => state.error,
  lastContentUrl: state => state.lastContentUrl,
  activeDigibookId: state => state.activeDigibookId,
  contentTitle: state => state.contentTitle,
  bookName: state => state.digibook.title,
  bookDesc: state => state.digibook.text,
  isDownloadable: state => state.digibook.is_downloadable,
  downloadLink: state => state.digibook.download,
  buyLink: state => state.digibook.buy_link,
  physicalProductId: state => state.digibook.physical_product_id,
}

const mutations = {
  setDigibook: (state, data) => (state.digibook = data),

  setLoading: (state, data) => (state.loading = data),

  setLoaded: (state, data) => (state.loaded = data),

  setError: (state, data) => (state.error = data),

  setActiveDigibookId: (state, data) => (state.activeDigibookId = data),

  setLastContentUrl: (state, data) => {
    const path = location.pathname
    const hash = location.hash
    let url = `/digibook/${state.digibook.id}`

    if (url !== path) {
      return (state.lastContentUrl = path + hash)
    }

    url = data.hasPrologue ? (url += '/onsoz') : url

    if (data.history.length > 0) {
      const fields = {
        digibookId: state.digibook.id,
        unitId: data.history[0].id,
        exerciseId: data.history[1].id,
        contentType: data.history[2].content_type,
        contentId: data.history[2].id,
      }
      console.log('hash: ', hash)
      url = digibookContentRouter(fields, hash)
    }

    state.lastContentUrl = url
  },

  setContentTitle: (state, title) => (state.contentTitle = title),

  clearDigibook: state => {
    state.digibook = null
    state.loading = true
    state.loaded = false
  },
}

const actions = {
  getDigibook: async ({ dispatch, commit }, id) => {
    dispatch('handleClearDigibook')
    commit('setActiveDigibookId', id)
    commit('setLoading', true)
    commit('setLoaded', false)
    document.title = 'Yükleniyor... | Digibook | RH+ Platform'
    try {
      const response = await getDigibook(id)
      document.title = `${response.data.title} | Digibook | RH+ Platform`
      commit('statics/setContentsList', response.data.static_contents)
      commit('index/setStaticContents', response.data.static_contents)
      commit('setDigibook', response.data)
      commit('setLastContentUrl', {
        history: response.data.selection,
        hasPrologue: response.data.static_contents !== null,
      })
      commit('index/setUnits', response.data.units)
      commit('index/setSelection', response.data.selection)
      commit('index/setExercises', response.data.exercises)
      commit('index/setExercisesLoading', false)
      commit('index/setExercisesLoaded', true)

      if (response.data.exercises) {
        dispatch('index/handleActiveExercise', {
          exerciseId: response.data.selection[1].id,
          content: {
            id: response.data.selection[2].id,
          },
        })
      }

      let currentUnit = response.data.units.find(el => el.currentUnit === true)

      if (currentUnit) {
        commit('index/setActiveUnitId', currentUnit.id)
      }
    } catch (error) {
      commit('setError', error)
      console.log('digibook module error: ', error)
      document.title = `Hata | Digibook | RH+ Platform`
    } finally {
      commit('setLoading', false)
      commit('setLoaded', true)
    }
  },

  handleClearDigibook: ({ commit }) => {
    commit('clearDigibook')
    commit('index/clearSelection')
    commit('statics/setContentsList', null)
    commit('index/setActiveList', 'unit')
  },

  handleContentTitle: ({ commit }, data) => {
    commit('setContentTitle', data)
  },

  handleActiveStatus: ({ commit }, status) => {
    commit('setActiveStatus', status)
  },
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
  modules: {
    index,
    statics,
    translation,
    table,
    test,
    matching,
    reading,
    wordlist,
    tools,
  },
}
