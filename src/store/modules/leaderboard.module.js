import Vue from 'vue'

const getters = {
  leaderboard: state => state.leaderboard,
  loading: state => state.loading,
  loaded: state => state.loaded,
  filters: state => {
    let filters = JSON.parse(JSON.stringify(state.leaderboard.filter))
    filters.groups.options = filters.groups.options.map(
      ({ value, label, media, selected = false }) => ({
        value,
        label,
        selected,
        icon: media,
      }),
    )
    filters.apps.options = filters.apps.options.map(
      ({ value, label, media, selected = false }) => ({
        value,
        label,
        selected,
        icon: media,
      }),
    )
    filters.date.options = filters.date.options.map(
      ({ value, label, media, selected = false }) => ({
        selected,
        title: label,
        id: value,
        icon: media,
      }),
    )
    return filters
  },
  filter: state => state.filter
}

const actions = {
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
  getLeaderboard: ({ commit, state }) => {
    commit('SET_LOADING', true)

    const params =
      state.filter.groups !== null || state.filter.groups !== null || state.filter.groups !== null
        ? `?groups=${state.filter.groups}&apps=${state.filter.apps}&date=${state.filter.date}`
        : ''

    Vue.axios
      .get(`leadership${params}`)
      .then(response => {
        commit('SET_LEADERBOARD', response.data)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        commit('SET_FILTER', {
          groups: response.data.filter.groups.value,
          apps: response.data.filter.apps.value,
          date: response.data.filter.date.value,
        })
      })
      .catch((e) => {
        console.log('catch', e)
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  setFilter: ({ commit, dispatch }, filter) => {
    commit('SET_FILTER', filter)
    dispatch('getLeaderboard')
  }
}

const mutations = {
  SET_LEADERBOARD(state, leaderboard) {
    const $state = state
    $state.leaderboard = leaderboard
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  RESET_STATE(state) {
    const $state = state
    $state.leaderboard = null
    $state.loading = false
    $state.loaded = false
    $state.filter = {
      groups: null,
      apps: null,
      date: null,
    }
  },
  SET_FILTER(state, filter) {
    const $state = state
    $state.filter = filter
  }
}

const state = {
  leaderboard: null,
  loading: false,
  loaded: false,
  filter: {
    groups: null,
    apps: null,
    date: null,
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
