import WordbookIndex from '../views/Wordbook/index'
export default [
  {
    path: '/kelime-defteri',
    name: 'WordBook',
    component: WordbookIndex,
    children: [
      {
        path: ':listId',
        name: 'WordBook',
        component: WordbookIndex,
        meta: {
          title: 'Kelime Defteri',
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          tourName: 'wordbookTour',
        },
        children: [
          {
            path: ':page',
            name: 'WordBookPage',
            component: WordbookIndex,
            meta: {
              title: 'Kelime Defteri',
              requiresAuth: true,
              headerShow: false,
              toolbarVisible: true
            }
          },
        ]
      },
    ],
  },
]
