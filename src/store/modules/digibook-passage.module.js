import {
  getDigibookContent,
  getDigibookItemData,
  saveAnswerParagraph
} from '../../services/digibook.service'

const state = {
  questions: null,
  paragraphId: null,
  progress: 0,
  texts: null,
  tools: null,
  loading: true,
  loaded: false,
  passage: null,
  passageLoading: null,
  passageLoaded: null
}

const getters = {
  questions: state => state.questions,
  paragraphId: state => state.paragraphId,
  progress: state => state.progress,
  texts: state => state.texts,
  tools: state => state.tools,
  loading: state => state.loading,
  loaded: state => state.loaded,
  passage: state => state.passage,
  passageLoading: state => state.passageLoading,
  passageLoaded: state => state.passageLoaded
}

const mutations = {
  setQuestions: (state, data) => (state.questions = data),

  setParagraphId: (state, data) => (state.paragraphId = data),

  setProgress: (state, data) => (state.progress = data),

  setTexts: (state, data) => (state.texts = data),

  setTools: (state, data) => (state.tools = data),

  setLoading: (state, data) => (state.loading = data),

  setLoaded: (state, data) => (state.loaded = data),

  setPassage: (state, data) => (state.passage = data),

  setPassageLoading: (state, data) => (state.passageLoading = data),

  setPassageLoaded: (state, data) => (state.passageLoaded = data),

  setAnswerData: (state, data) => {
    let question = state.questions.find(el => el.id === data.questionId)
    question.answer_id = data.answerId
  },

  setShowAnswer: (state, data) => {
    let question = state.questions.find(el => el.id === data.questionId)
    let option = question.options.find(el => el.id === data.optionId)

    if (option) question.answer_type = option.correctOption ? 'correct' : 'wrong'
    question.is_locked = true
  }
}

const actions = {
  getContent: async ({ state, commit }, id) => {
    if (state.paragraphId !== id) {
      commit('setLoading', true)
      commit('setLoaded', false)
      try {
        const response = await getDigibookContent(id)
        console.log('response: ', response)
        commit('setParagraphId', response.data.data.paragraph_id)
        commit('setQuestions', response.data.data.tests)
        commit('setTexts', response.data.data.texts)
        commit('setProgress', response.data.progress)
        commit('setTools', response.data.tools)
      } catch (error) {
        console.error('error: ', error)
      } finally {
        commit('setLoading', false)
        commit('setLoaded', true)
      }
    }
  },

  saveAnswer: async ({ commit }, payload) => {
    commit('setAnswerData', payload)
    try {
      const response = await saveAnswerParagraph(payload)
      return response.data
    } catch (error) {
      return error.response.data
    }
  },

  getPassageUnitContent: async ({ state, commit }, payload) => {
    if (!state.passage) {
      commit('setPassageLoading', true)
      commit('setPassageLoaded', false)
      try {
        const response = await getDigibookItemData(payload)
        commit('setPassage', response.data)
      } catch (error) {
        console.error(error)
      } finally {
        commit('setPassageLoading', false)
        commit('setPassageLoaded', true)
      }
    }
  },

  showAnswer: ({ commit }, payload) => {
    console.log('pl: ', payload)
    commit('setShowAnswer', payload)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
