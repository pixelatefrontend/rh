import Vue from 'vue'

const getters = {
  report: state => state.report,
  reportName: state => state.report.name,
  loading: state => state.loading,
  loaded: state => state.loaded,
  questionList: state => state.report.annotated_answered_key.questionList,
  selectedQuestionsIds: state => state.selectedQuestionsIds,
  selectedQuestions: state =>
    state.report.annotated_answered_key.questionList.filter(o =>
      state.selectedQuestionsIds.includes(o.id),
    ),
  questionState: state => state.questionState,
  filteredQuestions: state =>
    state.questionState === 'all'
      ? state.report.annotated_answered_key.questionList
      : state.report.annotated_answered_key.questionList.filter(
        o => o.state === state.questionState,
      ),
  statusButtons: state => {
    const $list = state.report.annotated_answered_key.questionList
    const $buttons = [
      {
        id: 0,
        title: `Tümü ${$list.length}`,
        status: 'all',
      },
      {
        id: 1,
        title: `Doğrular ${$list.filter(o => o.state === 'correct').length}`,
        status: 'correct',
      },
      {
        id: 2,
        title: `Yanlışlar ${$list.filter(o => o.state === 'wrong').length}`,
        status: 'wrong',
      },
      {
        id: 3,
        title: `Boşlar ${$list.filter(o => o.state === 'nonchecked').length}`,
        status: 'nonchecked',
      },
    ]
    return $buttons
  },
}

const actions = {
  /**
   * get report data from backend server
   * @param {id:number} params
   */
  getReport: ({ commit, dispatch }, params) => {
    commit('RESET_STATE')
    commit('SET_LOADING', true)
    Vue.axios
      .get(`https://www.remzihoca.com/platform_v2/exams/report/${params.id}`)
      .then(response => {
        commit('SET_REPORT', response.data)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        dispatch(
          'triggerSelectedQuestionId',
          response.data.annotated_answered_key.questionList[0].id,
        )
      })
      .catch(() => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  triggerSelectedQuestionId: ({ commit, state }, id) => {
    // REMOVE MULTIPLE QUESTION SELECTION SUPPORT
    // const $selecteds = JSON.parse(JSON.stringify(state.selectedQuestionsIds))
    // if ($selecteds.includes(id)) {
    //   const $index = $selecteds.indexOf(id)
    //   delete $selecteds[$index]
    // } else {
    //   $selecteds.push(id)
    // }
    const $selecteds = [id]
    commit('SET_SELECTED_QUESTIONS_IDS', $selecteds)
  },
  setQuestionState: ({ commit }, status) => {
    commit('SET_SELECTED_QUESTIONS_IDS', [])
    commit('SET_QUESTION_STATE', status)
  },
}

const mutations = {
  SET_REPORT(state, report) {
    const $state = state
    $state.report = report
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_SELECTED_QUESTIONS_IDS(state, selecteds) {
    const $state = state
    $state.selectedQuestionsIds = selecteds
  },
  SET_QUESTION_STATE(state, status) {
    const $state = state
    $state.questionState = status
  },
  RESET_STATE(state) {
    const $state = state
    $state.report = null
    $state.loading = false
    $state.loaded = false
    $state.selectedQuestionsIds = []
    $state.questionState = 'all'
  }
}

const state = {
  report: null,
  loading: false,
  loaded: false,
  selectedQuestionsIds: [],
  questionState: 'all', // availables: all, correct, wrong, skiped
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
