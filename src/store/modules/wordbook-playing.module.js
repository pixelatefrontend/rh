import { gspeakNew } from '@/common/helpers'

const getters = {
  workingMode: state => state.workingMode,
  playingList: state => state.playingList,
  soundsLoading: state => state.soundsLoading,
  playingWord: state => state.playingWord,
  isPlaying: state => state.isPlaying,
  reader: state => state.reader,
  playingCounter: state => state.playingCounter,
}

const actions = {
  gettingReadingList({ commit }) {
    commit('SET_PLAYING_LIST', [])
    commit('SET_PLAYING_WORD', 0)
    commit('SET_PLAYING_COUNTER', 0)
    commit('SET_WORKING_MODE', true)
  },
  /**
   *
   */
  resetState: ({ commit }) => {
    commit('RESET_STATE')
  },
  /**
   *
   * @param {*} param0
   * @param {*} PlayingList
   */
  setPlayingList({ commit }, playingList) {
    commit('SET_PLAYING_LIST', playingList)
  },
  /**
   *
   * @param {*} param0
   * @param {*} soundsLoading
   */
  updateSoundsLoading({ commit }, soundsLoading) {
    commit('SET_SOUNDS_LOADING', soundsLoading)
  },
  /**
   *
   */
  updatePlayingWord({ commit }, playingWord) {
    commit('SET_PLAYING_WORD', playingWord)
  },

  /**
   *
   */
  async playingWordSound({ commit, state, dispatch }, wordIndex) {
    if (wordIndex <= state.playingList.length - 1) {
      commit('SET_IS_PLAYING', true)
      commit('SET_PLAYING_WORD', state.playingList[wordIndex].id)
      // await this.soundList.forEach(wordSound => {
      let wordSound = state.playingList[wordIndex]
      if (!wordSound.read) {
        let wordbinrySound = gspeakNew(wordSound.text, wordSound.lang)
        await dispatch('playWord', wordbinrySound)
      }
      return true
    } else {
      dispatch('stopPlying')
      return false
    }
  },
  /**
   *
   */
  async playWord({ commit, state, dispatch }, soundword) {
    state.reader.src = 'data:audio/mpeg;base64,' + soundword
    state.reader.currentTime = 0
    await state.reader.play()
    state.reader.addEventListener('ended', async () => {
      // console.log('playing:', 'index :', state.playingCounter, ' - Id: ', state.playingList[state.playingCounter].id, '-text :', state.playingList[state.playingCounter].text)
      commit('SET_IS_PLAYING', false)
      commit('UPDATE_WORD_READING', state.playingCounter)
      commit('SET_PLAYING_COUNTER', state.playingCounter + 1)
      commit('STOP_READER')
      // console.log('next Playing index:', state.playingCounter, '/', state.playingList.length)
      if (state.playingCounter <= state.playingList.length - 1) {
        if (state.playingWord === state.playingList[state.playingCounter].id) {
          await dispatch('playingWordSound', state.playingCounter)
          return true
        } else {
          return false
        }
      }
      // } else {
      //   dispatch('stopPlying')
      //   return true
      // }
    })
  },
  stopPlying({ commit }, playSound = true) {
    commit('SET_PLAYING_WORD', 0)
    commit('SET_PLAYING_COUNTER', 0)
    commit('SET_WORKING_MODE', false)
    commit('STOP_READER')
    commit('SET_PLAYING_LIST', [])
    if (playSound) {
      new Audio('/complete.wav').play()
    }
  },
}

const mutations = {
  RESET_STATE(state) {
    const $state = state
    $state.playingList = []
    $state.soundsLoading = false
    $state.playingWord = false
    state.isPlaying = false
  },
  SET_WORKING_MODE(state, workingMode) {
    const $state = state
    $state.workingMode = workingMode
  },
  SET_PLAYING_LIST(state, playingList) {
    const $state = state
    $state.playingList = playingList
  },
  SET_SOUNDS_LOADING(state, soundsLoading) {
    const $state = state
    $state.soundsLoading = soundsLoading
  },
  SET_PLAYING_WORD(state, playingWord) {
    const $state = state
    $state.playingWord = playingWord
  },
  SET_IS_PLAYING(state, isPlaying) {
    const $state = state
    $state.isPlaying = isPlaying
  },
  SET_PLAYING_COUNTER(state, playingCounter) {
    const $state = state
    $state.playingCounter = playingCounter
  },
  STOP_READER(state) {
    const $state = state
    $state.reader.pause()
    $state.reader = new Audio()
  },
  UPDATE_WORD_READING(state, wordIndex) {
    state.playingList[wordIndex].read = true
  },
}

const state = {
  workingMode: false,
  playingList: [],
  soundsLoading: false,
  playingWord: 0,
  reader: new Audio(),
  isPlaying: false,
  playingCounter: 0,
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
