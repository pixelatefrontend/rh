import Vue from 'vue'

const getters = {
  vocabularies: state => state.vocabularies,
  wordSearchResult: state => state.wordSearchResult,
  listDetails: state => state.listDetails,
  listMassegeStatus: state => state.listMassegeStatus,
  listMessageText: state => state.listMessageText,
  loadingEditForm: state => state.loadingEditForm,
  loadingSearch: state => state.loadingSearch,
  addWordModal: state => state.addWordModal,
  loading: state => state.loading,
  loaded: state => state.loaded,
  nextPage: state => state.nextPage,
  wordPage: state => state.wordPage,
  loadMordWord: state => state.loadMordWord,
  loadingMoreList: state => state.loadingMoreList,
  currentTab: state => state.currentTab,
}

const actions = {
  /**
   *
   */
  getMyList: ({ commit }, isSilence = false) => {
    if (!isSilence) {
      commit('SET_LOADING', true)
    }
    Vue.axios
      .get(`/word-lists`)
      .then(response => {
        commit('SET_VOCABULARIES', response.data.data ? response.data.data : [])
        commit('SET_CURRENT_PAGE', response.data.next_page)
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  /**
   *
   */
  loadMoreList: ({ commit, state }, pageNo) => {
    commit('SET_LOADING_MORE_LIST', true)
    const $state = state
    Vue.axios
      .get(`/word-lists?page=${pageNo}`)
      .then(response => {
        let newlist = response.data.data
        newlist.unshift(...$state.vocabularies)
        // console.log('oldlist', oldlist, newlist)
        commit('SET_VOCABULARIES', newlist)
        commit('SET_CURRENT_PAGE', response.data.next_page)
        commit('SET_LOADING_MORE_LIST', false)
      })
      .catch(e => {
        commit('SET_LOADING_MORE_LIST', false)
      })
  },
  /**
   *
   */
  getMyListDetails: ({ commit }, id) => {
    commit('SET_LOADING_EDITING_FORM', true)
    return Vue.axios.get(`/word-lists/toolbar/${id}`).then(response => {
      commit('SET_LISTDETAILS', response.data)
      commit('SET_WORD_PAGE', response.data.next_page)
      commit('SET_LOADING_EDITING_FORM', false)
    })
  },
  restListDetails({ commit }) {
    commit('SET_LISTDETAILS', {})
  },
  /**
   *
   * @param {*} param0
   * @param {*} listData
   */
  MoreWords({ commit, state }, listData) {
    commit('SET_LOAD_MORE_WORD', true)
    Vue.axios
      .get(`/word-lists/toolbar/${listData.id}?page=${listData.pageNO} `)
      .then(response => {
        let newwords = response.data
        newwords.items.unshift(...state.listDetails.items)
        commit('SET_LISTDETAILS', newwords)
        commit('SET_WORD_PAGE', response.data.next_page)
        commit('SET_LOAD_MORE_WORD', false)
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} listDetails
   */
  newList({ commit }, listDetails) {
    commit('SET_LIST_MESSAGE_STATU', false)
    commit('SET_LOADING_EDITING_FORM', true)
    return Vue.axios
      .post(
        `/word-lists?title=${listDetails.title}&description=${listDetails.description}&is_public=${listDetails.isPublic}`
      )
      .then(response => {
        commit('SET_LISTDETAILS', response.data)
        commit('SET_CURRENT_PAGE', 1)
        commit('SET_LIST_MESSAGE_STATU', response.data.success)
        commit('SET_LIST_MESSAGE_TEXT', response.data.message)
        commit('SET_LOADING_EDITING_FORM', false)
      })
      .catch(e => {
        commit('SET_LIST_MESSAGE_STATU', false)
        commit('SET_LOADING_EDITING_FORM', false)
        commit('SET_LIST_MESSAGE_STATU', false)
        commit('SET_LIST_MESSAGE_TEXT', 'Hata Oluştu ')
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} listDetails
   */
  editList({ commit }, listDetails) {
    commit('SET_LIST_MESSAGE_STATU', false)
    commit('SET_LOADING_EDITING_FORM', true)
    return Vue.axios
      .put(
        `/word-lists/${listDetails.id}?title=${listDetails.title}&description=${listDetails.description}&is_public=${listDetails.isPublic}`
      )
      .then(response => {
        commit('SET_CURRENT_PAGE', 1)
        commit('SET_LIST_MESSAGE_STATU', response.data.success)
        commit('SET_LIST_MESSAGE_TEXT', response.data.message)
        commit('SET_LOADING_EDITING_FORM', false)
      })
      .catch(e => {
        commit('SET_LIST_MESSAGE_STATU', false)
        commit('SET_LOADING_EDITING_FORM', false)
        commit('SET_LIST_MESSAGE_STATU', false)
        commit('SET_LIST_MESSAGE_TEXT', 'Hata Oluştu ')
      })
  },
  /**
   * Delete the Current List Api request.
   */
  deleteList({ commit }, listid) {
    return Vue.axios
      .delete(`/word-lists/${listid}`)
      .then(response => {
        commit('SET_VOCABULARIES', response.data.data ? response.data.data : [])
        commit('SET_CURRENT_PAGE', response.data.next_page)
        commit('SET_LIST_MESSAGE_STATU', true)
        commit('SET_LIST_MESSAGE_TEXT', 'Kelime Listesi Silindi!')
      })
      .catch(e => {
        commit('SET_LIST_MESSAGE_STATU', false)
        commit('SET_LOADING_EDITING_FORM', false)
        commit('SET_LIST_MESSAGE_STATU', false)
        commit('SET_LIST_MESSAGE_TEXT', 'Hata Oluştu ')
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} searchWord
   */
  async searchWord({ commit }, searchWord) {
    commit('SET_LOADING_SEARCH', true)
    return Vue.axios
      .get(`/word-lists/search?q=${searchWord}`)
      .then(response => {
        commit('SET_WORDSEARCHRESULT', response.data)
        commit('SET_LOADING_SEARCH', false)
      })
  },
  /**
   *
   * @param {*} param0
   */
  restSearchResult({ commit }) {
    commit('SET_WORDSEARCHRESULT', {})
    commit('SET_LOADING_SEARCH', false)
  },
  /**
   *
   * @param {*} param
   */
  clearFromMessage({ commit }) {
    commit('SET_LIST_MESSAGE_STATU', false)
    commit('SET_LIST_MESSAGE_TEXT', '')
  },
  /**
   *
   * @param {*} param0
   * @param {*} newWord
   */
  addNewWordToList({ commit, state }, newWord) {
    return Vue.axios
      .post(
        `/add-to-user-word-list?type=${newWord.type}&data[]=${newWord.data}&word_list_id=${newWord.listID}`
      )
      .then(response => {
        commit('SET_LIST_MESSAGE_STATU', response.data.success)
        commit('SET_LIST_MESSAGE_TEXT', response.data.message)
        if (response.data.success) {
          let listData = state.listDetails
          listData.word_count++
          commit('SET_LISTDETAILS', listData)
        }
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} word
   */
  deleteWordFromList({ commit, state }, word) {
    return Vue.axios
      .post(
        `/delete-from-user-word-list?type=${word.type}&data=${word.data}&word_list_id=${word.listID}`
      )
      .then(response => {
        commit('SET_LIST_MESSAGE_STATU', response.data.success)
        commit('SET_LIST_MESSAGE_TEXT', response.data.message)
        if (response.data.success) {
          let listData = state.listDetails
          listData.word_count--
          commit('SET_LISTDETAILS', listData)
        }
      })
  },
  /**
   *
   * @param {*} param0
   * @param {*} addWordModal
   */
  setAddWordModal({ commit }, addWordModal) {
    commit('SET_ADD_WORD_MODAL', addWordModal)
  },
  setCurrentTab({ commit }, currentTab) {
    commit('SET_CURRENT_TAB', currentTab)
  },
}
const mutations = {
  SET_VOCABULARIES(state, vocabularies) {
    const $state = state
    $state.vocabularies = vocabularies
  },
  SET_WORDSEARCHRESULT(state, wordSearchResult) {
    const $state = state
    $state.wordSearchResult = wordSearchResult
  },
  SET_LISTDETAILS(state, listDetails) {
    const $state = state
    $state.listDetails = listDetails
  },
  SET_LIST_MESSAGE_STATU(state, listMassegeStatus) {
    const $state = state
    $state.listMassegeStatus = listMassegeStatus
  },
  SET_LIST_MESSAGE_TEXT(state, listMessageText) {
    const $state = state
    $state.listMessageText = listMessageText
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  },
  SET_LOADING_EDITING_FORM(state, status) {
    const $state = state
    $state.loadingEditForm = status
  },
  SET_ADD_WORD_MODAL(state, addWordModal) {
    const $state = state
    $state.addWordModal = addWordModal
  },
  SET_CURRENT_PAGE(state, nextPage) {
    const $state = state
    if (nextPage) {
      $state.nextPage = $state.nextPage + 1
    } else {
      $state.nextPage = false
    }
  },
  SET_LOADING_SEARCH(state, loadingSearch) {
    const $state = state
    $state.loadingSearch = loadingSearch
  },
  SET_CURRENT_TAB(state, currentTab) {
    const $state = state
    $state.currentTab = currentTab
  },
  SET_WORD_PAGE(state, wordPage) {
    const $state = state
    if (wordPage) {
      $state.wordPage = parseInt($state.listDetails.current_page) + 1
    } else {
      $state.wordPage = false
    }
  },
  SET_LOAD_MORE_WORD(state, loadMordWord) {
    const $state = state
    $state.loadMordWord = loadMordWord
  },
  SET_LOADING_MORE_LIST(state, loadingMoreList) {
    const $state = state
    $state.loadingMoreList = loadingMoreList
  },
}

const state = {
  vocabularies: [],
  wordSearchResult: {},
  listDetails: {},
  loading: false,
  loaded: false,
  nextPage: 1,
  wordPage: 1,
  loadMordWord: false,
  listMassegeStatus: false,
  listMessageText: '',
  loadingEditForm: false,
  addWordModal: false,
  loadingSearch: false,
  loadingMoreList: false,
  currentTab: 0,
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
}
