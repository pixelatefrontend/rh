const synth = window.speechSynthesis
let voices = []
let engVoice = null

function setSpeech(synth) {
  return new Promise(function(resolve, reject) {
    let id
    id = setInterval(() => {
      if (synth.getVoices().length !== 0) {
        resolve(synth.getVoices())
        clearInterval(id)
      }
    }, 10)
  })
}

setSpeech(synth).then(voiceList => {
  voices = voiceList
  engVoice = voices.find(v => v.lang === 'en-US')
})

const textToSpeech = text => {
  let utterance = new SpeechSynthesisUtterance(text)
  utterance.voice = engVoice
  utterance.lang = 'en-US'
  utterance.rate = 0.8
  synth.speak(utterance)
  return synth
}

export default engVoice

export { textToSpeech }
