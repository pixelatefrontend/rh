import Vue from 'vue'

const getters = {

  Badges: state => state.Badges,
  // availableBadges: state => state.availableBadges,
  // userBadges: state => state.userBadges,
  badgeDetail: state => state.badgeDetail,
  loading: state => state.loading,
  loaded: state => state.loaded,
}

const actions = {
  /**
   *
   */
  getBadges: ({ commit }) => {
    commit('SET_LOADING', true)
    Vue.axios
      .get(`/wordy/badges`)
      .then(response => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        commit('SET_BADGES', response.data.data.sort((a, b) => b.won - a.won))
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
  /**
   *
   */
  getBadgeDetails: ({ commit }, badgeID) => {
    commit('SET_LOADING', true)
    Vue.axios
      .get(`/badges/${badgeID}`)
      .then(response => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', true)
        commit('SET_BADGE_DETAIL', response.data)
      })
      .catch(e => {
        commit('SET_LOADING', false)
        commit('SET_LOADED', false)
      })
  },
}

const mutations = {
  SET_BADGES(state, Badges) {
    const $state = state
    $state.Badges = Badges
  },
  // SET_AVAILABLE_BADGES(state, availableBadges) {
  //   const $state = state
  //   $state.availableBadges = availableBadges
  // },
  // SET_USER_BADGES(state, userBadges) {
  //   const $state = state
  //   $state.userBadges = userBadges
  // },
  SET_BADGE_DETAIL(state, badgeDetail) {
    const $state = state
    $state.badgeDetail = badgeDetail
  },
  SET_LOADED(state, status) {
    const $state = state
    $state.loaded = status
  },
  SET_LOADING(state, status) {
    const $state = state
    $state.loading = status
  }
}

const state = {
  Badges: null,
  // availableBadges: null,
  // userBadges: null,
  badgeDetail: null,
  loading: null,
  loaded: null,
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
