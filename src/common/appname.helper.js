const appList = [
  {
    app: 'readings',
    aliasses: ['reading', 'readings', 'okuma']
  },
  {
    app: 'documents',
    aliasses: ['document', 'documents', 'dokuman', 'materials', 'resource', 'kaynak']
  },
  {
    app: 'wordy',
    aliasses: ['wordy', 'wordies']
  },
  {
    app: 'digibook',
    aliasses: ['digibook', 'digibooks']
  },
  {
    app: 'tests',
    aliasses: ['test', 'tests']
  },
  {
    app: 'exams',
    aliasses: ['exam', 'exams', 'deneme', 'e_deneme']
  },
  {
    app: 'cluebank',
    aliasses: ['cluebank', 'cluebanks', 'clue_bank', 'ipucu-havuzu']
  },
  {
    app: 'dictionary',
    aliasses: ['dictionary', 'sozluk']
  },
  {
    app: 'lessons',
    aliasses: ['lesson', 'lessons', 'ders', 'dersler']
  }
]

export const appNamer = appName => {
  const item = appList.find(el => el.aliasses.indexOf(appName) > -1)
  return item.app
}
