import Vue from 'vue'
import { products, baskets, store, destroy } from '../../services/store.service'

const state = {
  private: null,
  onlineLesson: null,
  packages: null,
  book: null,
  note: null,
  digital: null,
  isLoading: true,
  isLoaded: false,
  lastOnlineFilter: null,
  basketLoading: true,
  cartVisible: false,
  basket: {
    quantity: 0,
    total_price: 0,
    products: [],
    packages: []
  }
}

const getters = {
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  basket: state => state.basket,
  basketEmpty: state => state.basket.quantity === 0,
  basketLoading: state => state.basketLoading,
  privateData: state => state.private,
  privatePack: state => state.private.data.content,
  privateNotes: state => state.private.data.notes,
  privateLessons: state => state.private.data.products,
  onlineLessons: state => state.onlineLesson.data,
  onlineLessonsFilters: state => state.onlineLesson.filters,
  onlineLessonsCategories: state => state.onlineLesson.filters.categories,
  cartVisible: state => state.cartVisible,

  packagesCategories: state => state.packages.filters.categories,
  packages: state => state.packages,

  lastOnlineFilter: state => state.lastOnlineFilter,
  books: state => state.book.data,
  notes: state => state.note.data,
  digitals: state => state.digital.data,
  filteredOnlineLessons: state => (catId, filterId) => {
    if (catId === null) {
      return state.onlineLesson.data
    } else {
      if (filterId === null) {
        return state.onlineLesson.data.filter(el => el.categories[0].id === catId)
      } else {
        return state.onlineLesson.data.filter(el => {
          return el.categories[0].id === catId && el.categories.some(obj => obj.id === filterId)
        })
      }
    }
  },

  filteredOnlinePackages: state => (catId, filterId) => {
    if (catId === null) {
      return state.packages.data
    } else {
      if (filterId === null) {
        return state.packages.data.filter(el => el.categories[0].id === catId)
      } else {
        return state.packages.data.filter(el => {
          return el.categories[0].id === catId && el.categories.some(obj => obj.id === filterId)
        })
      }
    }
  },

  onlineLessonFilters: state => id => {
    let lessonFilters = [
      state.onlineLesson.filters.categories[1][0],
      state.onlineLesson.filters.categories[2][0]
    ]

    let filterObj = lessonFilters.find(el => el.value === id)
    if (filterObj === undefined || filterObj === null) {
      return [{ value: null, label: 'Seviye Seçin' }]
    } else {
      return filterObj.childs
    }
  },

  onlinePackageFilters: state => id => {
    let packageFilters = [
      state.packages.filters.categories[1][0],
      state.packages.filters.categories[2][0]
    ]

    let filterObj = packageFilters.find(el => el.value === id)
    if (filterObj === undefined || filterObj === null) {
      return [{ value: null, label: 'Paket Türü Seçiniz.' }]
    } else {
      return filterObj.childs
    }
  }
}

const mutations = {
  setLoading(state, data) {
    state.isLoading = data
  },

  setLoaded(state, data) {
    state.isLoaded = data
  },

  setBasketLoading(state, data) {
    state.basketLoading = data
  },

  setBasket(state, data) {
    state.basket = data
  },

  setContent(state, data) {
    Vue.set(state, data.tab, data.data)
  },

  setLessonsFilter(state, data) {
    state.lastOnlineFilter = data
  },

  setCartVisible(state, data) {
    state.cartVisible = data
  }
}

const actions = {
  getContent({ commit }, tab) {
    commit('setLoading', true)
    commit('setLoaded', false)
    products(tab).then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setContent', {
        data: res.data,
        tab: tab
      })
    })
  },

  getBasket({ commit }) {
    commit('setBasketLoading', true)
    baskets().then(res => {
      commit('setBasketLoading', false)
      if (res.data.basket !== null) commit('setBasket', res.data.basket)
    })
  },

  async addToBasket({ commit }, data) {
    let result = await store(data)
    let response = {
      success: false,
      data: ''
    }
    if (result.data.error) {
      response.data = result.data.error
    } else {
      commit('setBasket', result.data.basket)
      response.success = true
      response.data = result.data
    }
    return response
  },

  async removeFromBasket({ commit }, data) {
    let result = await destroy(data)
    if (result.data.basket === null) {
      result.data.basket = {
        quantity: 0,
        total_price: 0,
        products: [],
        packages: []
      }
    }
    let response = {
      success: false,
      data: ''
    }
    if (result.data.error) {
      response.data = result.data.error
    } else {
      commit('setBasket', result.data.basket)
      response.data = result.data
    }
    return response
  },

  handleCartVisible({ commit }, data) {
    commit('setCartVisible', data)
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
