import {
  getCluebankQuestions,
  getCluebankGrammars,
  getCluebankStrategies,
  getCluebankWords,
  getFavoriteClues,
  favoriteClue
} from '../../services/cluebank.service'

const getters = {
  questionTypes: state => state.questionTypes,
  grammars: state => state.grammars,
  strategies: state => state.strategies,
  words: state => state.words,
  favorites: state => state.favorites,
  cluebanks: state => state.cluebanksList
}

const actions = {
  getQuestions: ({ commit }) => {
    console.log('getting questions')
    commit('SET_QUESTION_TYPES', {
      loading: true,
      loaded: false
    })
    getCluebankQuestions()
      .then(({ data }) => {
        commit('SET_QUESTION_TYPES', {
          data,
          loading: false,
          loaded: true
        })
      })
      .catch(() => {
        commit('SET_QUESTION_TYPES', {
          data: null,
          loading: false,
          loaded: false
        })
      })
  },
  getGrammars: ({ commit }) => {
    commit('SET_GRAMMARS', {
      loading: true,
      loaded: false
    })
    getCluebankGrammars()
      .then(({ data }) => {
        commit('SET_GRAMMARS', {
          data,
          loading: false,
          loaded: true
        })
      })
      .catch(() => {
        commit('SET_GRAMMARS', {
          data: null,
          loading: false,
          loaded: false
        })
      })
  },
  getStrategies: ({ commit }) => {
    commit('SET_STRATEGIES', {
      loading: true,
      loaded: false
    })
    getCluebankStrategies()
      .then(({ data }) => {
        commit('SET_STRATEGIES', {
          data,
          loading: false,
          loaded: true
        })
      })
      .catch(() => {
        commit('SET_STRATEGIES', {
          data: null,
          loading: false,
          loaded: false
        })
      })
  },
  getWords: ({ commit }) => {
    commit('SET_WORDS', {
      loading: true,
      loaded: false
    })
    getCluebankWords()
      .then(({ data }) => {
        commit('SET_WORDS', {
          data,
          loading: false,
          loaded: true
        })
      })
      .catch(() => {
        commit('SET_WORDS', {
          data: null,
          loading: false,
          loaded: false
        })
      })
  },
  getFavorites: async ({ commit }) => {
    commit('SET_FAVORITES', {
      loading: true,
      loaded: false
    })
    await getFavoriteClues()
      .then(({ data }) => {
        commit('SET_FAVORITES', {
          data,
          loading: false,
          loaded: true
        })
      })
      .catch(() => {
        commit('SET_FAVORITES', {
          data: null,
          loading: false,
          loaded: false
        })
      })
  },
  setFavoriteItem: async ({ state, commit, dispatch }, id) => {
    if (state.favorites.data === null) {
      await dispatch('getFavorites')
      console.log('got fav clues')
    }
    console.log('setting fav item')
    commit('SET_FAVORITE', id)
    favoriteClue(id).then(() => {
      dispatch('getFavorites')
      dispatch('getQuestions')
      dispatch('getGrammars')
      dispatch('getStrategies')
      dispatch('getWords')
    })
  }
}

const mutations = {
  SET_QUESTION_TYPES(state, questionTypes) {
    const $state = state
    $state.questionTypes = { ...$state.questionTypes, ...questionTypes }
  },
  SET_GRAMMARS(state, grammars) {
    const $state = state
    $state.grammars = { ...$state.grammars, ...grammars }
  },
  SET_STRATEGIES(state, strategies) {
    const $state = state
    $state.strategies = { ...$state.strategies, ...strategies }
  },
  SET_WORDS(state, words) {
    const $state = state
    $state.words = { ...$state.words, ...words }
  },
  SET_FAVORITES(state, favorites) {
    const $state = state
    $state.favorites = { ...$state.favorites, ...favorites }
  },
  SET_FAVORITE(state, id) {
    const $state = state
    const favs = JSON.parse(JSON.stringify($state.favorites))
    favs.data.forEach(fav => {
      if (fav.id === id) fav.favorited = !fav.favorited
    })
    $state.favorites = favs

    const questions = JSON.parse(JSON.stringify($state.questionTypes))
    if (questions.data) {
      questions.data.forEach(qType => {
        qType.clues.forEach(fav => {
          if (fav.id === id) fav.favorited = !fav.favorited
        })
      })
      $state.questionTypes = questions
    }

    const grammars = JSON.parse(JSON.stringify($state.grammars))
    if (grammars.data) {
      grammars.data.forEach(qType => {
        qType.clues.forEach(fav => {
          if (fav.id === id) fav.favorited = !fav.favorited
        })
      })
      $state.grammars = grammars
    }

    const strategies = JSON.parse(JSON.stringify($state.strategies))
    if (strategies.data) {
      strategies.data.forEach(qType => {
        qType.clues.forEach(fav => {
          if (fav.id === id) fav.favorited = !fav.favorited
        })
      })
      $state.strategies = strategies
    }

    const words = JSON.parse(JSON.stringify($state.words))
    if (words.data) {
      words.data.forEach(qType => {
        qType.clues.forEach(fav => {
          if (fav.id === id) fav.favorited = !fav.favorited
        })
      })
      $state.words = words
    }
  }
}

const state = {
  questionTypes: {
    data: null,
    loading: false,
    loaded: false
  },
  grammars: {
    data: null,
    loading: false,
    loaded: false
  },
  strategies: {
    data: null,
    loading: false,
    loaded: false
  },
  words: {
    data: null,
    loading: false,
    loaded: false
  },
  favorites: {
    data: null,
    loading: false,
    loaded: false
  },
  cluebanksList: null
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
