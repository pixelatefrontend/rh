import Vue from 'vue'

export const questions = () => Vue.$axios('GET', `/wizards`)

export const store = form => Vue.$axios('POST', `/wizards`, form)
