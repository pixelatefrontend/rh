export default [
  {
    path: '/test',
    component: () => import(/* webpackChunkName: "tests" */ '../views/Tests'),
    children: [
      {
        path: 'list',
        redirect: '/test'
      },
      {
        path: 'detay',
        redirect: '/test'
      },
      {
        path: 'rapor',
        redirect: '/test'
      },
      {
        path: ':other',
        redirect: '/test'
      },
      {
        path: '',
        name: 'TestsList',
        component: () => import(/* webpackChunkName: "tests" */ '../views/Tests/TestsList'),
        meta: {
          title: 'Test',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: true,
          tourName: 'testsTour',
          noteName: 'test'
        }
      },
      {
        path: 'detay/:detailId',
        name: 'TestsDetail',
        component: () => import(/* webpackChunkName: "tests" */ '../views/Tests/TestsDetail'),
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          noteName: 'test'
        }
      },
      {
        path: 'rapor/:detailId',
        name: 'TestsReport',
        component: () => import(/* webpackChunkName: "tests" */ '../views/Tests/TestsReport'),
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          noteName: 'test'
        }
      }
    ]
  }
]
