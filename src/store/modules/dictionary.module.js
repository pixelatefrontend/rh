import { getDictionary } from '../../services/dictionary.service'

const state = {
  lastSearches: null,
  isLoading: true,
  isLoaded: false
}

const mutations = {
  setLastSearches: (state, data) => (state.lastSearches = data),
  setLoading: (state, data) => (state.isLoading = data),
  setLoaded: (state, data) => (state.isLoaded = data)
}

const actions = {
  getDictionaryData: ({ commit }) => {
    commit('setLoading', true)
    getDictionary().then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setLastSearches', res.data.last_searched_words)
      console.log(res.data)
    })
  }
}

const getters = {
  hasSearches: state => state.lastSearches !== null,
  lastSearches: state => state.lastSearches,
  loading: state => state.isLoading,
  loaded: state => state.isLoaded
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
