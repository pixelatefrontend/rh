/* eslint-disable no-param-reassign */
import axios from 'axios'
import _ from 'lodash'
import config from '@/config'
import store from '../store'
import {
  HANDLE_LOGIN_STATUS,
  HANDLE_STATUS_CODE,
  LOGOUT,
  SYSTEM_AVAILABILITY,
} from '../store/actions.type'
import router from '../routes'

const getClient = Vue => {
  const options = {
    baseURL: config.baseUrl,
  }

  const client = axios.create(options)
  const getAuthToken = () => localStorage.getItem('auth_token')

  client.interceptors.request.use(async request => {
    request.requestedOn = new Date().getTime()
    Vue.logger.log(
      `[${_.upperCase(request.method)}]:\n%c${request.url}`,
      'color: #f7b823'
    )

    const authToken = await getAuthToken()
    if (authToken) {
      request.headers.Authorization = `Bearer ${authToken}`
    }

    return request
  })

  client.interceptors.response.use(
    response => {
      if (response) {
        store.dispatch(SYSTEM_AVAILABILITY, true)
        const { requestedOn } = response.config
        const responseTime = new Date().getTime() - requestedOn
        Vue.logger.log(
          `[${response.config.method.toUpperCase()}]\n${
            response.config.url
          } (${responseTime}ms) %cdone!`,
          'color: #32CD32'
        )
      }
      return response
    },
    async error => {
      const { response } = error

      if (!response) {
        return store.dispatch(SYSTEM_AVAILABILITY, false)
      }

      store.dispatch(HANDLE_STATUS_CODE, response.status)

      if (response.status === 401) {
        store.dispatch(LOGOUT)
        store.dispatch(HANDLE_LOGIN_STATUS, false)
        router.push({ name: 'Login' })
      }

      // if (_.get(error, 'response.status') === 400) {
      //   return error.response
      // }

      if (_.get(error, 'response.status') >= 500) {
        console.log('ServerError: ', error)
        store.dispatch(SYSTEM_AVAILABILITY, false)
      }

      return Promise.reject(response)
    }
  )

  return client
}

export default {
  install(Vue) {
    const client = getClient(Vue)
    Vue.$axios = (method, url, body, headers, options) =>
      client[_.lowerCase(method)](url, body, {
        headers: !headers ? {} : headers,
        ...options
      }).catch(error => {
        Vue.logger.warn(error)
        Promise.reject(error)
      })
  },
}
