export const digibookToolsMixin = {
  data: function() {
    return {
      myTools: [],
    }
  },

  computed: {
    visibleTools() {
      return this.myTools.filter(el => el.visible === true)
    },
  },

  methods: {
    setMyTools(tools) {
      this.myTools = Array.from(tools)
      this.myTools.map(el => {
        // Object.assign(el, { visible: el.status })
        this.$set(el, 'visible', el.status)
      })
    },

    setToolVisibility(data) {
      let item = this.myTools.find(el => el.icon === data.tool)
      console.log('data: ', data)
      console.log('item: ', item)
      console.log('tols: ', this.myTools)
      this.$set(item, 'visible', data.visible)
    },
  },
}
