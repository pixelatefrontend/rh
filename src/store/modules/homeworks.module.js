import * as http from '../../services/homework.service'

const state = {
  isLoading: true,
  isLoaded: false,
  hasMore: false,
  homeworksList: null,
  buttonLoading: false,
  unfinishedHomeworks: 0
}

const getters = {
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  more: state => state.hasMore,
  homeworks: state => state.homeworksList,
  btnLoading: state => state.buttonLoading,
  unfinished: state => state.unfinishedHomeworks
}

const mutations = {
  setLoading: (state, data) => (state.isLoading = data),
  setBtnLoading: (state, data) => (state.buttonLoading = data),
  setLoaded: (state, data) => (state.isLoaded = data),
  setHomeworksList: (state, data) => (state.homeworksList = data),
  setUnfinishedHomeworks: (state, data) => (state.unfinishedHomeworks = data),
  setHasMore: (state, data) => (state.hasMore = data !== null),
  setMoreHomeworks: (state, data) => state.homeworksList.push(...data)
}

const actions = {
  getHomeworksList: ({ commit }) => {
    commit('setLoading', true)
    http.getHomeworks().then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setHomeworksList', res.data.data)
      commit('setUnfinishedHomeworks', res.data.unfinished_homework_count)
      commit('setHasMore', res.data.next_page_url)
    })
  },

  setMoreHomeworks: ({ commit }, data) => {
    commit('setBtnLoading', true)
    http.filterHomeworks(data).then(res => {
      commit('setBtnLoading', false)
      console.log('data incoming', res.data)
      commit('setMoreHomeworks', res.data.data)
      commit('setHasMore', res.data.next_page_url)
    })
  },

  setHomeworkFilter: ({ commit }, data) => {
    commit('setLoading', true)
    commit('setLoaded', false)
    http.filterHomeworks(data).then(res => {
      commit('setLoading', false)
      commit('setLoaded', true)
      commit('setHomeworksList', res.data.data)
      commit('setHasMore', res.data.next_page_url)
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
