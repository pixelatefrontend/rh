import Vue from 'vue'

export const getQuestions = () => Vue.$axios('GET', `/my-questions`)

export const filterQuestions = page => Vue.$axios('GET', `/my-questions?page=${page}`)

export const readAnswer = id => Vue.$axios('POST', `/my-questions/read-answer/${id}`)
