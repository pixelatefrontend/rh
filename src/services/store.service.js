import Vue from 'vue'

export const products = tab => Vue.$axios('GET', `/products?tab=${tab}`)

export const contentShow = id => Vue.$axios('GET', `/products/content/${id}`)

export const productShow = id => Vue.$axios('GET', `/products/product/${id}`)

export const baskets = () => Vue.$axios('GET', `/baskets`)

export const store = form => Vue.$axios('POST', `/baskets`, form)

export const destroy = form => Vue.$axios('POST', `/baskets`, form)
