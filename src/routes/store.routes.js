import StoreIndex from '../views/Store/index'
// import StorePrivate from '../views/Store/StorePrivate'
import StoreLessons from '../views/Store/StoreLessons'
import StoreLessonPackage from '../views/Store/StoreLessonPackage'
import StorePublic from '../views/Store/StorePublic'
import StorePackageDetail from '../views/Store/StorePackageDetail'
import StoreProductDetail from '../views/Store/StoreProductDetail'

export default [
  {
    path: '/magaza',
    component: StoreIndex,
    meta: {
      title: 'Mağaza',
      requiresAuth: true,
      headerShow: true,
      pageClass: 'header--gray',
      toolbarVisible: false,
      hasTour: false,
    },
    children: [
      {
        path: '',
        redirect: 'size-ozel',
      },
      {
        path: 'size-ozel',
        name: 'StorePrivate',
        component: () => import('../views/Store/StorePrivate'),
        meta: {
          headerShow: true,
          title: 'Size Özel | Mağaza',
          tabName: 'private',
        },
      },
      {
        path: 'online-dersler',
        name: 'StoreLessons',
        component: StoreLessons,
        meta: {
          headerShow: true,
          pageClass: 'header--gray',
          title: 'Online Dersler | Mağaza',
          tabName: 'onlineLesson',
        },
      },
      {
        path: 'kitaplar',
        name: 'StoreBooks',
        component: StorePublic,
        meta: {
          headerShow: true,
          pageClass: 'header--gray',
          title: 'Kitaplar | Mağaza',
          tabName: 'book',
        },
      },
      {
        path: 'notlar',
        name: 'StoreNotes',
        component: StorePublic,
        meta: {
          headerShow: true,
          pageClass: 'header--gray',
          title: 'Notlar | Mağaza',
          tabName: 'note',
        },
      },
      {
        path: 'dijital-urunler',
        name: 'StoreDigitals',
        component: StorePublic,
        meta: {
          headerShow: true,
          title: 'Dijital Ürünler | Mağaza',
          tabName: 'digital',
        },
      },
      {
        path: 'calisma-paketleri',
        name: 'StoreLessonPackage',
        component: StoreLessonPackage,
        meta: {
          title: 'Çalışma Paketleri',
          requiresAuth: true,
          headerShow: true,
          pageClass: 'header--gray',
        },
      },
      {
        path: 'calisma-paketleri/:id',
        name: 'StorePackageDetail',
        component: StorePackageDetail,
        meta: {
          title: 'Çalışma Paketleri',
          requiresAuth: true,
          headerShow: true,
          pageClass: 'header--gray',
          hasTour: false,
        },
      },
      {
        path: 'urunler/:id',
        name: 'StoreProductDetail',
        component: StoreProductDetail,
        meta: {
          title: 'Ürünler',
          requiresAuth: true,
          headerShow: true,
          pageClass: 'header--gray',
          hasTour: false,
        },
      },
    ],
  },
]
