import Vue from 'vue'

export const tests = page => Vue.$axios('GET', `/tests?page=${page}`)

export const groups = (group, page) => Vue.$axios('GET', `/tests?group=${group}&_page=${page}`)

export const testsInfo = () => Vue.$axios('GET', `/tests/info`)

export const test = id => Vue.$axios('GET', `/tests/${id}`)
