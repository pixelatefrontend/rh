import Digibook from '../views/Digibook/index'
import DigibookList from '../views/Digibook/DigibookList'

// import DigibookStaticContent from '../views/Digibook/DigibookStaticContent'
// import DigibookPassage from '../views/Digibook/DigibookPassage'
// import DigibookTest from '../views/Digibook/DigibookTest'
// import DigibookTranslate from '../views/Digibook/DigibookTranslate'
// import DigibookWordList from '../views/Digibook/DigibookWordList'
// import DigibookTable from '../views/Digibook/DigibookTable'
// import DigibookPairing from '../views/Digibook/DigibookPairing'
// import DigibookInfoContents from '../views/Digibook/DigibookInfoContents'

import DigibookContent from '../views/Digibook/Content/index'
import DigibookDetail from '../views/Digibook/DigibookDetail'
// import DigibookDetailNew from '../views/DigibookNew/DigibookDetail'
// import DigibookStatic from '../views/DigibookNew/DigibookStatic'
// import DigibookStaticContent from '../views/Digibook/DigibookStaticContent'
// import DigibookPassage from '../views/Digibook/DigibookPassage'
// import DigibookTest from '../views/Digibook/DigibookTest'
// import DigibookTranslate from '../views/Digibook/DigibookTranslate'
// import DigibookWordList from '../views/Digibook/DigibookWordList'
// import DigibookTable from '../views/Digibook/DigibookTable'
// import DigibookPairing from '../views/Digibook/DigibookPairing'
// import DigibookInfoContents from '../views/Digibook/DigibookInfoContents'

export default [
  {
    path: '/digibook',
    // component: () => import(/* webpackChunkName: "digibook" */ '../views/Digibook/'),
    component: Digibook,
    children: [
      {
        path: '',
        name: 'DigibookList',
        // component: () => import(/* webpackChunkName: "digibook" */ '../views/Digibook/DigibookList'),
        component: DigibookList,
        meta: {
          title: 'Digibook',
          requiresAuth: true,
          headerShow: true,
          toolbarVisible: true,
          hasTour: true,
          tourName: 'digibookTour',
          noteName: 'digibook',
        },
      },
      {
        path: ':id',
        name: 'DigibookDetail',
        component: DigibookDetail,
        meta: {
          requiresAuth: true,
          headerShow: false,
          toolbarVisible: true,
          noteName: 'digibook',
        },
        children: [
          {
            path: ':content',
            name: 'DigibookStatic',
            component: DigibookContent,
            meta: {
              requiresAuth: true,
              headerShow: false,
              toolbarVisible: true,
              noteName: 'digibook',
            },
          },
          {
            path: 'unite/:unitId/alistirma/:exerciseId/:contentType/:contentId',
            name: 'DigibookContent',
            component: DigibookContent,
            meta: {
              requiresAuth: true,
              headerShow: false,
              toolbarVisible: true,
              noteName: 'digibook',
            },
          },
        ],
      },
    ],
  },
]
