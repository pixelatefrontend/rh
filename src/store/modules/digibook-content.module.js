import { getDigibook } from '../../services/digibook.service'

const state = {
  digibookItem: null,
  lastDigibookId: null,
  contentTitle: '',
  active: false
}

const getters = {
  digibookItem: state => state.digibookItem,
  digibookInit: state => !state.digibookItem && !state.digibookInit,
  digibookId: state => state.lastDigibookId,
  contentTitle: state => state.contentTitle,
  active: state => state.active
}

const mutations = {
  setDigibookItem: (state, payload) => (state.digibookItem = payload),
  setActiveStatus: (state, payload) => (state.active = payload),
  setContentTitle: (state, title) => (state.contentTitle = title)
}

const actions = {
  getDigibookItem: async ({ state, commit }, id) => {
    if (!state.digibookItem && state.lastDigibookId !== id) {
      try {
        const response = await getDigibook(id)
        console.log('response: ', response)
      } catch (error) {
        console.error('error: ', error)
      }
    }
  },

  handleSetContentTitle: ({ commit }, data) => {
    commit('setContentTitle', data)
  },

  handleActiveStatus: ({ commit }, status) => {
    commit('setActiveStatus', status)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions
}
