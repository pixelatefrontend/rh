import * as http from '../../services/lessons.service'

const state = {
  groups: [],
  groupsLoaded: false,
  activeGroup: null,
  more: false,
  loadingMore: false,
  perPage: 10,
  currentPage: 0,
  maxPage: null,
  sortType: '',
  groupItems: null,
  isLoading: true,
  isLoaded: false,
  nextPage: false,
  filteredList: [],
  filterStatus: false,
  lessons: [],
  lessonAbout: {},
  lessonData: {},
  lessonHeader: {},
  lessonsGroup: [],
  lessonLink: null,
  lessonPlatform: null,
  loadMore: false,
  targetLesson: null,
  stateLoad: true,
  isWatched: null,
  watchButton: null
}

const actions = {
  toggleWatched: ({ commit }, id) => {
    commit('WATCH_BUTTON_ACTIVE', false)
    http.reset(id).then(res => {
      console.log(res)
      commit('SET_WATCHED')
      commit('WATCH_BUTTON_ACTIVE', true)
    })
  },

  getInfo: ({ commit }) => {
    commit('GROUPS_LOADED', false)
    http.info().then(res => {
      commit('SET_GROUPS', res.data.groups)
      commit('GROUPS_LOADED', true)
    })
  },

  getLastTab: ({ commit }) => {
    commit('SET_LAST_GROUP')
  },

  clearLesson: ({ commit }) => {
    commit('CLEAR_LESSON')
  },

  fetchLesson: ({ commit }, lessonId) => {
    commit('SET_LOADING', true)
    http.lesson(lessonId).then(res => {
      commit('SET_LOADING', false)
      commit('SET_LOADED', true)
      if (res.data.status === 'continue' && res.data.lesson_status === 'recording') {
        commit('SET_LESSON_LINK', res.data.href)
      } else {
        commit('SET_LESSON_LINK', res.data.lesson_link)
      }
      commit('SET_LESSON_PLATFORM', res.data.platform)
      commit('FETCH_LESSON', res.data)
      commit('SET_LESSON_HEADER')
      commit('SET_LESSON_ABOUT')
    })
  },

  loadNext: ({ commit }, filters) => {
    // commit('SET_LOADING_MORE', true)

    http.list(filters).then(res => {
      // commit('SET_LOADING_MORE', false)
      commit('LOAD_NEXT', res.data.data)
      commit('SET_LOADMORE', res.data.next_page)
    })
  },

  getLessonsByGroup: ({ commit }, id) => {
    commit('CLEAR_LIST')
    commit('SET_LOADING', true)

    http.lessons(id).then(res => {
      commit('SET_LESSONS_LIST', res.data.data)
      commit('SET_LOADING', false)
      commit('SET_LOADED', true)
      commit('SET_LOADMORE', res.data.next_page)
      console.log(res.data)
    })
    console.log(id)
  },

  getLessonsList: ({ commit }, filters) => {
    commit('CLEAR_LIST')
    commit('SET_LOADING', true)

    http.list(filters).then(res => {
      commit('SET_LESSONS_LIST', res.data.data)
      commit('SET_LOADING', false)
      commit('SET_LOADED', true)
      commit('SET_LOADMORE', res.data.next_page)
    })
  },

  setActiveGroup: ({ commit }, groupId) => {
    commit('CLEAR_LIST')
    commit('SET_LOADING', true)
    http.lessons(groupId).then(res => {
      commit('SET_LESSONS_LIST', res.data.data)
      commit('SET_LOADING', false)
    })
  },

  setLastTab: ({ commit }, id) => {
    commit('SET_LAST_GROUP', id)
  },

  setFilter: ({ commit }, filter) => {
    commit('SET_LESSONS_FILTER', filter)
  },

  sortLessons: ({ commit }) => {
    commit('SORT_LESSONS')
  },

  setLoadMore: ({ commit }) => {
    commit('SET_LOADMORE')
  },

  initLessons: ({ commit }) => {
    commit('INIT_LESSONS')
  }
}

const mutations = {
  WATCH_BUTTON_ACTIVE(state, status) {
    state.watchButton = status
  },

  SET_WATCHED(state) {
    state.isWatched = !state.isWatched
    if (state.isWatched) {
      state.lessonData.last_watch_time = state.lessonData.duration
    } else {
      state.lessonData.last_watch_time = 0
    }
  },

  SET_LESSONS_LIST(state, data) {
    state.filteredList = data
  },

  GROUPS_LOADED(state, status) {
    state.groupsLoaded = status
  },

  SET_GROUPS(state, data) {
    state.groups = data
  },

  SET_LAST_GROUP(state, group) {
    if (group === null || group === undefined) {
      localStorage.lessonsGroup = -1
    } else {
      localStorage.lessonsGroup = group
    }
    state.activeGroup = localStorage.lessonsGroup
  },

  CLEAR_LESSON(state) {
    state.lessonData = {}
    state.lessonHeader = {}
  },

  FILTER_STATUS(state, status) {
    state.filterStatus = status
  },

  FILTER_FINISHED(state, filter) {
    if (filter.value === true) {
      let filteredItems = state.lessonsGroup.filter(el => {
        return el.progress === 100
      })
      state.filteredList = filteredItems.slice(0, state.perPage)
      state.groupItems = filteredItems.length
    } else if (filter.value === false) {
      let filteredItems = state.lessonsGroup.filter(el => {
        return el.progress > 0 && el.progress < 100
      })
      state.filteredList = filteredItems.slice(0, state.perPage)
      state.groupItems = filteredItems.length
    } else {
      let filteredItems = state.lessonsGroup
      state.filteredList = filteredItems.slice(0, state.perPage)
      state.groupItems = filteredItems.length
    }
  },

  SET_LESSON_PLATFORM(state, platform) {
    state.lessonPlatform = platform
  },

  SET_LESSON_LINK(state, link) {
    state.lessonLink = link
  },

  CLEAR_LIST(state) {
    state.filteredList = []
  },

  INIT_LESSONS(state) {
    state.sortType = ''
    state.filteredList = state.lessonsGroup
      .sort((a, b) => {
        return new Date(b.date) - new Date(a.date)
      })
      .slice(0, state.perPage)
  },

  FETCH_LESSON(state, lesson) {
    state.targetLesson = lesson.id
    state.lessonData = lesson
    if (lesson.duration === lesson.last_watch_time) {
      state.isWatched = true
    }
  },

  SET_LESSON_HEADER(state) {
    state.lessonHeader = {
      name: state.lessonData.name,
      tags: state.lessonData.tags,
      group: state.lessonData.group_name,
      icon: state.lessonData.group_icon,
      date: state.lessonData.date,
      hour: state.lessonData.hour,
      duration: state.lessonData.duration
    }
  },

  SET_LESSON_ABOUT(state) {
    state.lessonAbout = {
      teacher: state.lessonData.teacher,
      description: state.lessonData.description,
      units: state.lessonData.units
    }
  },

  LOAD_NEXT(state, data) {
    state.filteredList.push(...data)
  },

  SET_LESSONS(state, lessons) {
    state.lessons = lessons
    state.isLoading = false
    state.filteredList = lessons.slice(0, state.perPage)
  },

  SET_GROUP(state, group) {
    localStorage.lessonsGroup = group
  },

  SET_LESSONS_FILTER(state, filter) {
    state.sortType = filter
  },

  SORT_LESSONS(state) {
    state.lessonsGroup = state.lessonsGroup.sort((a, b) => {
      if (state.sortType === 'asc') {
        return new Date(a.date) - new Date(b.date)
      }
      if (state.sortType === 'desc') {
        return new Date(b.date) - new Date(a.date)
      }
      if (state.sortType === 'high') {
        return b.progress - a.progress
      }
      if (state.sortType === 'low') {
        return a.progress - b.progress
      }
    })
    state.filteredList = state.lessonsGroup.slice(0, state.perPage)
  },

  STATE_LOAD(state, status) {
    state.stateLoad = status
  },

  SET_LOADMORE(state, status) {
    state.loadMore = status
  },

  SET_LOADING(state, status) {
    state.isLoading = status
  },

  SET_LOADED(state, status) {
    state.isLoaded = status
  }
}

const getters = {
  groupsList: state => state.groups,
  groupsLoaded: state => state.groupsLoaded,
  activeGroup: state => state.activeGroup,
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  lessonsList: state => state.filteredList,
  lessonItems: state => state.filteredList,
  documentsByGroup: state => groupId => {
    if (groupId === -1) {
      return state.documents.slice(0, 10)
    } else {
      return state.documents.filter(el => {
        return el.groups.filter(g => g.id === groupId).length > 0
      })
    }
  },
  isLoaded: state => state.stateLoad,
  loadingFilter: state => state.filterStatus,
  loadMore: state => state.loadMore,
  currentLessonId: state => state.targetLesson,
  currentLesson: state => state.lessonData,
  link: state => state.lessonLink,
  platform: state => state.lessonPlatform,
  lessonHeaderInfo: state => state.lessonHeader,
  isWatched: state => state.isWatched,
  watchButtonActive: state => state.watchButton,
  isEmpty: state => state.filteredList.length === 0 && state.isLoading === false
}

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters
}
