export default {
  data() {
    return {
      toolsList: null
    }
  },

  methods: {
    setToolsList(tools) {
      let toolsArray = []

      Object.entries(tools).forEach(([k, v], i) => {
        let title = ''
        let icon = ''
        if (k === 'word_list') {
          title = 'Kelimeler'
          icon = 'icon-lists'
        }
        if (k === 'root_and_translate') {
          title = 'Çevir'
          icon = 'icon-translate'
        }
        if (k === 'answer') {
          title = 'Cevabı Gör'
          icon = 'icon-answer'
        }
        if (k === 'description_and_tags') {
          title = 'Açıklaması'
          icon = 'icon-info'
        }
        if (k === 'is_question_permission') {
          title = 'Soru Sor'
          icon = 'icon-question'
        }
        let item = {
          type: k,
          status: v.status,
          condition: v.condition,
          title: title,
          icon: icon
        }
        toolsArray.push(item)
      })

      this.toolsList = [...toolsArray]
    }
  }
}
