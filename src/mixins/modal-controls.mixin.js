import { mapGetters } from 'vuex'
import { CLUE_MODAL_VISIBLE, HANDLE_CLUE_MODAL } from '../store/actions.type'

export const modalControlsMixin = {
  data() {
    return {
      questionsModalVisible: false,
    }
  },

  computed: {
    ...mapGetters(['clueModal'])
  },

  methods: {
    showClueModal(clue) {
      this.$store.dispatch(HANDLE_CLUE_MODAL, { ...clue, isActive: true })
      // this.$store.dispatch(CLUE_MODAL_VISIBLE, true)
    },

    hideClueModal() {
      this.$store.dispatch(CLUE_MODAL_VISIBLE, false)
    },

    handleClueModal() {
      this.$store.dispatch(HANDLE_CLUE_MODAL, this.clueModal)
    },

    hideQuestionsModal() {
      this.questionsModalVisible = false
    },

    showQuestionsModal() {
      this.questionsModalVisible = true
    },

    handleVisibility(data) {
      let loading = data.toolLoading
      let loaded = data.toolLoaded
      let tool = data.activeTool

      return !loading && loaded && tool === 'question'
    },
  },
}
