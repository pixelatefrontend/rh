import Vue from 'vue'
import _ from 'lodash'

export default {
  props: {
    questions: {
      type: Array,
      default: () => [],
    },
    currentQuestion: {
      type: Object,
      default: () => {},
    },
    showStepIntro: {
      type: Boolean,
      default: true,
    },
    presentations: {
      type: Array,
      default: () => [],
    },
    stepName: {
      type: String,
      default: '',
    },
    stepType: {
      type: String,
      default: '',
    },
    nextStepsName: {
      type: String,
      default: '',
    },
    nextStepsDescription: {
      type: String,
      default: '',
    },
    setId: {
      type: Number,
      default: 0,
    },
    stepId: {
      type: Number,
      default: 0,
    },
    isRepeat: {
      type: Boolean,
      default: false,
    },
  },
  data() {
    return {
      showTest: false,
      showResult: false,
      showList: false,
      selectedAny: false,
      resultData: {
        percentage: 0,
        stepName: this.stepName,
        nextStepsName: this.nextStepsName,
        nextStepsDescription: this.nextStepsDescription,
      },
      correctOption: {},
      currentIntroItem: {},
    }
  },
  computed: {
    showIntro() {
      return (
        !_.isEmpty(this.presentations) &&
        !this.showList &&
        !this.showTest &&
        !this.showResult
      )
    },
  },
  methods: {
    async playWrong() {
      if (this.$store.getters['soundsOn']) {
        await new Audio('/wrong.mp3').play()
      }
    },
    async playCorrect() {
      if (this.$store.getters['soundsOn']) {
        await new Audio('/correct.mp3').play()
      }
    },
    getWordType(key) {
      const keyword = key ? key.toUpperCase() : ''
      if (keyword === 'V') {
        return 'verb'
      } else if (keyword === 'N') {
        return 'noun'
      } else if (keyword === 'D' || keyword === 'DET') {
        return 'det'
      } else if (keyword === 'A' || keyword === 'ADJ') {
        return 'adj'
      }
      return 'noun'
    },
    startTest() {
      this.showResult = false
      this.showList = false
      this.showTest = true
      this.$emit('hide-step-intro')
    },
    selectOption(option, timeout) {
      option.selected = true
      this.$emit(
        'answer-current-question',
        this.currentQuestion.answerId,
        option.id,
        this.currentQuestion.tryCount
      )
      this.nextQuestion(timeout)
    },
    nextQuestion(timeOut) {
      const currentQuestionIndex = this.questions.indexOf(this.currentQuestion)
      // if it's not latest question in queue
      setTimeout(() => {
        if (_.last(this.questions) !== this.currentQuestion) {
          this.correctOption = {}
          this.$emit('next-question', this.questions[currentQuestionIndex + 1])
          this.selectedAny = false
        } else {
          const stepData = {
            stepId: this.setId, // rh side is calling "set's" as "step"
            typeId: this.stepId, // and calling "step's" as "type",
            questions: _.map(this.questions, question => {
              return {
                answer_id:
                  question.answerId === -1
                    ? question.userAnswerId
                    : question.answerId,
                user_answer_id: question.userAnswerId,
                try_count: question.tryCount,
              }
            }),
          }

          Vue.$axios('POST', 'wordy/result', stepData).then(response => {
            this.resultData.percentage = _.get(response, 'data.percentage', 0)
            this.showResult = true
            this.showList = false
            this.showTest = false
          })
        }
      }, timeOut || 2000)
    },
    nextIntro() {
      const currentIntroIndex = this.presentations.indexOf(
        this.currentIntroItem
      )
      // if it's not latest intro item in queue
      if (_.last(this.presentations) !== this.currentIntroItem) {
        this.$emit('next-question')
        this.currentIntroItem = this.presentations[currentIntroIndex + 1]
      } else {
        this.showList = true
      }
    },
    setCorrectOption(option) {
      this.correctOption = option
    },
  },
}
