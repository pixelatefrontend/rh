import Vue from 'vue'
import * as http from '../../services/calendar.service'

const state = {
  weeklyPlan: {
    endDate: null
  },
  calendarData: {},
  workPlan: [],
  workPlanLoaded: false,
  workPlanLoading: true,
  isLoading: true,
  isLoaded: false,
  isWeeklyPlanCreated: false
}

const getters = {
  weeklyPlanStatus: state => state.isWeeklyPlanCreated && state.weeklyPlan.endDate !== null,
  weeklyPlanPresent: state => state.isWeeklyPlanCreated,
  endDateStatus: state => state.weeklyPlan.endDate !== null,
  formattedEndDate: state => new Date(state.weeklyPlan.endDate * 1000),
  loading: state => state.isLoading,
  loaded: state => state.isLoaded,
  weeklyPlan: state => state.weeklyPlan,
  planEndDate: state => state.weeklyPlan.endDate,
  calendarData: state => state.calendarData,
  calendarDataExists: state => Object.keys(state.calendarData).length !== 0 && state.calendarData.constructor === Object,
  workPlan: state => state.workPlan,
  workPlanLoaded: state => state.workPlanLoaded,
  workPlanLoading: state => state.workPlanLoading,
  workPlanExist: state => state.workPlan.length > 0
}

const mutations = {
  setLoading(state, data) {
    state.isLoading = data
  },

  setLoaded(state, data) {
    state.isLoaded = data
  },

  setWorkPlan(state, data) {
    state.workPlan = data
  },

  setWorkPlanLoading(state, data) {
    state.workPlanLoading = data
  },

  setWorkPlanLoaded(state, data) {
    state.workPlanLoaded = data
  },

  buildWeeklyPlan(state, data) {
    state.weeklyPlan = data
  },

  weeklyPlanCreated(state, data) {
    state.isWeeklyPlanCreated = data
  },

  setCalendarData(state, data) {
    state.calendarData = data
  },

  setEndDate(state, data) {
    console.log('mutated: ', data)
    Vue.set(state.weeklyPlan, data.key, data.value)
  },

  modifyDateBeforePost(state, data) {
    state.weeklyPlan.endDate = new Date(data).toISOString()
  },

  setCheckedDay(state, data) {
    let app = state.weeklyPlan.apps.find(el => el.app === data.app)
    let day = app.days.find(el => el.id === data.day)
    day.checked = data.status
    console.log('action app', app)
    console.log('action day', day)
    console.log('action chk', data.status)
  }
}

const actions = {
  getUserWeeklyPlan({ commit }) {
    commit('setLoading', true)
    http.getWeeklyPlan().then(res => {
      commit('setLoading', false)
      if (res.data.endDate !== null) {
        commit('weeklyPlanCreated', true)
      } else {
        commit('setWorkPlanLoading', false)
      }
      commit('setLoaded', true)
      commit('buildWeeklyPlan', res.data)
    })
  },

  getUserCalendar({ dispatch, commit }, payload) {
    commit('setLoading', true)
    http.calendar(payload).then(res => {
      commit('setLoading', false)
      // commit('setLoaded', true)
      commit('setCalendarData', res.data)
      dispatch('getUserWorkPlan')
    })
  },

  createUserWorkPlan({ dispatch, commit }, payload) {
    commit('setLoading', true)
    console.log('store payload: ', payload)
    http.createWeeklyPlan(payload).then(res => {
      console.log('Weekly Plan Result: ', res.data)
      if (res.data.endDate !== null) {
        commit('setLoading', false)
        commit('weeklyPlanCreated', true)
        commit('buildWeeklyPlan', res.data)
        dispatch('getUserCalendar', new Date().toISOString().substr(0, 10)).then(() => {
          commit('setLoaded', true)
        })
      } else {
        console.log('olmadi be kanka')
      }
    }).catch(err => {
      commit('setLoading', false)
      commit('weeklyPlanCreated', false)
      console.log('error: ', err)
    })
  },

  getUserWorkPlan({ commit }) {
    http.getWorkPlan().then(res => {
      commit('setWorkPlan', res.data)
      commit('setWorkPlanLoading', false)
      commit('setWorkPlanLoaded', true)
    })
  },

  updateEndDate({ commit }, payload) {
    commit('setEndDate', payload)
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
}
